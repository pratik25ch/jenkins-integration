var Helper = function () {

    //Wait for the element to Present
    this.waitForPresent = function (element) {
        return browser.wait(() => (element.isPresent()), 20000);
    }
    //Wait for the element to Display
    this.waitForDisplay = function (element) {
        return browser.wait(() => (element.isDisplayed()), 20000);
    }
    //Sleep Conditions
    this.sleep = function (time) {
        browser.sleep(time);
    }
    //Expected Conditions
    this.waitForVisibility = function (element, time) {
        let EC = protractor.ExpectedConditions;
        return browser.wait(EC.visibilityOf(element), time || 20000);
    }
    this.waitforClick = function(element){
        var EC = protractor.ExpectedConditions;
        return browser.wait(EC.elementToBeClickable(element), 15000);
    }
}
module.exports = new Helper();