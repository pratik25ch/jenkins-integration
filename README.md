# Protractor-Jasmine Framework ([![FLOWACE](https://flowace.in/)](https://flowace.in/))

![Flowace](testData/images/flowace.jpg)

## Flowace Appplication [![Built with Angular](https://angular.io/)](https://angular.io/)

* [Directory Structure](#directory-structure)
* [Introduction](#introduction)
* [Installation](#installation)
* [Web Dashboard](#web-dashboard)
* [Matters](#matters)
* [Timesheets](#timesheets)
* [Billing](#billing)
* [Reporting](#reporting)
* [Analytics](#analytics)

## Directory Structure

```
├──Frontend-Testing-Automation
│   │
│   ├──Images/
│   │   └── Flowace.jpg
│   │
│   ├──lib/
│   │   └── comm_lib.js
│   │
│   ├──node_modules/
│   │   └── All Dependencies
│   │               
│   ├──pages/
│   │   ├──analytics_po.js
│   │   ├──contacts_po.js
│   │   ├──employees_po.js
│   │   ├──gcm_po.js
│   │   ├──homepage_po.js
│   │   ├──login_po.js
│   │   ├──settings_po.js
│   │   ├──templates_po.js
│   │   └──timesheets_po.js
│   │
│   │
│   ├──specs/
│   │   ├──analytics/
│   │   │   ├──analytics_spec.js
│   │   │   └──e2eanalytics_spec.js
│   │   │
│   │   ├──contacts/
│   │   │   ├──contacts_spec.js
│   │   │   └──e2econtacts_spec.js
│   │   │
│   │   ├──employees/
│   │   │   ├──employees_spec.js
│   │   │   └──e2eemployees_spec.js
│   │   │
│   │   ├──gcm/
│   │   │   ├──gcm_spec.js
│   │   │   └──e2egcm_spec.js
│   │   │
│   │   ├──login/
│   │   │   └──login_spec.js
│   │   │
│   │   ├──settings/
│   │   │   ├──settings_spec.js
│   │   │   └──e2esettings_spec.js
│   │   │
│   │   ├──templates/
│   │   │   ├──templates_spec.js
│   │   │   └──e2etemplates_spec.js
│   │   │
│   │   └──timesheets/
│   │       ├──timesheets_spec.js
│   │       └──e2etimesheets_spec.js
│   │   
│   ├──test_data/
│   │   ├──bulkupload.csv
│   │   └──data.json
│   │
│   │
│   ├──testReport/
│   │   └──screenshots/
│   │           ├──screenshots/
│   │           │       └──image.png
│   │           └──htmlReport.html
│   │
│   ├──utils/
│   │   └──comm_utils.js
│   │
│   ├──bower.json
│   │
│   ├──protractor_conf.js
│   │
│   ├──mail.js
│   │
│   ├──package-lock.json
│   │
│   ├──package.json
│   │
│   └──README.md
```

## Introduction

####What is Flowace?
Flowace is unparalleled business intelligence software for law firms to track billing hours on a matter accurately, manage contacts, tasks, invoices and documents. Multiple services are provided on this single platform, enabling Law Firms to enhance efficiencies, increase productivity, maximize profits and streamline workflows.

![Introduction](testData/images/introduction.jpg)

## Installation

####Windows Desktop Plugin
- Check if you are using 32-bit office products OR 64-bit office products. As an example, launch word & go to About Word to check if it’s 32-bit or 64-bit.Windows Desktop Plugin
- Check if you are using 32-bit office products OR 64-bit office products. As an example, launch word & go to About Word to check if it’s 32-bit or 64-bit.

- You need to login into the system with the domain, username and password. Every registered user can find the password under their mailbox with a simple search of “Your Flowace Account Password”.

![Plugin](testData/images/plugin.jpg)

##Web Dashboard

####Login
- Open the Flowace’s url shared with you on a browser.
- Enter your username and password to login into the system.

![Dashboard](testData/images/dashboard.jpg)

##Matters

####Creating L1(Group), L2(Client) and L3(Matter) along with member assignment
- Admin/Manager starts creating L1, L2 & L3 and assigns members who will be working on those matters.
- The matters/L3 will be displayed to the users across the platform, only if assigned.

#####Archiving/Unarchiving Matters
- Admin/Manager can archive the matters/L3 once they feel these matters are no longer needed for classification.
- They also have a provision to unarchive the matters if they want it back in the system for classification.
####Tasks & User Comments
- Users have a provision to add tasks with description at Matter/L3 level. This can be used when multiple people are working. A simple checklist view lists all the “To Do” tasks under a particular Matter/L3.
- On completion of a task, a user can simply check the tasks completed and other assigned users get to know what all is done and what all needs to be done.
- Users can also add their comments against a respective task for future references.
This helps users to work effectively, be on the same page and avoid duplication of efforts.
####Shortcut to create Matters/L3
- Use “+” icon from left side menu to create Matters/L3. User just needs to select already created Group/L1 & Client/L2 from the drop-down.
- This feature can only be used to create Matters against already created Group/L1 & Clients/L2.

##Timesheet

#####Timesheet View
- The timesheet view displays classified, unclassified and idle entries of the users.
- The classified duration is the entry of your work on Word, Excel, PowerPoint and Acrobat (PDF). These entries of time duration are automatically recorded as a part of the workday.
- The unclassified duration is the duration you have spent on other areas of your Windows System, like browser related activities, launching softwares, File Explorer and so on. User can manually classify relevant entries to record it as a part of the workday.
- The idle duration is the recorded duration when you are not performing any action on your Windows Desktop.

##Billing

####View Billing Dashboard
- You can view “Outstanding” bill of existing groups, clients or matters. You can also select “Group by” option to specify if you want view as per clients or groups or matters.
- By clicking on “Invoiced” tab, you can view already generated invoices with date and amount. Upon clicking any invoice, you can view rest of the details of selected invoice.
- By selecting “Completed” from the tab, you can view fully or partially completed invoices. You can also sort the entries based on – Amount, Date or Name as per your requirement.
- On upper right corner, there is also an option to search invoices based on their name, ID, or Tally ID.
- There is also an option to hide all zero amount bills; you can do so by checking the box on the right, above “Amount” column.

##Reporting

- Depending on your requirement you can generate following type of reports
    - Day wise
    - Detailed report
    - Summary report
    - Timesheet data
    - Associate data
- Select any type of above-mentioned reports, then select Group, Client and Matter.
- You also need to specify the start and end date for requested report to be generated

#Analytics

- User can analyze their work progress in Analytics tab. It gives visual representation of time spent on classified and unclassified work as per their timesheets.
- You can select employee, start and end dates to get that specific data analyzed, and view results as per.
- There is “Matter Overview” demonstrating Existing, New, Active and Closed matters for selected employee/user.
- Duration spent on different matters is depicted in form of chart on bottom left of page.
- Most used app by user is showed in bottom right of the page.
- User can also download this analytics report by clicking top right button on screen labeled “DOWNLOAD REPORT”. Report is saved in PDF on your machine.


#About Protractor & Jasmine
Protractor is an end-to-end testing framework for AngularJS applications and works as a solution integrator — combining powerful tools and technologies such as NodeJS, Selenium, web driver, Jasmine.It has a bunch of customizations from Selenium to easily create tests for AngularJS applications.

**Protractor −** It is a wrapper over WebDriver JS especially designed for angular apps.

**Jasmine −** It is basically a behavior-driven development framework for testing the JavaScript code. 

**WebDriver JS −** It is a Node JS bindings implementation for selenium 2.0/WebDriver.

**Selenium** − It simply automates the browser.

## Setting Up Protractor:-

- Protractor is a Node.js program. To run Protractor, you will need to have Node.js installed.
- Install [npm](https://www.npmjs.com/) to make sure it's installed you can run `npm -v`
- Install [NodeJS](https://nodejs.org/en/download/package-manager/) to make sure it's installed you can run `node -v`
- Install dependencies: `npm install`

### Installing Protractor

- Use npm to install Protractor globally `npm install -g protractor`
- Try running `protractor --version` to make sure it's working.
- The webdriver-manager is a helper tool to easily get an instance of a Selenium Server running.
- For updating Webdriver `webdriver-manager update`
- Now start up a server with `webdriver-manager start`

#### Getting Started

- See the Protractor Website for most documentation.To get set up and running quickly:
- Work through the Tutorial [Protractor API](https://www.protractortest.org/#/api)
