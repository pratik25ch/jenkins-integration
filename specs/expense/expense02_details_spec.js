var CommPage = require("../../lib/CommLib/comm_lib")
var HelperPage = require("../../utils/helperUtils/helper")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var TimesheetPage = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")
var ExpensePage01 = require("../../pages/expense/expense01_summary_po")
var ExpensePage02 = require("../../pages/expense/expense02_details_po")

let R = Math.random().toString(36).substring(7);

// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login();
//     CommPage.sleep(3);
//     HomePage.Expances();
//     CommPage.sleep(2);
// })

describe('EXPENSE MODULE: Table Section', function () {
    it('Should Click on Table Btn', async function () {
        ExpensePage02.details_Btn();
        CommPage.sleep(1);
        expect(ExpensePage02.details_page.isDisplayed()).toBeTruthy();
    })
    it('Should Add New Expense', async function () {
        ExpensePage02.newExpenseBtn();
        CommPage.sleep(2)
        ExpensePage02.memberName.sendKeys('Debasish Patra')
        CommPage.sleep(1);
        ExpensePage02.firstOption.click();
        CommPage.sleep(3);
        ExpensePage02.matterName.click();
        CommPage.sleep(2);
        ExpensePage02.firstOption.click();
        CommPage.sleep(2);
        ExpensePage02.expenseType.click();
        CommPage.sleep();
        ExpensePage02.secondOption.click();
        ExpensePage02.expenseAmount.sendKeys('3000');
        CommPage.sleep(2);
        ExpensePage02.calender.click();
        ExpensePage02.todaysDate.click();
        CommPage.sleep(3);
        expect("Expenses Created Successfully").toEqual("Expenses Created Successfully");
    })
    it('Should Edit Expense Type & amount', async function () {
        CommPage.refresh();
        ExpensePage02.details_Btn();
        CommPage.sleep(2);
        ExpensePage02.editDots.click();
        CommPage.sleep(1);
        ExpensePage02.editBtn.click();
        ExpensePage02.expenseType.clear();
        ExpensePage02.firstOption.click();
        CommPage.sleep();
        ExpensePage02.expenseAmount.clear();
        ExpensePage02.expenseAmount.sendKeys('7000');
        CommPage.sleep(2);
        ExpensePage02.saveBtn.click().then(function () {
            console.log("Expenses Type & amount Updated Successfully")
        })
        CommPage.sleep(3);
        expect("Expenses Updated Successfully").toEqual("Expenses Updated Successfully");
    })
    it('Should Edit Expense Matter & Member-628', async function () {
        ExpensePage02.editDots.click();
        CommPage.sleep(1);
        ExpensePage02.editBtn.click();
        CommPage.sleep(2)
        ExpensePage02.memberName.clear().sendKeys('Soham');
        ExpensePage02.firstOption.click();
        CommPage.sleep(2);
        ExpensePage02.matterName.clear();
        ExpensePage02.matterName.click();
        CommPage.sleep(2);
        ExpensePage02.secondOption.click();
        CommPage.sleep(2);
        ExpensePage02.saveBtn.click().then(function () {
            console.log("Expenses Matter & Member Updated Successfully")
        })
        CommPage.sleep(3);
        expect("Expenses Updated Successfully").toEqual("Expenses Updated Successfully");
    })
    xit('Should go to next Page of Expense Section', async function () {
        ExpensePage02.nextPage.click();
        CommPage.sleep(2);
        ExpensePage02.previousPage.click();
        CommPage.sleep(2);
    })
    xit('Should go to Last Page of Expense Section', async function () {
        ExpensePage02.lastPage.click();
        CommPage.sleep(2);
        ExpensePage02.firstPage.click();
        CommPage.sleep(2);
    })
    xit('Should ascending the Expense by Date', async function () {
        ExpensePage02.dateHeader.click();
        ExpensePage02.dateHeader.click();
        CommPage.sleep(1);
        ExpensePage02.matterHeader.click();
        ExpensePage02.matterHeader.click();
        CommPage.sleep(1);
        ExpensePage02.nameHeader.click();
        ExpensePage02.nameHeader.click();
        CommPage.sleep(1);
        ExpensePage02.typeHeader.click();
        ExpensePage02.typeHeader.click();
        CommPage.sleep(1);
        ExpensePage02.amountHeader.click();
        ExpensePage02.amountHeader.click();
        CommPage.sleep(1);
    })
    xit('Should Upload Expense Files', async function () {
        




    })
    


























})