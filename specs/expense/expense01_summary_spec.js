var CommPage = require("../../lib/CommLib/comm_lib")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var TimesheetPage = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")
var ExpensePage01 = require("../../pages/expense/expense01_summary_po")
var ExpensePage02 = require("../../pages/expense/expense02_details_po")

let R = Math.random().toString(36).substring(7);

beforeAll(function () {
    LoginPage.GetURL();
    LoginPage.login();
    CommPage.sleep(3);
    HomePage.Expances();
    CommPage.sleep(2);
})
describe('EXPENSE MODULE: Summary Section', function () {
    it('Should Click on Summary Btn', async function () {
        ExpensePage01.summary_Btn();
        console.log('Summary Section Opened Succesfully');
        expect(ExpensePage01.summary_page.isDisplayed()).toBeTruthy();
    })
})