var CommPage = require("../../lib/CommLib/comm_lib")
var VerifyPage = require("../../lib/actionLib/verify")
var HelperPage = require("../../utils/helperUtils/helper")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")

let R = Math.random().toString(36).substring(7);

// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     CommPage.sleep(5);
// })

describe('GCM MODULE:-- Matter Search', function () {
    it('Should Search Matter Throgh Multiple Filters', function () {
        // //Continue from Previous
        // CommPage.sleep(2);
        // CommPage.refresh();
        // CommPage.sleep(5);
        //Matter
        // HomePage.Matter.click();
        // CommPage.sleep(5);
        HomePage.Matter();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        GCMPage03.gcmSearch.click();
        HelperPage.waitForDisplay(VerifyPage.displayAllMatter_verify);
        GCMPage03.totalMatters();
        //Yesterday
        GCMPage03.selectDate.click();
        CommPage.sleep(1);
        GCMPage03.yesterday.click();
        GCMPage03.searchBtn.click();
        GCMPage03.yesterdayMatters();
        GCMPage03.clearBtn.click();
        //Last 7 Days
        GCMPage03.selectDate.click();
        GCMPage03.last7Days.click();
        GCMPage03.searchBtn.click();
        GCMPage03.last7DaysMatters();
        GCMPage03.clearBtn.click();
        //Last 30 Days
        GCMPage03.selectDate.click();
        GCMPage03.last30Days.click();
        GCMPage03.searchBtn.click();
        GCMPage03.last30DaysMatters();
        GCMPage03.clearBtn.click();
        //This Month
        GCMPage03.selectDate.click();
        GCMPage03.thisMonth.click();
        GCMPage03.searchBtn.click();
        GCMPage03.thisMonthMatters();
        GCMPage03.clearBtn.click();
        //Last Month 
        GCMPage03.selectDate.click();
        GCMPage03.lastMonth.click();
        GCMPage03.searchBtn.click();
        GCMPage03.lastMonthMatters();
        GCMPage03.clearBtn.click();
        //Last Year
        GCMPage03.selectDate.click();
        GCMPage03.lastYear.click();
        GCMPage03.searchBtn.click();
        GCMPage03.lastYearMatters();
        GCMPage03.clearBtn.click();
        //Active
        GCMPage03.active.click();
        GCMPage03.searchBtn.click();
        GCMPage03.activeMatters();
        GCMPage03.clearBtn.click();
        //Archived
        GCMPage03.archived.click();
        GCMPage03.searchBtn.click();
        GCMPage03.archivedMatters();
        GCMPage03.clearBtn.click();
        //Select by Group
        GCMPage03.group.click();
        GCMPage03.groupSelect.click();
        GCMPage03.searchBtn.click();
        GCMPage03.groupMatters();
        GCMPage03.clearBtn.click();
        //Select by Client
        GCMPage03.client.click();
        GCMPage03.clientSelect.click();
        GCMPage03.searchBtn.click();
        GCMPage03.clientMatters();
        GCMPage03.clearBtn.click();
        //Dropbox
        GCMPage03.dropbox.click();
        GCMPage03.searchBtn.click();
        GCMPage03.dropboxMatters();

        GCMPage03.dropbox.click();
        GCMPage03.searchBtn.click();
        GCMPage03.withoutDropboxMatters();

        GCMPage03.clearBtn.click();
        //Salesforce
        GCMPage03.salesforce.click();
        GCMPage03.searchBtn.click();
        GCMPage03.salesforceMatters();

        GCMPage03.salesforce.click();
        GCMPage03.searchBtn.click();
        GCMPage03.withoutSalesforceMatters();

        GCMPage03.clearBtn.click();
        //without Dropbox & Salesforce
        GCMPage03.doubleclickDropbox();
        GCMPage03.doubleclickSalesforce();
        GCMPage03.searchBtn.click();
        GCMPage03.withoutDropboxSalesforceMatters();

        GCMPage03.clearBtn.click();
        //Select by Employee
        GCMPage03.selectEmployee.sendKeys('Debasish');
        GCMPage01.firstOption.click();
        GCMPage03.searchBtn.click();
        GCMPage03.selectEmployeeMatters();

        GCMPage03.clearBtn.click();

        GCMPage03.selectEmployee.sendKeys('D');
        GCMPage01.firstOption.click();
        GCMPage03.searchBtn.click();
        GCMPage03.selectEmployeeMatters();

        GCMPage03.clearBtn.click().then(function () {
            console.log("Search Matter Worked Sucessfully")
            expect('Search Matter Worked Sucessfully').toEqual('Search Matter Worked Sucessfully');
        })
        CommPage.sleep(3);
    })
    it('Should Search Matter, Created By matter icon--370', function () {
        GCMPage01.gcm_plusIcon.click();
        CommPage.sleep(1);
        GCMPage01.iconMatterDropDown.click();
        CommPage.sleep(5);
        GCMPage01.matSelectGroup.click();
        GCMPage01.secondOption.click();
        CommPage.sleep(2);
        GCMPage01.matSelectClient.click();
        GCMPage01.secondOption.click();
        CommPage.sleep(2);
        GCMPage01.matterName.sendKeys('-Icon Matter-', R);
        CommPage.sleep(2)
        GCMPage01.selectMember.sendKeys('Debasish Patra');
        CommPage.sleep(3);
        GCMPage01.firstOption.click();
        GCMPage01.saveBtn.click().then(function () {
            console.log("Matter Created sucessfully")
        })
        CommPage.sleep(2);
        GCMPage03.matter.sendKeys('-Icon Matter-', R);
        GCMPage03.searchBtn.click();
        CommPage.sleep(1);
        GCMPage03.totalIconMatters();
        expect('Matter Searched sucessfully').toEqual('Matter Searched sucessfully');
    })
    it('Should Search Group Globally', function () {
        CommPage.refresh();
        CommPage.sleep(3);
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(3);
        GCMPage03.globalGroupSearch.click();
        GCMPage03.matterSearchInput.sendKeys('group').then(function () {
            console.log("Group Globally Searched Sucessfully")
            expect('Group Globally Searched Sucessfully').toEqual('Group Globally Searched Sucessfully');
        })
        CommPage.sleep(2);
    })
    it('Should Search Matter Globally', function () {
        CommPage.escBtn();
        //GCMPage03.firstMatterSearch.click();
        HomePage.Matter();
        CommPage.sleep(3);
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(3);
        GCMPage03.matterSearchInput.sendKeys('-Icon Matter-', R).then(function () {
            console.log("Matter Globally Searched Sucessfully")
            expect('Matter Globally Searched Sucessfully').toEqual('Matter Globally Searched Sucessfully');
        })
        CommPage.sleep(3);
    })
})
describe('GCM MODULE:-- Moving L2 and L3', function () {
    it('Should Create GCM Through Icon', function () {
        CommPage.escBtn();
        GCMPage01.gcm_plusIcon.click();
        CommPage.sleep(1);
        GCMPage01.iconGroupDropDown.click();
        CommPage.sleep(3);
        GCMPage01.groupName.sendKeys('-000001 Icon Group-' + R + '-');
        GCMPage01.saveBtn.click();
        CommPage.sleep(2);
        console.log('GROUP created sucessfully')
        expect('GROUP created sucessfully').toEqual('GROUP created sucessfully');
        GCMPage01.gcm_plusIcon.click();
        CommPage.sleep(1);
        GCMPage01.iconClientDropDown.click();
        CommPage.sleep(3);
        GCMPage01.matSelectGroup.click();
        GCMPage01.searchDropdown.sendKeys('-000001 Icon Group-' + R + '-');
        GCMPage01.secondOption.click();
        GCMPage01.clientName.sendKeys('-00001 Icon Client-' + R + '-');
        CommPage.sleep(1);
        GCMPage01.saveBtn.click();
        CommPage.sleep(2);
        console.log("CLIENT created sucessfully")
        expect('CLIENT created sucessfully').toEqual('CLIENT created sucessfully');
        CommPage.refresh();
        GCMPage01.gcm_plusIcon.click();
        CommPage.sleep(1);
        GCMPage01.iconMatterDropDown.click();
        CommPage.sleep(5);
        GCMPage01.matSelectGroup.click();
        GCMPage01.searchDropdown.sendKeys('-000001 Icon Group-' + R + '-');
        GCMPage01.secondOption.click();
        GCMPage01.matSelectClient.click();
        GCMPage01.searchDropdown.sendKeys('-00001 Icon Client-' + R + '-')
        GCMPage01.secondOption.click();
        GCMPage01.matterName.sendKeys('-001 Create Icon Matter-' + R + '-');
        CommPage.sleep(1);
        GCMPage01.selectMember.sendKeys('Debasish Patra');
        CommPage.sleep(3);
        GCMPage01.firstOption.click();
        CommPage.sleep(1);
        GCMPage01.saveBtn.click();
        CommPage.sleep(3);
        console.log("Matter Created sucessfully")
        expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
    })
    it('Should Move the L3 into another Group and Client',async function () {
        //CommPage.refresh();
        GCMPage01.firstClient.click();
        CommPage.sleep(1);
        GCMPage01.searchMatterValue.sendKeys('-001 Create Icon Matter-' + R + '-');
        CommPage.sleep(2);
        GCMPage01.firstMatter.click();
        CommPage.sleep(3);
        GCMPage01.editMatter.click();
        CommPage.sleep(5);
        GCMPage01.matSelectGroup.click();
        GCMPage01.thirdOption.click();
        GCMPage01.matSelectClient.click();
        GCMPage01.secondOption.click();
        GCMPage01.saveBtn.click();
        CommPage.sleep(1);
        console.log("Matter Updated Sucessfully")
        expect('Matter Updated Sucessfully').toEqual('Matter Updated Sucessfully');
        CommPage.refresh();
        HomePage.Matter();
        CommPage.sleep();
        GCMPage01.firstClient.click();
        expect(await VerifyPage.noActiveMatter_msg.isDisplayed()).toBeTruthy();
        HomePage.Matter();
        GCMPage01.mouseMoveFirst();
        GCMPage01.editGroup.click();
        CommPage.sleep(1);
        GCMPage01.groupName.clear().sendKeys("z-",R);
        await GCMPage01.saveBtn.click();
        console.log('Group Edited Successfully');
    })
    it('Should Export ALL the Matter', function () {
        HomePage.Matter();
        GCMPage04.bulkUpload_Shotcut.click();
        CommPage.sleep(2);
        GCMPage04.ExportAll.click();
        CommPage.sleep(3);
        console.log("Matter Exported Sucessfully")
        expect('Matter Exported Sucessfully').toEqual('Matter Exported Sucessfully');
    })
})
/*
    xit('Should Bulk Upload the Matter', function () {
        GCMPage03.clientClick.click();
        HomePage.Matter.click();
        CommPage.sleep(5);
    })
*/