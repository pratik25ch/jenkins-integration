var CommPage = require("../../lib/CommLib/comm_lib")
var VerifyPage = require("../../lib/actionLib/verify");
var HelperPage = require("../../utils/helperUtils/helper");
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")

let R = Math.random().toString(36).substring(7);
/**
 * 1- Should Create Matter without Task.
 * 2- Should Add & Edit Task.
 * 3- Should Add,Edit Description & Comment.
 * 4- Should Check Time vs Task.
 * 
 * 1- Should Create a new Expenses Data.
 * 2- Should Download the Expenses Data.
 * 3- Should Edit the Expenses Data.
 * 4- Should Check the METADATA.
 * 5- Should Print the Matter Data.
 */
// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     CommPage.sleep(3);
// })
describe('GCM MODULE:-- Task Section', function () {
    it('Should Create Matter without Task', async function () {
        HomePage.Matter();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        //Starting from Main
        await GCMPage01.firstClient.click();
        CommPage.sleep(2);
        // Continue from Previous
        // CommPage.back();
        // CommPage.sleep(2);
        GCMPage01.newMatterBtn.click();
        CommPage.sleep(5);
        GCMPage01.matterName.sendKeys("New Task Matter-", R);
        CommPage.sleep(3);
        GCMPage01.selectMember.sendKeys('Debasish Patra');
        CommPage.sleep(2)
        GCMPage01.firstOption.click();
        CommPage.sleep(2);
        GCMPage01.selectMember.sendKeys('Soham');
        CommPage.sleep(2);
        GCMPage01.firstOption.click();
        CommPage.sleep(1);
        await GCMPage01.saveBtn.click().then(function () {
            console.log("Matter Created sucessfully");
        })
        expect('Matter Created Successfully').toEqual('Matter Created Successfully');
        CommPage.sleep(2);
    })
    it('Should Add & Edit Task', async function () {
        // GCMPage02.firstClient.click();
        CommPage.refresh();
        CommPage.sleep(1);
        GCMPage01.searchMatterValue.sendKeys("New Task Matter-", R); // + R + '-'
        CommPage.sleep(2);
        GCMPage01.firstMatter.click();
        CommPage.sleep(1);
        expect(await VerifyPage.inside_Matter.isDisplayed()).toBeTruthy();
        //Task ADD
        GCMPage02.taskName.sendKeys('2nd Task Added');
        CommPage.sleep(1);
        GCMPage02.taskDescription.sendKeys('Check each & every functionality of Appplication');
        GCMPage02.addTask.click();
        CommPage.sleep(1);
        GCMPage02.taskName.sendKeys('3rd Task Added');
        GCMPage02.taskDescription.sendKeys('Full Regression Testing');
        GCMPage02.addTask.click();
        CommPage.sleep(0.5);
        GCMPage02.clickTaskName1.click();
        CommPage.sleep(0.5);
        GCMPage02.editTask.click();
        GCMPage02.firstTaskInput.clear().sendKeys('1st Task Added');
        GCMPage02.saveTask.click();
        GCMPage02.clickTaskName2.click();
        CommPage.sleep(0.5);
        GCMPage02.markAsActive.click().then(function () {
            console.log("Task Edited Successfully")
        })
        expect(await VerifyPage.secondTask_verify.isDisplayed()).toBeTruthy();
        CommPage.sleep(2);
    })
    it('Should Add,Edit Description & Comment ', async function () {
        //Description
        CommPage.sleep(2);
        GCMPage02.editDescBtn.click();
        GCMPage02.editDescInput.clear().sendKeys('Description added');
        GCMPage02.saveDescBtn.click();
        CommPage.sleep(1);
        expect(await VerifyPage.taskDesc_verify.isDisplayed()).toBeTruthy();
        //Add comment
        GCMPage02.comments.click();
        CommPage.sleep(1);
        GCMPage02.addComment.sendKeys('Comment Added');
        GCMPage02.submitComment.click();
        CommPage.sleep(0.5);
        expect(await VerifyPage.taskComment_verify.isDisplayed()).toBeTruthy();
        GCMPage02.editComment.click();
        GCMPage02.editCommentInput.clear().sendKeys('New Comment Added');
        GCMPage02.saveComment.click();
        CommPage.sleep(0.5);
        // GCMPage02.deleteComment.click();
        // CommPage.sleep(0.5);
        // GCMPage02.confirmDeleteComment.click().then(function () {
        //     console.log("Description & Comment Edited Successfully")
        // })
        // expect(await VerifyPage.deleteComment_verify.isDisplayed()).toBeTruthy();
    })
    it('Should Check Time vs Task', async function () {
        CommPage.sleep(1);
        GCMPage02.clickTaskName1.click();
        CommPage.sleep(0.5);
        GCMPage02.markAsActive.click();
        expect(await VerifyPage.firstTask_verify.isDisplayed()).toBeTruthy();
        CommPage.sleep(2);
        HomePage.Timesheet();
        CommPage.sleep(1);
        CommPage.refresh();
        CommPage.sleep(3);
        expect(await VerifyPage.timesheetPage_verify.isDisplayed()).toBeTruthy();
        TimesheetPage.manualEntryBtn();
        CommPage.sleep(2);
        expect(await VerifyPage.manualEntryPage_verify.isDisplayed()).toBeTruthy();
        TimesheetPage.selectMatter.click();
        TimesheetPage.searchMatter.sendKeys("New Task Matter-", R);
        TimesheetPage.firstEntry.click();
        TimesheetPage.manualEntryName.sendKeys('New Timesheet');
        TimesheetPage.validateBtn.click().then(function () {
            console.log('Timesheet Entry Sucessfully Added')
        })
        expect(await VerifyPage.createManualEntry_msg.isDisplayed()).toBeTruthy();
        CommPage.sleep(2);
        HomePage.Matter();
        GCMPage01.firstClient.click();
        CommPage.sleep(1);
        GCMPage01.searchMatterValue.sendKeys("New Task Matter-", R); // + R + '-'
        CommPage.sleep(2);
        GCMPage01.firstMatter.click().then(function () {
            console.log('Time Spent on Task Displayed Sucessfully')
        })
        CommPage.sleep(1);
        expect(VerifyPage.timeVsTask_verify.isPresent()).toBeTruthy();
    })
    it('Should Check Time mouseover "i" icon--389', async function () {
        GCMPage02.mouseMoveiIcon();
        CommPage.sleep(3);
        console.log("Time Duration Displayed Successfully on MouseOver");
        expect('Time Duration Displayed Successfully on MouseOver').toEqual('Time Duration Displayed Successfully on MouseOver');
    })
})
describe('GCM MODULE:-- Matter Expenses & Matter Details', function () {
    it('Should Create a new Expenses Data', async function () {
        // await GCMPage02.firstClient.click();
        // CommPage.sleep(3);
        // GCMPage02.firstMatter.click();
        // CommPage.sleep(5);
        GCMPage02.ExpensesBtn.click();
        CommPage.sleep(1);
        expect(await VerifyPage.emptyMatterExpenses_verify.isDisplayed()).toBeTruthy();
        GCMPage02.createExpenses.click();
        CommPage.sleep(0.5);
        // GCMPage02.member.click();
        // GCMPage02.firstMember.click();
        GCMPage02.expensesType.click();
        GCMPage02.communication.click();
        GCMPage02.expenseAmount.sendKeys('15000');
        GCMPage02.chooseDate.click();
        GCMPage02.TodaysDate.click();
        CommPage.sleep(1);
        await GCMPage02.saveExpenses.click().then(function () {
            console.log("Expenses Successfully Added");
        })
        expect(await VerifyPage.newExpenses_msg.isDisplayed()).toBeTruthy();
    })
    it('Should Download the Expenses Data', async function () {
        CommPage.sleep(1);
        await GCMPage02.downloadExpenses.click().then(function () {
            console.log("Expenses Downloaded Sucessfully");
        })
        expect("Matter Data Printed Sucessfully").toEqual("Matter Data Printed Sucessfully");
        CommPage.sleep(0.5);
    })
    it('Should Edit the Expenses Data', async function () {
        GCMPage02.editOption.click();
        CommPage.sleep(0.5);
        GCMPage02.editBtn.click();
        CommPage.sleep(0.5);
        GCMPage02.member.clear().sendKeys('Soham')
        GCMPage02.firstMember.click();
        GCMPage02.expensesType.clear().sendKeys('Fuel');
        GCMPage02.fuel.click();
        CommPage.sleep(1);
        await GCMPage02.saveExpenses.click().then(function () {
            console.log("Expenses Successfully Updated")
        })
        expect(await VerifyPage.updateExpenses_msg.isDisplayed()).toBeTruthy();
    })
    // it('Should Download the Attached expenses File', function () {

    // })
    it('Should Check the METADATA--585,626', async function () {
        //Details
        GCMPage02.details.click();
        CommPage.sleep(1);
        expect(await VerifyPage.matterDetails_verify.isDisplayed()).toBeTruthy();
        await GCMPage02.metaData.click().then(function () {
            console.log("Matter Data Displayed Sucessfully")
        })
        expect(await VerifyPage.matterDetails_verify.isDisplayed()).toBeTruthy();
        CommPage.sleep(2);
    })
    it('Should Print the Matter Data--585', async function () {
        //Print Matter Data
        GCMPage02.closeMetaData.click();
        CommPage.sleep(2);
        await GCMPage02.printDetails.click().then(function () {
            console.log("Matter Data Printed Sucessfully")
        })
        expect("Matter Data Printed Sucessfully").toEqual("Matter Data Printed Sucessfully");
        CommPage.sleep(1);
    })
})