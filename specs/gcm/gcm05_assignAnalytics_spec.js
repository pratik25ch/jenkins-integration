var CommPage = require("../../lib/CommLib/comm_lib")
var HelperPage = require("../../utils/helperUtils/helper")
var LoginPage = require("../../pages/login/login_po")
var VerifyPage = require("../../lib/actionLib/verify")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var GCMPage05 = require("../../pages/gcm/gcm05_assignAnalytics_po")
var TimesheetPage = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")

let R = Math.random().toString(36).substring(7);
/**
 * 1- Should Create Matter.
 * 2- Should assign member from shortcut (Plus) Icon.
 * 3- Should Create a Manual entry.
 * 4- Should Create a Manual entry for Other Member.
 * 5- Should Check the Matter Analytics, Timesheet is added or not.
 * 6- Should add new member and Remove the existing Member.
 * 7- Should display the relese updates.
 */
// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     CommPage.sleep(3);
// })

describe('GCM MODULE:-- Assign Member', function () {

    it("should Create a Matter", async function () {
        HomePage.Matter();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        await GCMPage01.firstClient.click();
        CommPage.sleep(2);
        GCMPage01.newMatterBtn.click();
        CommPage.sleep(5);
        GCMPage01.matterName.sendKeys("Assign Matter-", R);
        GCMPage01.selectMember.sendKeys('Debasish Patra');
        CommPage.sleep(2);
        GCMPage01.firstOption.click();
        CommPage.sleep(1);
        await GCMPage01.saveBtn.click().then(function () {
            console.log("Matter Created sucessfully");
            expect('Matter updated sucessfully').toEqual('Matter updated sucessfully');
        })
        CommPage.sleep(1);
    })
    it("should assign member from Plus Icon", function () {
        GCMPage01.viewMatter.click();
        CommPage.sleep(3);
        GCMPage05.memberAssignIcon.click();
        GCMPage05.searchMember.sendKeys('Rasika');
        GCMPage05.selectMember.click();
        CommPage.tabBtn();
        CommPage.sleep(2);
        GCMPage01.editMatter.click();
        CommPage.sleep(3);
        GCMPage01.saveBtn.click().then(function () {
            CommPage.sleep(2);
            console.log('Member Added Successfully')
        })
        expect(GCMPage05.secondAssociate.isDisplayed()).toBeTruthy();
    })
    it("should Create a Manual entry", function () {
        HomePage.Timesheet();
        CommPage.sleep(3);
        TimesheetPage.manualEntryBtn();
        CommPage.sleep(2);
        TimesheetPage.selectMatter.click();
        TimesheetPage.searchMatter.sendKeys("Assign Matter-", R);
        TimesheetPage.firstEntry.click();
        TimesheetPage.manualEntryName.sendKeys('T1');
        TimesheetPage.validateBtn.click();
        CommPage.sleep(2);
        console.log('Timesheet Entry Sucessfully Added')
        expect('Timesheet Entry Sucessfully Added').toEqual('Timesheet Entry Sucessfully Added');
    })
    it("should Create a Manual entry for Other Member", function () {
        CommPage.refresh();
        CommPage.sleep(2);
        TimesheetPage.manualEntryBtn();
        CommPage.sleep(2);
        TimesheetPage.associateName.clear();
        TimesheetPage.associateName.sendKeys('rasika@flowace.in');
        CommPage.sleep(0.5);
        TimesheetPage.firstOption.click();
        CommPage.sleep(1);
        TimesheetPage.selectMatter.click();
        TimesheetPage.searchMatter.sendKeys("Assign Matter-", R);
        TimesheetPage.firstEntry.click();
        TimesheetPage.manualEntryName.sendKeys('Timesheet Check');
        TimesheetPage.validateBtn.click();
        CommPage.sleep(2);
        console.log('Timesheet Entry Sucessfully Added');
        expect('Timesheet Entry Sucessfully Added').toEqual('Timesheet Entry Sucessfully Added');
    })
    it("should Check the Matter Analytics--551,620", function () {
        CommPage.sleep(1);
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(2);
        GCMPage03.matterSearchInput.sendKeys("Assign Matter-", R);
        CommPage.sleep(2);
        GCMPage03.firstMatterSearch.click();
        CommPage.sleep(2);
        GCMPage05.matterAnalytics.click();
        CommPage.sleep(3);
        console.log('Matter Analytics Displayed Successfully');
        expect(GCMPage05.analyticsData.isDisplayed()).toBeTruthy();
    })
    it("should Remove the Member ", function () {
        CommPage.sleep(2);
        GCMPage02.taskBtn.click();
        GCMPage05.memberAssignIcon.click();
        GCMPage05.searchMember.sendKeys('soham');
        GCMPage05.selectMember.click();
        CommPage.tabBtn();
        CommPage.sleep(2);
        GCMPage05.memberAssignIcon.click();
        GCMPage05.searchMember.sendKeys('niranjan');
        GCMPage05.selectMember.click();
        CommPage.tabBtn();
        CommPage.sleep(2);
        GCMPage05.memberAssignIcon.click();
        CommPage.sleep(1);
        GCMPage05.searchMember.sendKeys('Rasika');
        CommPage.sleep(1)
        GCMPage05.selectMember.click();
        CommPage.escBtn();
        CommPage.refresh();
        CommPage.sleep(3);
        expect(GCMPage05.activeAssociate.count()).toEqual(3);
        CommPage.sleep(3);
    })
    it("should display the release Updates", function () {
        var N1 = GCMPage05.notificationIcon.click();
        HelperPage.waitForPresent(N1);
        var N2 = GCMPage05.rightNotification.click();
        HelperPage.waitForPresent(N2);

        CommPage.escBtn();
    })

})