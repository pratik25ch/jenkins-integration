var CommPage = require("../../lib/CommLib/comm_lib")
var VerifyPage = require("../../lib/actionLib/verify")
var HelperPage = require("../../utils/helperUtils/helper")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");

let R = Math.random().toString(36).substring(7);
/**
 * 1- Should Create a Group.
 * 2- Should Display Error Message When entered Duplicate Group Name.
 * 3- Should Display Error Message When Group Name is Empty.
 * 4- Should Edit group.
 * 5- Should Create New Client.
 * 6- Should Display Error Message When entered Duplicate Client Name.
 * 7- Should Edit Client.
 * 8- Should add Matter.
 * 9- Should Display the matter though toast Message.
 * 10- Should Edit Matter.
 * 11- Should Archive Matter.
 * 12- Should UnArchive Matter.
 */
beforeAll(function () {
    // LoginPage.GetURL();
    LoginPage.login();
    CommPage.sleep(3);
})

/** 
 * afterAll(function () {
    LoginPage.signOut();
    CommPage.close();
    CommPage.sleep(3);
})
 */

describe('GCM MODULE :--- Create & Edit GCM', function () {
    it('Should Create a Group', async function () {
        HomePage.Matter();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        //New Group
        await GCMPage01.newGroup.click();
        //Group Name
        await GCMPage01.groupName.sendKeys("001 New Group-", R);
        await GCMPage01.groupTagContacts.sendKeys('Debasish Patra');
        CommPage.sleep(5);
        await GCMPage01.firstOption.click();
        CommPage.sleep(1);
        GCMPage01.placeType.click();
        CommPage.sleep(0.5);
        GCMPage01.firstOption.click();
        CommPage.sleep(1);
        GCMPage01.address.sendKeys('Millenium Bussiness Park');
        CommPage.sleep(2);
        CommPage.downArrow();
        CommPage.sleep(2);
        CommPage.enterBtn();
        CommPage.sleep(4)
        await GCMPage01.saveBtn.click().then(async function () {
            // expect("GROUP created sucessfully").toEqual("GROUP created sucessfully");
            console.log('GROUP created sucessfully');
        })
        CommPage.sleep(3);
    })
    it('Should Display Error Message When entered Duplicate Group Name', async function () {
        await GCMPage01.newGroup.click();
        GCMPage01.groupName.sendKeys("001 New Group-", R);
        await GCMPage01.saveBtn.click().then(function () {
            console.log('GROUP creation failed due to Group name already used')
        })
        // expect("Group name already used").toEqual("Group name already used");
        CommPage.sleep(2);
        GCMPage01.CancelBtn.click();
    })
    it('Should Display Error Message When Group Name is Empty', async function () {
        await GCMPage01.newGroup.click();
        GCMPage01.saveBtn.click().then(function () {
            console.log("Group name can't be Blank");
        })
        // expect(await VerifyPage.saveBtn_disabled.isEnabled()).toBe(false);
        //Click On Cancel Btn
        GCMPage01.CancelBtn.click();
        CommPage.sleep(2);
    })
    it('Should Edit group', async function () {
        CommPage.refresh();
        CommPage.sleep(2);
        GCMPage01.mouseMoveFirst();
        GCMPage01.editGroup.click();
        CommPage.sleep(1);
        GCMPage01.groupName.clear().sendKeys("001 Group Edit-", R);
        await GCMPage01.saveBtn.click().then(function () {
            console.log('GROUP updated sucessfully');
        })
        // expect("GROUP updated sucessfully").toEqual("GROUP updated sucessfully");
        CommPage.sleep(2);
    })
    it('Should Create New Client--631', async function () {
        CommPage.refresh();
        CommPage.sleep(2);
        GCMPage01.mouseMoveFirst();
        GCMPage01.newClientBtn.click();
        CommPage.sleep(2);
        GCMPage01.clientName.sendKeys("New Client-", R);
        GCMPage01.panCard.sendKeys('ABCDE1234J');
        GCMPage01.clientTagContacts.sendKeys('Soham');
        CommPage.sleep(3);
        GCMPage01.firstOption.click();
        CommPage.sleep(1);
        GCMPage01.placeType.click();
        CommPage.sleep(0.5)
        GCMPage01.firstOption.click();
        GCMPage01.address.sendKeys('Plot no -116 , 1st Floor , Millenium Bussiness Park , Navi Mumbai')
        await GCMPage01.saveBtn.click().then(function () {
            console.log("CLIENT created sucessfully")
        })
        // expect("CLIENT created sucessfully").toEqual("CLIENT created sucessfully");
        CommPage.sleep(2);
    })
    it('Should Display Error Message When entered Duplicate Client Name', async function () {
        GCMPage01.mouseMoveFirst();
        GCMPage01.newClientBtn.click();
        CommPage.sleep(2);
        GCMPage01.clientName.sendKeys("New Client-", R);
        await GCMPage01.saveBtn.click().then(function () {
            console.log('CLIENT creation failed due to Client name already used')
        })
        // expect("Client name already used").toEqual("Client name already used");
        CommPage.sleep(1);
    })
    it('Should Edit Client', async function () {
        await GCMPage01.firstClient.click();
        GCMPage01.editClient.click();
        CommPage.sleep(3);
        GCMPage01.clientName.clear().sendKeys("Edit Client-", R);
        GCMPage01.clientTagContacts.sendKeys('Debasish Patra');
        CommPage.sleep(5);
        GCMPage01.firstOption.click();
        CommPage.sleep(2);

        await GCMPage01.saveBtn.click().then(function () {
            console.log("CLIENT updated Sucessfully");
        })
        // expect("CLIENT updated Sucessfully").toContain("CLIENT updated Sucessfully");
        CommPage.sleep(3);
    })
    it('Should Add Matter', async function () {
        CommPage.back();
        GCMPage01.firstClient.click();
        GCMPage01.newMatterBtn.click();
        CommPage.sleep(5);
        await GCMPage01.matterName.sendKeys("New Matter-", R);
        CommPage.sleep(1);
        GCMPage01.selectMember.sendKeys('Debasish Patra');
        CommPage.sleep(3);
        GCMPage01.firstOption.click();
        GCMPage01.templateOption.click();
        GCMPage01.categoryName.click();
        CommPage.sleep(0.5);
        GCMPage01.firstOption.click();
        CommPage.sleep(1);
        GCMPage01.templateName.click();
        CommPage.sleep(0.5);
        GCMPage01.firstOption.click();
        CommPage.sleep(2);
        GCMPage01.fees.click();
        GCMPage01.quotedfees.sendKeys('1000');
        GCMPage01.contacts.click();
        GCMPage01.tagContacts.sendKeys('Debasish Patra');
        CommPage.sleep(5);
        GCMPage01.firstOption.click();
        CommPage.sleep(2);
        GCMPage01.placeType.click();
        CommPage.sleep(0.5)
        GCMPage01.firstOption.click();
        GCMPage01.address.sendKeys('Plot no -116 , 1st Floor , Millenium Bussiness Park , Navi Mumbai');
        await GCMPage01.saveBtn.click().then(function () {
            console.log("Matter Created sucessfully")
        })
        //expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
        expect(VerifyPage.newMatter_msg.getText()).toContain('created Successfully');
        CommPage.sleep(1);
    })
    it('Should Display the New Matter', async function () {
        await GCMPage01.viewMatter.click().then(function () {
            console.log("New Matter Displayed sucessfully")
        })
        // expect(await VerifyPage.inside_Matter.isPresent()).toBeTruthy();
        CommPage.sleep(5);
    })
    it('Should Edit Matter--624', async function () {
        CommPage.back();
        GCMPage01.firstMatter.click();
        CommPage.sleep(3);
        GCMPage01.editMatter.click();
        CommPage.sleep(2);
        GCMPage01.matterName.clear();
        CommPage.sleep(2);
        GCMPage01.matterName.sendKeys("Edit Matter-", R);
        GCMPage01.removeMember.click();
        GCMPage01.selectMember.sendKeys('Soham');
        CommPage.sleep(5);
        GCMPage01.firstOption.click();
        await GCMPage01.saveBtn.click().then(function () {
            console.log("Matter updated sucessfully");
        })
        expect("Matter updated sucessfully").toEqual("Matter updated sucessfully");
        CommPage.sleep(2);
    })
    it('Should Archive Matter', async function () {
        CommPage.back();
        GCMPage01.firstMatter.click();
        GCMPage01.archiveBtn.click();
        await GCMPage01.confirmBtn.click().then(function () {
            console.log("Matter archived sucessfully");
        })
        expect(await GCMPage01.unarchiveBtn.isDisplayed()).toBeTruthy();
        CommPage.sleep(2);
    })
    it('Should UnArchive Matter', async function () {
        CommPage.back();
        GCMPage01.closedBtn.click();
        GCMPage01.firstMatter.click();
        GCMPage01.unarchiveBtn.click();
        await GCMPage01.confirmBtn.click().then(function () {
            console.log("Matter unarchived sucessfully");
        })
        expect(await GCMPage01.archiveBtn.isDisplayed()).toBeTruthy();
        CommPage.sleep(2);
    })

    // xit('Demo ignore this spec', async function () {

    //     CommPage.sleep(3);
    //     //browser.executeScript("window.scrollBy(0,300)")
    //     browser.executeScript("document.getElementById('group-name-9').scrollIntoView(true)");
    //     CommPage.sleep(5);
    // })
})