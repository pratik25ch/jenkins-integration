var GCMData = require("../../utils/jsonUtils/gcm.json")
var CommPage = require("../../lib/CommLib/comm_lib")
var HelperPage = require("../../utils/helperUtils/helper")
var VerifyPage = require("../../lib/actionLib/verify")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")

let R = Math.random().toString(36).substring(7);
/**
 * 1- Should Create a Group Through Icon.
 * 2- Should Create a Client Through Icon.
 * 3- Should Add Matter.
 * 4- Should Delete Matter.
 * 5- Should Delete Client.
 * 6- Should Delete Group.
 * 
 * 1- Should Create a Group Through Icon.
 * 2- Should Create a Client, through Toast Message.
 * 3- Should Add Matter.
 * 4- Should Archive Matter.
 * 5- Should Delete Arvhive Matter with Message.
 * 6- Should Delete Client with message.
 * 7- Should Delete Group with message.
 * 
 * 1- L1 should allow to delete with blank L2--431
 * 
 * 1- Should Bulk upload GCM.
 * 
 */
// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     CommPage.sleep(5);
// })

describe('GCM MODULE:1st TC Crete through Icon,Delete L1& L2', function () {
    it('Should Create a Group Through Icon', async function () {
        HomePage.Matter();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        GCMPage01.gcm_plusIcon.click();
        CommPage.sleep(1);
        GCMPage01.iconGroupDropDown.click();
        await GCMPage01.groupName.sendKeys("First Delete Group-", R);
        await GCMPage01.groupTagContacts.sendKeys('Debasish Patra');
        CommPage.sleep(5);
        await GCMPage01.firstOption.click();
        await GCMPage01.saveBtn.click().then(function () {
            console.log('GROUP created sucessfully')
        })
        expect(VerifyPage.newGroup_msg.getText()).toContain("created Successfully");
        CommPage.sleep(2);
    })
    it('Should Create a Client Through Icon', async function () {
        GCMPage01.gcm_plusIcon.click();
        CommPage.sleep(1);
        GCMPage01.iconClientDropDown.click();
        CommPage.sleep(1);
        GCMPage01.matSelectGroup.click();
        GCMPage01.searchDropdown.sendKeys("First Delete Group-", R);
        GCMPage01.secondOption.click();
        CommPage.sleep(1);
        GCMPage01.clientName.sendKeys("First Delete Client-", R);
        GCMPage01.panCard.sendKeys('ABCDE1234J');
        await GCMPage01.saveBtn.click().then(function () {
            console.log("CLIENT created sucessfully");
        })
        expect(VerifyPage.newClient_msg.getText()).toContain('created Successfully');
        CommPage.sleep(2);
    })
    it('Should Add Matter', async function () {
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(2);
        GCMPage03.globalClientSearch.click();
        GCMPage03.matterSearchInput.sendKeys("First Delete Client-", R);
        CommPage.sleep(2);
        GCMPage03.globalSearch_firstClient.click();
        CommPage.sleep(2);
        GCMPage01.newMatterBtn.click();
        CommPage.sleep(5);
        await GCMPage01.matterName.sendKeys("first Delete Matter-", R);
        CommPage.sleep(3);
        GCMPage01.selectMember.sendKeys('Debasish Patra');
        CommPage.sleep(3);
        GCMPage01.firstOption.click();

        await GCMPage01.saveBtn.click().then(function () {
            console.log("Matter Created sucessfully")
        })
        expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
        CommPage.sleep(1);
    })
    it('Should Delete Matter--593', async function () {
        GCMPage01.firstMatter.click();
        CommPage.sleep(3);
        GCMPage04.deleteTooltip.click();
        CommPage.sleep(2)
        GCMPage04.deleteMatterBtn.click();
        CommPage.sleep(2);
        GCMPage04.deleteMatterData.click();
        GCMPage04.validateBtn.click();
        CommPage.sleep(1);
        GCMPage04.validateBtn.click();
        CommPage.sleep(2);
        GCMPage04.confirmMatterName.sendKeys("first Delete Matter-", R)
        CommPage.sleep(2);
        GCMPage04.validateBtn.click().then(function () {
            console.log("MATTER Deleted sucessfully");
        })
        expect(VerifyPage.deleteMatter_msg.isDisplayed()).toBeTruthy();
        CommPage.sleep(2);
    })
    it('Should Delete a Client--625', async function () {
        CommPage.refresh();
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(2);
        GCMPage03.globalClientSearch.click();
        GCMPage03.matterSearchInput.sendKeys("First Delete Client-", R);
        CommPage.sleep(2);
        GCMPage03.globalSearch_firstClient.click();
        GCMPage01.editClient.click();
        CommPage.sleep(3);
        GCMPage04.deleteBtn.click();
        CommPage.sleep(1);
        GCMPage04.deleteBtn.click().then(function () {
            console.log("Client Deleted sucessfully")
        })
        expect(VerifyPage.deleteClient_msg.isDisplayed()).toBeTruthy();
        CommPage.sleep(2);
    })
    it('Should Delete the Group', async function () {
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(2);
        GCMPage03.globalGroupSearch.click();
        GCMPage03.matterSearchInput.sendKeys("first Delete Group-", R);
        GCMPage01.globalSearchMouseMoveFirst();
        GCMPage01.editGlobalGroup.click();
        CommPage.sleep(1);
        GCMPage04.deleteBtn.click();
        CommPage.sleep(1);
        GCMPage04.deleteBtn.click().then(function () {
            console.log("Group Deleted sucessfully")
        })
        expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
        CommPage.sleep(4);
        CommPage.refresh();
        CommPage.sleep(3);
    })
})
describe('GCM MODULE:2nd TC-Crete through Icon,Delete L1& L2', function () {
    it('Should Create a Group Through Icon', async function () {
        GCMPage01.gcm_plusIcon.click();
        CommPage.sleep(1);
        GCMPage01.iconGroupDropDown.click();
        await GCMPage01.groupName.sendKeys("Second Delete Group-", R);
        await GCMPage01.groupTagContacts.sendKeys('Debasish Patra');
        CommPage.sleep(2);
        await GCMPage01.firstOption.click();
        await GCMPage01.saveBtn.click().then(function () {
            console.log('GROUP created sucessfully')
        })
        expect(VerifyPage.newGroup_msg.getText()).toContain("created Successfully");
        CommPage.sleep(2);
    })
    it('Should Create a Client Through Toast Message--569', async function () {
        GCMPage01.createClient_Msg.click();
        CommPage.sleep(1);
        GCMPage01.clientName.sendKeys("Second Delete Client-", R);
        GCMPage01.panCard.sendKeys('ABCDE1234J');
        await GCMPage01.saveBtn.click().then(function () {
            console.log("CLIENT created sucessfully")
        })
        expect(VerifyPage.newClient_msg.getText()).toContain('created Successfully');
        CommPage.sleep(2);
    })
    it('Should Add Matter', async function () {
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(2);
        GCMPage03.globalClientSearch.click();
        GCMPage03.matterSearchInput.sendKeys("Second Delete Client-", R);
        CommPage.sleep(2);
        GCMPage03.globalSearch_firstClient.click();
        CommPage.sleep(2);
        GCMPage01.newMatterBtn.click();
        CommPage.sleep(5);
        await GCMPage01.matterName.sendKeys("Second Delete Matter-", R);
        CommPage.sleep(3);
        GCMPage01.selectMember.sendKeys('Debasish Patra');
        CommPage.sleep(2);
        GCMPage01.firstOption.click();

        await GCMPage01.saveBtn.click().then(function () {
            console.log("MATTER Deleted sucessfully");
        })
        expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
        CommPage.sleep(1);
    })
    it('Should Archive Matter', async function () {
        GCMPage01.firstMatter.click();
        CommPage.sleep(3);
        GCMPage04.deleteTooltip.click();
        CommPage.sleep(2)
        GCMPage04.deleteMatterBtn.click();
        CommPage.sleep(2);
        GCMPage04.validateBtn.click().then(function () {
            console.log("Matter Archived sucessfully")
        })
        CommPage.sleep(3);
        expect(await GCMPage01.unarchiveBtn.isDisplayed()).toBeTruthy();
    })
    it('Should Delete the Archive Matter', async function () {
        GCMPage04.deleteTooltip.click();
        CommPage.sleep(2)
        GCMPage04.deleteMatterBtn.click();
        CommPage.sleep(2);
        GCMPage04.deleteMatterData.click();
        GCMPage04.validateBtn.click();
        CommPage.sleep(1);
        GCMPage04.validateBtn.click();
        CommPage.sleep(2);
        GCMPage04.confirmMatterName.sendKeys("Second Delete Matter-", R)
        CommPage.sleep(2);
        GCMPage04.deletionReason.sendKeys('As Discussed with Soham: Please Delete')
        CommPage.sleep(2);
        GCMPage04.validateBtn.click().then(function () {
            console.log("MATTER Deleted sucessfully");
        })
        expect(VerifyPage.deleteMatter_msg.isDisplayed()).toBeTruthy();
        CommPage.sleep(2);
    })
    it('Should Delete a Client', async function () {
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(2);
        GCMPage03.globalClientSearch.click();
        GCMPage03.matterSearchInput.sendKeys("Second Delete Client-", R);
        CommPage.sleep(2);
        GCMPage03.globalSearch_firstClient.click();
        GCMPage01.editClient.click();
        CommPage.sleep(3);
        GCMPage04.deleteBtn.click();
        CommPage.sleep(1);
        GCMPage04.reasonDeletion_Client.sendKeys("As Discussed with Soham: Please Delete");
        CommPage.sleep(1);
        GCMPage04.deleteBtn.click().then(function () {
            console.log("Client Deleted sucessfully")
        })
        expect(VerifyPage.deleteClient_msg.isDisplayed()).toBeTruthy();
        CommPage.sleep(2);
        CommPage.refresh();
    })
    it('Should Delete the Group', async function () {
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(2);
        GCMPage03.globalGroupSearch.click();
        GCMPage03.matterSearchInput.sendKeys("Second Delete Group-", R);
        GCMPage01.globalSearchMouseMoveFirst();
        GCMPage01.editGlobalGroup.click();
        CommPage.sleep(1);
        GCMPage04.deleteBtn.click();
        CommPage.sleep(2);
        GCMPage04.reasonDeletion_Group.sendKeys("As Discussed with Soham: Please Delete");
        CommPage.sleep(1);
        GCMPage04.deleteBtn.click().then(function () {
            console.log("Group Deleted sucessfully")
        })
        expect(VerifyPage.deleteGroup_msg.isDisplayed()).toBeTruthy();
        CommPage.sleep(4);
        CommPage.refresh();
        CommPage.sleep(3);
    })
})
describe('GCM Deletion:-- Delete L1, If L2 is empty', function () {
    it('L1 should allow to delete with blank L2--431', async function () {
        await GCMPage01.newGroup.click();
        CommPage.sleep(2);
        await GCMPage01.groupName.sendKeys("delete L1-", R);
        await GCMPage01.saveBtn.click().then(function () {
            expect(VerifyPage.newGroup_msg.getText()).toContain("created Successfully");
            console.log('GROUP created sucessfully');
        })
        CommPage.sleep(2);
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(2);
        GCMPage03.globalGroupSearch.click();
        GCMPage03.matterSearchInput.sendKeys("delete L1-", R);
        GCMPage01.globalSearchMouseMoveFirst();
        GCMPage03.createClientGlobalSearch.click();
        //Create Client
        GCMPage01.clientName.sendKeys("delete L2-", R);
        await GCMPage01.saveBtn.click().then(function () {
            console.log("CLIENT created sucessfully")
        })
        expect(VerifyPage.newClient_msg.getText()).toContain('created Successfully');
        CommPage.sleep(2);
        //global Search & Delete
        CommPage.refresh();
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(2);
        GCMPage03.globalGroupSearch.click();
        GCMPage03.matterSearchInput.sendKeys("delete L1-", R);
        GCMPage01.globalSearchMouseMoveFirst();
        GCMPage01.editGlobalGroup.click();
        CommPage.sleep(1);
        GCMPage04.deleteBtn.click();
        CommPage.sleep(2);
        //Delete Group
        CommPage.sleep(1);
        GCMPage04.deleteBtn.click().then(function () {
            console.log("Group Deleted sucessfully")
        })
        expect(VerifyPage.deleteMatter_msg.isDisplayed()).toBeTruthy();
        CommPage.sleep(1);
        CommPage.escBtn();
        CommPage.refresh();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
    })
})
xdescribe('GCM Deletion: Blank Group Delete ', function () {
    it('Check The Blank Group & Delete Group', function () {
        HomePage.Matter();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        CommPage.sleep(3);
        GCMPage04.deleteBlankGroup();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        GCMPage04.deleteBlankGroup();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        GCMPage04.deleteBlankGroup();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        GCMPage04.deleteBlankGroup();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        GCMPage04.deleteBlankGroup();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        GCMPage04.deleteBlankGroup();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        












    })

















})

xdescribe('GCM MODULE:-- Bulk Upload', function () {
    xit('Should Bulk Upload GCM ', async function () {
        GCMPage04.bulkUpload_Shotcut.click();
        CommPage.sleep(0.5);
        GCMPage04.bulkUploadBtn.click();
        CommPage.sleep(1);
        GCMPage04.uploadFile.click();
    })
})