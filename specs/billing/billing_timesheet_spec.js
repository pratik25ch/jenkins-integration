var CommPage = require("../../lib/CommLib/comm_lib")
var VerifyPage = require("../../lib/actionLib/verify")
var HelperPage = require("../../utils/helperUtils/helper")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var TimesheetPage01 = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")
var ReportingPage = require("../../pages/reporting/reporting_po")
var AnalyticsPage01 = require("../../pages/analytics/analytics01_matterAnalytics_po")
var AnalyticsPage02 = require("../../pages/analytics/analytics02_financialAnalytics_po")

let R = Math.random().toString(36).substring(7);
/**
 * 1- Should Create a New Matter
 * 2- Should Add Manual entry.
 * 3- Should Search the matter on billing Page
 * 4- Should Create NEW Invoice. (Before 3 Min)
 * 5- Should Add amount on Invoice
 * 6- Should Mark As Completed the invoice
 * 7- Should Add Manual entry.
 * 8- Should Search the matter on billing Page
 * 9- Should Create NEW Invoice. (Same Time)
 * 10- Should Add amount on Invoice.
 * 11- Should Mark As Completed the invoice
 * 12- Should Add Manual entry.(Error Message Displayed)
 *
 */
beforeAll(function () {
    LoginPage.GetURL();
    LoginPage.login();
    CommPage.sleep(3);
    //HomePage.Billing.click();
    //CommPage.sleep(3);
})
describe('Billing Module:--Timesheet Integration', function () {
    it('Should Create a New Matter', async function () {
        HomePage.Matter();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        GCMPage01.firstClient.click();
        CommPage.sleep(1);
        GCMPage01.newMatterBtn.click();
        CommPage.sleep(1);
        GCMPage01.matterName.sendKeys("Billing-Timesheet-",R);
        CommPage.sleep(5);
        GCMPage01.selectMember.sendKeys('Debasish Patra');
        CommPage.sleep(3);
        GCMPage01.firstOption.click();
        GCMPage01.saveBtn.click();
        CommPage.sleep(2);
        console.log("Matter Created sucessfully")
        expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
    })
    it('Should Add Manual Entry', async function () {
        HomePage.Timesheet();
        CommPage.sleep(5);
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(2);
        TimesheetPage01.selectMatter.click();
        CommPage.sleep(0.5);
        TimesheetPage01.searchMatter.sendKeys("Billing-Timesheet-",R)
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('BILLLING-Timesheet');
        TimesheetPage01.validateBtn.click();
        CommPage.sleep(2);
        console.log('Timesheet Entry Sucessfully Added')
        expect('Timesheet Entry Sucessfully Added').toEqual('Timesheet Entry Sucessfully Added');
    })
    it('Should Search the matter on billing Page', async function () {
        HomePage.Billing();
        CommPage.sleep(3);
        BillingPage.hideZeroAmount.click();
        BillingPage.billingsSearchMatter.sendKeys("Billing-Timesheet-",R);
        CommPage.sleep(2);
        console.log('Matter Search worked Successfully')
        expect('Matter Search worked Successfully').toEqual('Matter Search worked Successfully');
    })
    it('Should Create NEW Invoice--403', async function () {
        CommPage.refresh();
        CommPage.sleep(3);
        BillingPage.newInvoice.click();
        CommPage.sleep(3);
        BillingPage.selectMatter_CreateInvoice.click();
        CommPage.sleep(3);
        BillingPage.searchMatter_CreateInvoice.sendKeys("Billing-Timesheet-",R);
        BillingPage.firstOption_CreateInvoice.click();
        CommPage.sleep(2);
        BillingPage.billing_endDate.click();
        CommPage.sleep(2);
        BillingPage.billing_endMin.click();
        BillingPage.billing_endMin.click();
        BillingPage.billing_endMin.click();
        BillingPage.billing_setTime.click();
        CommPage.sleep(2);
        BillingPage.outOfPocketExpenses.sendKeys(4000);
        CommPage.sleep(1);
        BillingPage.professionalFee.sendKeys(5000);
        CommPage.sleep(0.5);
        BillingPage.createInvoice.click();
        CommPage.sleep(5);
        console.log('Invoice generated Successfully')
        expect('Invoice generated Successfully').toEqual('Invoice generated Successfully');
    })
    it('Should Add amount on Invoice', async function () {
        BillingPage.billingsSearchMatter.clear().sendKeys("Billing-Timesheet-",R);
        CommPage.sleep(1);
        BillingPage.firstMatter_UpdateInvoice.click();
        CommPage.sleep(3);
        BillingPage.amount.sendKeys('3000');
        BillingPage.proof.sendKeys('Communication');
        BillingPage.addAmount.click();
        CommPage.sleep(1);
        BillingPage.amount.sendKeys('4000');
        BillingPage.proof.sendKeys('Fuel');
        BillingPage.addAmount.click();
        CommPage.sleep(1);
        BillingPage.updateInvoice.click()
        CommPage.sleep(0.5);
        console.log('Invoice updated Successfully')
        expect('Invoice updated Successfully').toEqual('Invoice updated Successfully');
    })
    it('Should Mark As Completed the invoice', async function () {
        BillingPage.billingsSearchMatter.clear().sendKeys("Billing-Timesheet-",R);
        CommPage.sleep(1);
        BillingPage.firstMatter_UpdateInvoice.click();
        CommPage.sleep(3);
        BillingPage.markAsCompleted.click();
        CommPage.sleep(1);
        BillingPage.updateInvoice.click()
        CommPage.sleep(0.5);
        console.log('Invoice updated Successfully')
        expect('Invoice updated Successfully').toEqual('Invoice updated Successfully');
    })
    it('Should Add Manual Entry', async function () {
        HomePage.Timesheet();
        CommPage.sleep(3);
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(2);
        TimesheetPage01.selectMatter.click();
        CommPage.sleep(0.5);
        TimesheetPage01.searchMatter.sendKeys("Billing-Timesheet-",R)
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('BILLLING-Timesheet-Again');
        TimesheetPage01.validateBtn.click();
        CommPage.sleep(2);
        console.log('Timesheet Entry Sucessfully Added')
        expect('Timesheet Entry Sucessfully Added').toEqual('Timesheet Entry Sucessfully Added');
    })
    it('Should Search the matter on billing Page', async function () {
        HomePage.Billing();
        CommPage.refresh();
        CommPage.sleep(3);
        BillingPage.hideZeroAmount.click();
        BillingPage.billingsSearchMatter.sendKeys("Billing-Timesheet-",R);
        CommPage.sleep(2);
        console.log('Matter Search worked Successfully')
        expect('Matter Search worked Successfully').toEqual('Matter Search worked Successfully');
    })
    it('Should Create NEW Invoice', async function () {
        CommPage.refresh();
        CommPage.sleep(3);
        BillingPage.newInvoice.click();
        CommPage.sleep(1);
        BillingPage.selectMatter_CreateInvoice.click();
        CommPage.sleep(3);
        BillingPage.searchMatter_CreateInvoice.sendKeys("Billing-Timesheet-",R);
        BillingPage.firstOption_CreateInvoice.click();
        BillingPage.outOfPocketExpenses.sendKeys(4000);
        CommPage.sleep(1);
        BillingPage.professionalFee.sendKeys(5000);
        CommPage.sleep(0.5);
        BillingPage.createInvoice.click();
        CommPage.sleep(5);
        console.log('Invoice generated Successfully')
        expect('Invoice generated Successfully').toEqual('Invoice generated Successfully');
    })
    it('Should Add amount on Invoice', async function () {
        BillingPage.billingsSearchMatter.clear().sendKeys("Billing-Timesheet-",R);
        CommPage.sleep(1);
        BillingPage.firstMatter_UpdateInvoice.click();
        CommPage.sleep(3);
        BillingPage.amount.sendKeys('3000');
        BillingPage.proof.sendKeys('Communication');
        BillingPage.addAmount.click();
        CommPage.sleep(1);
        BillingPage.amount.sendKeys('4000');
        BillingPage.proof.sendKeys('Fuel');
        BillingPage.addAmount.click();
        CommPage.sleep(1);
        BillingPage.updateInvoice.click();
        CommPage.sleep(0.5);
        console.log('Invoice updated Successfully')
        expect('Invoice updated Successfully').toEqual('Invoice updated Successfully');
    })
    it('Should Mark As Completed the invoice', async function () {
        BillingPage.billingsSearchMatter.clear().sendKeys("Billing-Timesheet-",R);
        CommPage.sleep(1);
        BillingPage.firstMatter_UpdateInvoice.click();
        CommPage.sleep(3);
        BillingPage.markAsCompleted.click();
        CommPage.sleep(1);
        BillingPage.updateInvoice.click()
        CommPage.sleep(0.5);
        console.log('Invoice updated Successfully')
        expect('Invoice updated Successfully').toEqual('Invoice updated Successfully');
    })
    it('Should check the error messsage of timesheet matter, billing completed', async function () {
        HomePage.Timesheet();
        CommPage.sleep(3);
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.searchMatter.sendKeys("Billing-Timesheet-",R)
        TimesheetPage01.firstEntry.click();
        CommPage.sleep(3)
        TimesheetPage01.cancelBtn.click();
        console.log('Timesheet error message displayed')
        expect('Timesheet error message displayed').toEqual('Timesheet error message displayed')
    })
})
