var CommPage = require("../../lib/CommLib/comm_lib")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var TimesheetPage01 = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")
var ReportingPage = require("../../pages/reporting/reporting_po")
var AanlyticsPage01 = require("../../pages/analytics/analytics01_matterAnalytics_po")
var AanlyticsPage02 = require("../../pages/analytics/analytics02_financialAnalytics_po")
var DropBoxPage = require("../../components/DroboxPO/dropbox_po")

let R = Math.random().toString(36).substring(7);

/**
 * 1- Should Hide Zero Amounts Enabled.
 * 2- Should Hide Zero Amounts Disabled.    
 * 3- Should Filter the matter.
 * 
 * 1- Should Create a New Matter.
 * 2- Should Add Manual entry.
 * 3- Should Search the matter on billing Page.
 *
 * 1- Should Generate the Invoice.
 * 2- Should Update the Invoice(Skipped).
 * 3- Should Add amount on Invoice.
 * 4- Should Download the Updated Invoice.
 * 5- Should Mark As Completed the invoice.
 * 6- Should move to Completed Invoice.
 * 7- Should Customize the columns of Completed Invoice.
 * 
 * 1- Should Check On Financial Analytics.
 * 2- Should Check On Financial Analytics Graph.
 * 3- Should Check On Financial Analytics Table.
 * 4- Should the amount in Billing table against same matter.
 * 5- Should check the error messsage of timesheet matter, billing completed.
 * 
 * 1- Should check the matter shared with dropbox.
 * 
 */

beforeAll(function () {
    LoginPage.GetURL();
    LoginPage.login();
    CommPage.sleep(3);
    HomePage.Billing();
    CommPage.sleep(1);
})
describe('BILLING Module:-- Filter Btn & Hide Zero Amounts', function () {
    it('Should Hide Zero Amounts Enabled', async function () {
        BillingPage.hideZeroAmount.click();
        CommPage.sleep(2);
        console.log("Hide Zero Amounts Enbled");
        expect("Hide Zero Amounts Selected Sucessfully").toEqual("Hide Zero Amounts Selected Sucessfully");
    })
    it('Should Hide Zero Amounts Disabled', async function () {
        BillingPage.hideZeroAmount.click();
        CommPage.sleep(2);
        console.log("Hide Zero Amounts Disabled");
        expect("Hide Zero Amounts Disabled").toEqual("Hide Zero Amounts Disabled");
    })
    it('Should Filter the matter', async function () {
        BillingPage.filterBtn.click();
        CommPage.sleep(1);
        BillingPage.closedMatter.click();
        CommPage.sleep(3);
        console.log("All the Closed Matter Displayed");
        expect("Active Matter Displayed").toEqual("Active Matter Displayed");
        BillingPage.filterBtn.click();
        CommPage.sleep(1);
        BillingPage.activeMatter.click();
        CommPage.sleep(3);
        console.log("All the Active Matter Displayed");
    })
})
describe('BILLING Module:--Add Timesheet for Generating Invoice ', function () {
    it('Should Create a New Matter', async function () {
        HomePage.Matter();
        GCMPage01.firstClient.click();
        CommPage.sleep(1);
        GCMPage01.newMatterBtn.click();
        CommPage.sleep(1);
        GCMPage01.matterName.sendKeys('-01 Billing Automation Test-' + R + '-');
        CommPage.sleep(5);
        GCMPage01.selectMember.sendKeys('Debasish Patra');
        CommPage.sleep(3);
        GCMPage01.firstOption.click();
        GCMPage01.saveBtn.click();
        CommPage.sleep(2);
        console.log("Matter Created sucessfully")
        expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
    })
    it('Should Add Manual entry', async function () {
        HomePage.Timesheet();
        CommPage.sleep(3);
        //CommPage.refresh();
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(2);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.searchMatter.sendKeys('-01 Billing Automation Test-' + R + '-')
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('New Timesheet');
        TimesheetPage01.validateBtn.click();
        CommPage.sleep(2);
        console.log('Timesheet Entry Sucessfully Added')
        expect('Timesheet Entry Sucessfully Added').toEqual('Timesheet Entry Sucessfully Added');
    })
    it('Should Search the matter on billing Page', async function () {
        HomePage.Billing();
        CommPage.sleep(5);
        CommPage.refresh();
        BillingPage.hideZeroAmount.click();
        BillingPage.billingsSearchMatter.sendKeys('-01 Billing Automation Test-' + R + '-');
        CommPage.sleep(2);
        console.log('Matter Search worked Successfully')
        expect('Matter Search worked Successfully').toEqual('Matter Search worked Successfully');
    })
})
describe('BILLING Module:--New Generate & Update Invoice', function () {
    it('Should Generate the Invoice', async function () {
        CommPage.sleep(1);
        BillingPage.hideZeroAmount.click();
        BillingPage.firstMatter_Outstanding.click();
        BillingPage.generatingInvoice.click();
        CommPage.sleep(3);
        BillingPage.invoiceId.sendKeys(R)
        CommPage.sleep(1);
        BillingPage.perticularTitle.sendKeys("No Title");
        CommPage.sleep(1);
        BillingPage.particularCost.sendKeys('500');
        CommPage.sleep(1);
        BillingPage.addCost.click();
        CommPage.sleep();
        BillingPage.outOfPocketExpenses.sendKeys(4000);
        CommPage.sleep(1);
        // BillingPage.professionalFee.sendKeys(5000);
        // CommPage.sleep(0.5);
        BillingPage.createInvoice.click();
        CommPage.sleep(5);
        console.log('Invoice generated Successfully')
        expect('Invoice generated Successfully').toEqual('Invoice generated Successfully');
    })
    /*
        it('Should Update the Invoice', async function () {
            CommPage.sleep(3);
            BillingPage.billingsSearchMatter.clear().sendKeys('-01 Billing Automation Test-' + R + '-');
            CommPage.sleep(1);
            BillingPage.firstMatter_UpdateInvoice.click();
            CommPage.sleep(3);
            BillingPage.editProfessionalFees.click();
            BillingPage.editValue.clear().sendKeys(70);
            CommPage.sleep(1);
            BillingPage.editPocketExpences.click();
            BillingPage.editValue.clear().sendKeys(80);
            CommPage.sleep(1);
            BillingPage.updateInvoice.click()
            CommPage.sleep(0.5);
            console.log('Invoice updated Successfully')
            expect('Invoice updated Successfully').toEqual('Invoice updated Successfully');
        })
    */
    it('Should Add amount on Invoice', async function () {
        BillingPage.billingsSearchMatter.clear().sendKeys('-01 Billing Automation Test-' + R + '-');
        CommPage.sleep(1);
        BillingPage.firstMatter_UpdateInvoice.click();
        CommPage.sleep(3);
        BillingPage.amount.sendKeys('3000');
        BillingPage.proof.sendKeys('Communication');
        BillingPage.addAmount.click();
        CommPage.sleep(1);
        BillingPage.amount.sendKeys('4000');
        BillingPage.proof.sendKeys('Fuel');
        BillingPage.addAmount.click();
        CommPage.sleep(1);
        BillingPage.updateInvoice.click()
        CommPage.sleep(0.5);
        console.log('Invoice updated Successfully')
        expect('Invoice updated Successfully').toEqual('Invoice updated Successfully');
    })
    it('Should Download the Updated Invoice', async function () {
        CommPage.sleep(1);
        BillingPage.downloadInvoice.click();
        console.log('Invoice Downloaded Successfully')
        expect('Invoice Downloaded Successfully').toEqual('Invoice Downloaded Successfully');
    })
    it('Should Mark As Completed the invoice', async function () {
        BillingPage.billingsSearchMatter.clear().sendKeys('-01 Billing Automation Test-' + R + '-');
        CommPage.sleep(1);
        BillingPage.firstMatter_UpdateInvoice.click();
        CommPage.sleep(3);
        BillingPage.markAsCompleted.click();
        CommPage.sleep(1);
        BillingPage.updateInvoice.click()
        CommPage.sleep(0.5);
        console.log('Invoice updated Successfully')
        expect('Invoice updated Successfully').toEqual('Invoice updated Successfully');
    })
    it('Should move to Completed Invoice', async function () {
        BillingPage.billingsSearchMatter.clear().sendKeys('-01 Billing Automation Test-' + R + '-');
        CommPage.sleep(1);
        BillingPage.firstMatter_UpdateInvoice.click();
        CommPage.sleep(5);
        BillingPage.closeInvoice.click();
        CommPage.sleep(1);
        console.log('Invoice Completed Successfully')
        expect('Invoice Completed Successfully').toEqual('Invoice Completed Successfully');
    })
    it('Should Download the Completed Invoice', async function () {
        CommPage.sleep(3);
        BillingPage.downloadInvoice.click();
        console.log('Completed Invoice Downloaded Successfully')
        expect('Completed Invoice Downloaded Successfully').toEqual('Completed Invoice Downloaded Successfully');
    })
    xit('Should Customize the columns of Completed Invoice', async function () {
        CommPage.refresh();
        CommPage.sleep(1);
        BillingPage.completed.click();
        CommPage.sleep(1);
        BillingPage.customizeSetting.click();
        CommPage.sleep(1);
        BillingPage.creationDate.click();
        CommPage.sleep(0.5);
        BillingPage.receivedAmount.click();
        CommPage.sleep(0.5);
        BillingPage.margin.click();
        CommPage.sleep(0.5);
        BillingPage.download.click();
        CommPage.sleep(2);
        BillingPage.creationDate.click();
        CommPage.sleep(0.5);
        BillingPage.receivedAmount.click();
        CommPage.sleep(0.5);
        BillingPage.margin.click();
        CommPage.sleep(0.5);
        BillingPage.download.click();
        CommPage.tabBtn();
    })
})
xdescribe('BILLING Module:Financial ANALYTICS', function () {
    it('Should Check On Financial Analytics', async function () {
        CommPage.sleep(1);
        HomePage.Analytics();
        CommPage.sleep(5);
        CommPage.refresh();
        AanlyticsPage02.financialAnlytics.click();
        CommPage.sleep(2);
        AanlyticsPage02.selectMatter.click();
        CommPage.sleep(7);
        AanlyticsPage02.searchForMatter.sendKeys('-01 Billing Automation Test-' + R + '-');
        AanlyticsPage02.firstOptionMatter.click();
        CommPage.escBtn();
        AanlyticsPage01.startDate.click();
        AanlyticsPage01.FirstCalender();
        CommPage.sleep(0.5);
        AanlyticsPage01.todaysDate.click();
        CommPage.sleep(2);
        AanlyticsPage01.analyzeReport.click();
        CommPage.sleep(2);
        console.log('Financial Analytics Analyzed Successfully')
        expect('Financial Analytics Analyzed Successfully').toEqual('Financial Analytics Analyzed Successfully');
    })
    it('Should Check On Financial Analytics Graph', async function () {
        AanlyticsPage02.graph.click();
        console.log('Financial Analytics Graph Displayed Successfully')
        expect('Financial Analytics Graph Displayed Successfully').toEqual('Financial Analytics Graph Displayed Successfully');
    })
    it('Should Check On Financial Analytics Table', async function () {
        AanlyticsPage02.scrollEnd();
        CommPage.sleep(3);
        console.log('Financial Analytics Graph Displayed Successfully')
        expect('Financial Analytics Graph Displayed Successfully').toEqual('Financial Analytics Graph Displayed Successfully');
    })
})
describe('BILLING Module: New Matter', function () {
    it('Should check the amount in Billing table against same matter', async function () {
        HomePage.Billing();
        CommPage.sleep(5);
        // CommPage.refresh();
        BillingPage.billingsSearchMatter.sendKeys('-01 Billing Automation Test-' + R + '-');
        CommPage.sleep(2);
        let text = BillingPage.outstandingAmount();
        expect(text).toEqual('₹0.00')
        console.log("Outstanding Amount value = Zero")
    })
    it('Should check the error messsage of timesheet matter, billing completed', async function () {
        //Manual Entry
        HomePage.Timesheet();
        CommPage.sleep(2);
        TimesheetPage01.PreviousDate.click();
        CommPage.sleep(0.5);
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.searchMatter.sendKeys('-01 Billing Automation Test-' + R + '-')
        TimesheetPage01.firstEntry.click();
        CommPage.sleep(3)
        TimesheetPage01.cancelBtn.click();
        console.log('Timesheet error message displayed')
        expect('Timesheet error message displayed').toEqual('Timesheet error message displayed')
    })
    xit('Should add New Timesheet added on same date & check on billing', async function () {
        CommPage.refresh();
        CommPage.sleep(2);
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.searchMatter.sendKeys('-01 Billing Automation Test-' + R + '-')
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('Previous Date Manual Timesheet');
        TimesheetPage01.validateBtn.click();
        console.log('Timesheet Entry sucessfully added')
        HomePage.Billing();
        CommPage.sleep(5);
        CommPage.refresh();
        BillingPage.hideZeroAmount.click();
        BillingPage.billingsSearchMatter.sendKeys('-01 Billing Automation Test-' + R + '-');
        CommPage.sleep(2);
    })
})
xdescribe('BILLING Matter-Dropbox Integration', function () {
    it('Should check the matter shared with dropbox', async function () {
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(5);
        GCMPage03.matterSearchInput.sendKeys('-01 Billing Automation Test-' + R + '-');
        CommPage.sleep(2);
        GCMPage03.firstMatterSearch.click();
        CommPage.sleep(3);
        GCMPage01.editMatter.click();
        CommPage.sleep(3);
        GCMPage01.selectMember.sendKeys('soham@flowace.in');
        GCMPage01.firstOption.click();
        GCMPage01.saveBtn.click();
        CommPage.sleep(2)
        GCMPage02.details.click();
        CommPage.sleep(2);
        GCMPage02.dropbox_Flies.click();
        CommPage.childWindow();
        CommPage.sleep(5);
        DropBoxPage.dropbox_Emailname.sendKeys('debasish@flowace.in');
        DropBoxPage.dropbox_Password.sendKeys('debasish@123');
        DropBoxPage.dropbox_SignIn.click();
        CommPage.sleep(10);
        CommPage.sleep(3);
        CommPage.sleep(5);
    })
})