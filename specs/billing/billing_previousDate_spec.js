var CommPage = require("../../lib/CommLib/comm_lib")
var VerifyPage = require("../../lib/actionLib/verify")
var HelperPage = require("../../utils/helperUtils/helper")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var TimesheetPage01 = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")
var ReportingPage = require("../../pages/reporting/reporting_po")
var AnalyticsPage01 = require("../../pages/analytics/analytics01_matterAnalytics_po")
var AnalyticsPage02 = require("../../pages/analytics/analytics02_financialAnalytics_po")

let R = Math.random().toString(36).substring(7);

/**
 * 1- Should Create a New Matter
 * 2- Should Add Manual entry of Previous Date 
 * 3- Should Search the matter on billing Page
 * 
 * 1- Should Create NEW Invoice
 * 2- Should Add amount on Invoice
 * 3- Should Download the Updated Invoice
 * 4- Should Mark As Completed the invoice
 * 5- Should move to Completed Invoice
 * 6- Should Download the Completed Invoice
 * 7- Should Customize the columns of Completed Invoice
 * 
 * 1- Should Check On Financial Analytics
 * 2- Should Check On Financial Analytics Graph
 * 3- Should Check On Financial Analytics Table
 * 
 */

beforeAll(function () {
    LoginPage.GetURL();
    LoginPage.login();
    CommPage.sleep(3);
    //HomePage.Billing.click();
    //CommPage.sleep(3);
})
describe('Billing Module:-- OLD Data', function () {
    it('Should Create a New Matter', async function () {
        HomePage.Matter();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        GCMPage01.firstClient.click();
        CommPage.sleep(1);
        GCMPage01.newMatterBtn.click();
        CommPage.sleep(1);
        GCMPage01.matterName.sendKeys('-01 Old Data Billing-Timesheet-' + R + '-');
        CommPage.sleep(5);
        GCMPage01.selectMember.sendKeys('Debasish Patra');
        CommPage.sleep(3);
        GCMPage01.firstOption.click();
        GCMPage01.saveBtn.click();
        CommPage.sleep(2);
        console.log("Matter Created sucessfully")
        expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
    })
    it('Should Add Manual entry of Previous Date ', async function () {
        CommPage.refresh();
        CommPage.sleep(3);
        HomePage.Timesheet();
        CommPage.sleep(3);
        TimesheetPage01.PreviousDate.click();
        CommPage.sleep(0.5);
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(2);
        TimesheetPage01.selectMatter.click();
        CommPage.sleep(0.5);
        TimesheetPage01.searchMatter.sendKeys('-01 Old Data Billing-Timesheet-' + R + '-')
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('New Timesheet');
        TimesheetPage01.validateBtn.click();
        CommPage.sleep(2);
        console.log('Timesheet Entry Sucessfully Added')
        expect('Timesheet Entry Sucessfully Added').toEqual('Timesheet Entry Sucessfully Added');
    })
    it('Should Search the matter on billing Page', async function () {
        HomePage.Billing();
        CommPage.sleep(3);
        BillingPage.hideZeroAmount.click();
        BillingPage.billingsSearchMatter.sendKeys('-01 Old Data Billing-Timesheet-' + R + '-');
        CommPage.sleep(2);
        console.log('Matter Search worked Successfully')
        expect('Matter Search worked Successfully').toEqual('Matter Search worked Successfully');
    })
})
describe('BILLING Module:--New Create New & Update Invoice', function () {
    it('Should Create NEW Invoice', async function () {
        CommPage.refresh();
        CommPage.sleep(3);
        BillingPage.newInvoice.click();
        CommPage.sleep(3);
        BillingPage.selectMatter_CreateInvoice.click();
        CommPage.sleep(3);
        BillingPage.searchMatter_CreateInvoice.sendKeys('-01 Old Data Billing-Timesheet-' + R + '-');
        BillingPage.firstOption_CreateInvoice.click();
        BillingPage.outOfPocketExpenses.sendKeys(4000);
        CommPage.sleep(1);
        BillingPage.professionalFee.sendKeys(5000);
        CommPage.sleep(0.5);
        BillingPage.createInvoice.click();
        CommPage.sleep(5);
        console.log('Invoice generated Successfully')
        expect('Invoice generated Successfully').toEqual('Invoice generated Successfully');
    })
    it('Should Add amount on Invoice', async function () {
        BillingPage.billingsSearchMatter.clear().sendKeys('-01 Old Data Billing-Timesheet-' + R + '-');
        CommPage.sleep(1);
        BillingPage.firstMatter_UpdateInvoice.click();
        CommPage.sleep(3);
        BillingPage.amount.sendKeys('3000');
        BillingPage.proof.sendKeys('Communication');
        BillingPage.addAmount.click();
        CommPage.sleep(1);
        BillingPage.amount.sendKeys('4000');
        BillingPage.proof.sendKeys('Fuel');
        BillingPage.addAmount.click();
        CommPage.sleep(1);
        BillingPage.updateInvoice.click()
        CommPage.sleep(0.5);
        console.log('Invoice updated Successfully')
        expect('Invoice updated Successfully').toEqual('Invoice updated Successfully');
    })
    it('Should Download the Updated Invoice', async function () {
        CommPage.sleep(1);
        BillingPage.downloadInvoice.click();
        console.log('Invoice Downloaded Successfully')
        expect('Invoice Downloaded Successfully').toEqual('Invoice Downloaded Successfully');
    })
    it('Should Mark As Completed the invoice', async function () {
        BillingPage.billingsSearchMatter.clear().sendKeys('-01 Old Data Billing-Timesheet-' + R + '-');
        CommPage.sleep(1);
        BillingPage.firstMatter_UpdateInvoice.click();
        CommPage.sleep(3);
        BillingPage.markAsCompleted.click();
        CommPage.sleep(1);
        BillingPage.updateInvoice.click()
        CommPage.sleep(0.5);
        console.log('Invoice updated Successfully')
        expect('Invoice updated Successfully').toEqual('Invoice updated Successfully');
    })
    it('Should move to Completed Invoice', async function () {
        BillingPage.billingsSearchMatter.clear().sendKeys('-01 Old Data Billing-Timesheet-' + R + '-');
        CommPage.sleep(1);
        BillingPage.firstMatter_UpdateInvoice.click();
        CommPage.sleep(5);
        BillingPage.closeInvoice.click();
        CommPage.sleep(1);
        console.log('Invoice Completed Successfully')
        expect('Invoice Completed Successfully').toEqual('Invoice Completed Successfully');
    })
    it('Should Download the Completed Invoice', async function () {
        CommPage.sleep(3);
        BillingPage.downloadInvoice.click();
        console.log('Completed Invoice Downloaded Successfully')
        expect('Completed Invoice Downloaded Successfully').toEqual('Completed Invoice Downloaded Successfully');
    })
    it('Should Customize the columns of Completed Invoice', async function () {
        CommPage.refresh();
        CommPage.sleep(1);
        BillingPage.completed.click();
        BillingPage.customizeSetting.click();
        CommPage.sleep(1);
        BillingPage.creationDate.click();
        CommPage.sleep(0.5);
        BillingPage.receivedAmount.click();
        CommPage.sleep(0.5);
        BillingPage.margin.click();
        CommPage.sleep(0.5);
        BillingPage.download.click();
        CommPage.sleep(2);
        BillingPage.creationDate.click();
        CommPage.sleep(0.5);
        BillingPage.receivedAmount.click();
        CommPage.sleep(0.5);
        BillingPage.margin.click();
        CommPage.sleep(0.5);
        BillingPage.download.click();
        CommPage.tabBtn();
    })
})
describe('BILLING Module:--FINANCIAL ANALYTICS', function () {
    it('Should Check On Financial Analytics', async function () {
        CommPage.sleep(1);
        HomePage.Analytics()
        CommPage.sleep(5);
        CommPage.refresh();
        AnalyticsPage02.financialAnlytics.click();
        CommPage.sleep(2);
        AnalyticsPage02.selectMatter.click();
        CommPage.sleep(5);
        AnalyticsPage02.searchForMatter.sendKeys('-01 Old Data Billing-Timesheet-' + R + '-');
        AnalyticsPage02.firstOptionMatter.click();
        CommPage.escBtn();
        AnalyticsPage01.endDate.click();
        AnalyticsPage01.FirstCalender();
        // CommPage.sleep(0.5);
        // AnalyticsPage01.todaysDate.click();
        CommPage.sleep(2);
        AnalyticsPage01.analyzeReport.click();
        CommPage.sleep(2);
        console.log('Financial Analytics Analyzed Successfully')
        expect('Financial Analytics Analyzed Successfully').toEqual('Financial Analytics Analyzed Successfully');
    })
    it('Should Check On Financial Analytics Graph', async function () {
        AnalyticsPage02.graph.click();
        CommPage.sleep(2);
        console.log('Financial Analytics Graph Displayed Successfully')
        expect('Financial Analytics Graph Displayed Successfully').toEqual('Financial Analytics Graph Displayed Successfully');
    })
    it('Should Check On Financial Analytics Table', async function () {
        AnalyticsPage02.scrollEnd();
        CommPage.sleep(3);
        console.log('Financial Analytics Table Displayed Successfully')
        expect('Financial Analytics Table Displayed Successfully').toEqual('Financial Analytics Table Displayed Successfully');
    })
})