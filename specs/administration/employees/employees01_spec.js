var CommPage = require("../../../lib/CommLib/comm_lib")
var VerifyPage = require("../../../lib/actionLib/verify")
var LoginPage = require("../../../pages/login/login_po")
var HomePage = require("../../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../../pages/timesheets/timesheets01_po")
var EmployeePage01 = require("../../../pages/administration/employees/employees01_po")
var EmployeePage02 = require("../../../pages/administration/employees/employees02_assign_po.js")
var EmployeePage03 = require("../../../pages/administration/employees/employees03_role_po.js")
var e = require("../../../testData/DataDriven/01_employeeData")

let R = Math.random().toString(36).substring(7);

/**
 * 1- Should Create Employee.
 * 2- Should check the plugin Integration Enabled by default.
 * 3- Should Display Error Message without email-id.
 * 4- Should Edit Employee.
 * 
 * 1- Should Disable Employee.
 * 2- Should Enable Employee.
 * 
 * 1- Should Download Employee List.
 * 2- Should Ascending & Descnding Employee.
 * 3- Should Ascending & Descnding Employee Online Status.
 * 4- Should Check Online Status of an Employee
 *  
 */

// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login();
//     CommPage.sleep(3);
//     HomePage.Administration();
//     CommPage.sleep(0.5);
//     HomePage.Employee();
//     CommPage.sleep(1);
// })
describe('Employee Module Login', function () {
    it('Should Login Again', async function () {
        LoginPage.GetURL();
        CommPage.sleep(15);

        LoginPage.login()
        CommPage.sleep(15);
        HomePage.Administration();
        CommPage.sleep(0.5);
        HomePage.Employee();
        CommPage.sleep(5);
    })
})
describe('EMPLOYEE MODULE: Create & Edit Employee', function () {
    let newEmail = 'new-' + R + '@test.com'
    let editEmail = 'edit-' + R + '@test.com'
    it('Should Create Employee', async function () {
        //new emp button
        EmployeePage01.newEmpBtn();
        CommPage.sleep(3);
        EmployeePage01.firstname.sendKeys(e.employee.NewEmployee.firstName);
        EmployeePage01.lastname.sendKeys(e.employee.NewEmployee.lastName);
        EmployeePage01.email.sendKeys(newEmail);
        EmployeePage01.yearlysalary.sendKeys(e.employee.NewEmployee.yearlySal);
        EmployeePage01.fulldays.clear().sendKeys(e.employee.NewEmployee.fullDays);
        EmployeePage01.halfdays.clear().sendKeys(e.employee.NewEmployee.halfDays);
        EmployeePage01.fulldaysHours.clear().sendKeys(e.employee.NewEmployee.fullHours);
        EmployeePage01.halfdaysHours.clear().sendKeys(e.employee.NewEmployee.halfHours);
        EmployeePage01.chargeOutRate.clear().sendKeys(e.employee.NewEmployee.chargeOutRate);
        EmployeePage01.role.click();
        EmployeePage01.managerBtnRole.click();
        EmployeePage01.contactNumber.sendKeys(e.employee.NewEmployee.contactNumber);
        EmployeePage01.reportsTo.sendKeys(e.employee.NewEmployee.reportsTo);
        EmployeePage01.reportslist.click();
        //save
        EmployeePage01.savebutton.click().then(function () {
            console.log('User Created Sucessfully')
            expect('User Created Sucessfully').toEqual('User Created Sucessfully');
        })
        CommPage.sleep(3);
    })
    it('Should check the plugin Integration Enabled by default--538', async function () {
        EmployeePage01.newEmpBtn();
        CommPage.sleep(1);
        EmployeePage03.selectIntegrations.click();
        expect(EmployeePage03.wordIntegration.isEnabled()).toBe(true);
        expect(EmployeePage03.excelIntegration.isEnabled()).toBe(true);
        expect(EmployeePage03.pptIntegration.isEnabled()).toBe(true);
        expect(EmployeePage03.outlookIntegration.isEnabled()).toBe(true);
        expect(EmployeePage03.acrobatIntegration.isEnabled()).toBe(true);
        CommPage.escBtn();
        console.log('Integration Is Selected ByDefault');
        EmployeePage01.cancelbutton.click();
    })
    it('Should Display Error Message without email-id', async function () {
        //new emp button
        EmployeePage01.newEmpBtn();
        CommPage.sleep(3);
        EmployeePage01.firstname.sendKeys(e.employee.EditEmployee.firstName);
        EmployeePage01.lastname.sendKeys(e.employee.EditEmployee.lastName);
        //Without Email id
        EmployeePage01.yearlysalary.sendKeys(e.employee.EditEmployee.yearlySal);
        EmployeePage01.fulldays.clear().sendKeys(e.employee.EditEmployee.fullDays);
        EmployeePage01.halfdays.clear().sendKeys(e.employee.EditEmployee.halfDays);
        EmployeePage01.fulldaysHours.clear().sendKeys(e.employee.EditEmployee.fullHours);
        EmployeePage01.halfdaysHours.clear().sendKeys(e.employee.EditEmployee.halfHours);
        EmployeePage01.chargeOutRate.clear().sendKeys(e.employee.EditEmployee.chargeOutRate);
        EmployeePage01.role.click()
        EmployeePage01.employeeBtnRole.click();
        EmployeePage01.contactNumber.sendKeys(e.employee.EditEmployee.contactNumber);
        //save
        EmployeePage01.savebutton.click();
        CommPage.sleep(3);
        EmployeePage01.firstname.click().then(function () {
            console.log('Employee doesnot allow To Create')
            expect('Employee doesnot allow To Create').toEqual('Employee doesnot allow To Create');
        })
        CommPage.sleep(1)
    })
    it('Should Edit Employee', async function () {
        EmployeePage01.cancelbutton.click();
        CommPage.sleep(3);
        EmployeePage01.searchEmp.sendKeys(newEmail);
        //edit 1st emp
        CommPage.sleep(2);
        EmployeePage01.editEmp.click();
        CommPage.sleep(2);
        EmployeePage01.firstname.clear().sendKeys(e.employee.EditEmployee.firstName);
        EmployeePage01.lastname.clear().sendKeys(e.employee.EditEmployee.lastName);
        CommPage.sleep(2);
        EmployeePage01.email.clear().sendKeys(editEmail);
        EmployeePage01.fulldays.clear().sendKeys(e.employee.EditEmployee.fullDays);
        EmployeePage01.halfdays.clear().sendKeys(e.employee.EditEmployee.halfDays);
        EmployeePage01.fulldaysHours.clear().sendKeys(e.employee.EditEmployee.fullHours);
        EmployeePage01.halfdaysHours.clear().sendKeys(e.employee.EditEmployee.halfHours);
        //skip historical salary
        CommPage.sleep(2)
        EmployeePage01.chargeOutRate.clear().sendKeys(e.employee.EditEmployee.chargeOutRate);
        EmployeePage01.role.click();
        EmployeePage01.employeeBtnRole.click();
        EmployeePage01.contactNumber.clear().sendKeys(e.employee.EditEmployee.contactNumber);
        EmployeePage01.reportsTo.clear().sendKeys(e.employee.EditEmployee.reportsTo);
        EmployeePage01.reportslist.click();
        EmployeePage01.savebutton.click().then(function () {
            console.log('User Updated Sucessfully');
            expect('User Updated Sucessfully').toEqual('User Updated Sucessfully');
        })
        CommPage.sleep(3);
    })
    it('Should Disable Employee', async function () {
        CommPage.refresh();
        CommPage.sleep(7);
        EmployeePage01.searchEmp.sendKeys(editEmail);
        CommPage.sleep(2);
        EmployeePage01.editEmp.click();
        CommPage.sleep(1);
        EmployeePage01.searchEmp.clear();
        CommPage.sleep(2);
        //click on disable
        EmployeePage01.disable.click().then(function () {
            console.log('User disabled Sucessfully')
            expect('User disabled Sucessfully').toEqual('User disabled Sucessfully');
        })
        CommPage.sleep(1);
    })
    it('Should Enable Employee', async function () {
        CommPage.refresh();
        //click on Disble Button
        CommPage.sleep(3);
        await EmployeePage01.disabled.click();
        CommPage.sleep(3)
        EmployeePage01.searchEmp.sendKeys(editEmail);
        CommPage.sleep(2);
        EmployeePage01.editEmp.click();
        CommPage.sleep(1);
        EmployeePage01.enable.click().then(function () {
            console.log('User Activated Sucessfully')
            expect('User Activated Sucessfully').toEqual('User Activated Sucessfully');
        })
        CommPage.sleep(1);
    })
})
describe('EMPLOYEE MODULE: Download,Ascending & Descnding Employee', function () {
    it('Should Download Employee List', async function () {

        CommPage.refresh();
        EmployeePage01.download.click().then(function () {
            console.log('Employee List Downloaded Sucessfully')
            expect('Employee List Downloaded Sucessfully').toEqual('Employee List Downloaded Sucessfully');
        })
        CommPage.sleep(1);
    })

    it('Should Ascending & Descnding Employee', function () {
        CommPage.refresh();
        CommPage.sleep(1);
        EmployeePage01.empAll.getText().then(function (text) {
            console.log(text);
        });
        CommPage.sleep(1);
        EmployeePage01.empHeader.click().then(function () {
            console.log('Employee Ascending & Descending worked Sucessfully')
            expect('Employee Ascending & Descending worked Sucessfully').toEqual('Employee Ascending & Descending worked Sucessfully');
        })
        EmployeePage01.empAll.getText().then(function (text) {
            console.log(text);
        });
    })
    it('Should Ascending & Descnding Employee Online Status', function () {
        CommPage.sleep(2);
        EmployeePage01.onlineStatusHeader.click().then(function () {
            console.log('Ascending & Descending Online Status')
            expect('Online Status Ascending & Descending worked Sucessfully').toEqual('Online Status Ascending & Descending worked Sucessfully');
        })
    })
    it('Should Check Online Status of an Employee', function () {
        CommPage.sleep(1);
        EmployeePage01.searchEmp.sendKeys('debasish@flowace.in');
        CommPage.sleep(1);
        EmployeePage01.onlineStatus();
        expect("Online Status Displayed").toEqual("Online Status Displayed");
        CommPage.sleep(1);
    })
})