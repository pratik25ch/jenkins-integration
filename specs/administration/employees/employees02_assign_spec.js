var CommPage = require("../../../lib/CommLib/comm_lib")
var LoginPage = require("../../../pages/login/login_po")
var HomePage = require("../../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../../pages/timesheets/timesheets01_po")
var EmployeePage01 = require("../../../pages/administration/employees/employees01_po.js")
var EmployeePage02 = require("../../../pages/administration/employees/employees02_assign_po.js")
var EmployeePage03 = require("../../../pages/administration/employees/employees03_role_po.js")

let R = Math.random().toString(36).substring(7);
/**
 * Should Select Employee.
 * Should Assign MATTER.
 * Should Show Message of Assign MATTER.
 * Should Check the Associate Name inside the Matter.
 * 
 */

// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login();
//     CommPage.sleep(3);
//     HomePage.Administration();
//     CommPage.sleep(0.5);
//     HomePage.Employee();
//     CommPage.sleep(1);
// })
describe('EMPLOYEE MODULE: Assign Matter to Employee', function () {
    it('Should Select Employee', function () {
        //EmployeePage02.searchEmp.sendKeys('qa-' + R + '@test.com');
        EmployeePage02.mouseMoveFirst();
        EmployeePage02.checkBox.click().then(function () {
            console.log('Employee Selected Sucessfully')
            expect('Employee Selected Sucessfully').toEqual('Employee Selected Sucessfully');
        })
        CommPage.sleep(2);
    })
    it('Should Assign MATTER', function () {
        EmployeePage02.matIcon.click();
        CommPage.sleep(1);
        EmployeePage02.assignMatter.click();
        CommPage.sleep(5);
        //group dropdown
        EmployeePage02.groupDropdown.click();
        CommPage.sleep(0.5);
        EmployeePage02.groupCheckbox.click();
        CommPage.sleep(1);
        //Escape Button
        EmployeePage02.escBtn();
        //client dropdown
        EmployeePage02.clientDropdown.click();
        CommPage.sleep(0.5);
        EmployeePage02.clientCheckbox.click();
        CommPage.sleep(1);
        EmployeePage02.escBtn();
        CommPage.sleep(2);
        EmployeePage02.MatterDopdown.click();
        CommPage.sleep(0.5);
        EmployeePage02.MatterCheckbox1.click();
        //EmployeePage02.MatterCheckbox2.click();
        CommPage.sleep(0.5);
        EmployeePage02.escBtn();
        console.log('Employee Cleared Sucessfully')
        expect('Employee Cleared Sucessfully').toEqual('Employee Cleared Sucessfully');
        CommPage.sleep(2);
    })
    it('Should Show Message of Assign MATTER ', function () {
        //CLEAR Button
        EmployeePage02.clearBtn.click();
        CommPage.sleep(3);
        EmployeePage02.cancelAssignBtn.click();
        CommPage.sleep(2);
        EmployeePage02.matIcon.click();
        CommPage.sleep(1);
        EmployeePage02.assignMatter.click();
        CommPage.sleep(5);
        //group dropdown
        EmployeePage02.groupDropdown.click();
        CommPage.sleep(0.5);
        EmployeePage02.groupCheckbox.click();
        CommPage.sleep(0.5);
        //Escape Button
        EmployeePage02.escBtn();
        //client dropdown
        EmployeePage02.clientDropdown.click();
        CommPage.sleep(0.5);
        EmployeePage02.clientCheckbox.click();
        CommPage.sleep(0.5);
        EmployeePage02.escBtn();
        CommPage.sleep(2);
        EmployeePage02.MatterDopdown.click();
        CommPage.sleep(0.5);
        EmployeePage02.MatterCheckbox1.click();
        //EmployeePage02.MatterCheckbox2.click();
        CommPage.sleep(0.5);
        EmployeePage02.escBtn();
        CommPage.sleep(2);
        //Assign Matter
        EmployeePage02.assignBtn.click().then(function () {
            console.log('Employee Assigned Sucessfully')
            expect('Employee Assigned Sucessfully').toEqual('Employee Assigned Sucessfully');
        })
        CommPage.sleep(2);
    })
    it('Should Check the Associate Name inside the Matter ', function () {
        HomePage.Matter();
        CommPage.sleep(5);
        EmployeePage02.firstClient.click();
        EmployeePage02.firstMatter.click();
        EmployeePage02.associatename.getText().then(function (text) {
            console.log('Assigned Employees', text);
        })
        console.log('Associate Name Assigned Sucessfully')
        expect('Associate Name Assigned Sucessfully').toEqual('Associate Name Assigned Sucessfully');
        CommPage.sleep(3);
    })
})