var LoginData = require("../../../utils/jsonUtils/login.json");
var CommPage = require("../../../lib/CommLib/comm_lib")
var LoginPage = require("../../../pages/login/login_po")
var HomePage = require("../../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../../pages/timesheets/timesheets01_po")
var EmployeePage01 = require("../../../pages/administration/employees/employees01_po.js")
var EmployeePage02 = require("../../../pages/administration/employees/employees02_assign_po.js")
var EmployeePage03 = require("../../../pages/administration/employees/employees03_role_po.js")
var e = require("../../../testData/DataDriven/01_employeeData")

let R = Math.random().toString(36).substring(7);

/**
 * 1- Should Change the Role of an Employee.
 * 2- Should Signout The Admin.
 * 3- Should Login a New EMPLOYEE.
 * 4- Should Create Create Through Matter Icon.
 * 5- Associate Should allow  Manual entry.
 * 6- Shouldnot Check financial Section of Matter Datails.
 * 7- Should Signout The EMPLOYEE.
 * 8- Should Login a New EMPLOYEE.
 * 9- Should Search Matter Globally.
 * 10- Should check the Task Amount Against Manual entry.
 * 
 * 1- Should Open & Download the Employee Hierarchy.
 * 
 */

// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login();
//     CommPage.sleep(3);
//     HomePage.Administration();
//     CommPage.sleep(0.5);
//     HomePage.Employee();
//     CommPage.sleep(1);
// })

describe('EMPLOYEE MODULE: Chanege The Role of an Employee', function () {
    let associateEmail = "qa1@flowace.in"
    it('Should Change the Role of an Employee--572 ', function () {
        CommPage.sleep(2);
        HomePage.Administration();
        CommPage.sleep(1);
        HomePage.Employee();
        CommPage.sleep(2);
        EmployeePage01.searchEmp.sendKeys(associateEmail);
        CommPage.sleep(2);
        EmployeePage01.editEmp.click();
        CommPage.sleep(1);
        EmployeePage01.role.click();
        EmployeePage01.adminBtnRole.click();
        CommPage.sleep(1);
        EmployeePage01.savebutton.click().then(function () {
            console.log("EMPLOYEE Role Changed to Admin sucessfully")
        })
        CommPage.sleep(2);
        //CommPage.refresh();
        //EmployeePage03.searchEmp.sendKeys('qa1@flowace.in');
        CommPage.sleep(1);
        EmployeePage01.editEmp.click();
        CommPage.sleep(1);
        EmployeePage01.role.click();
        EmployeePage01.employeeBtnRole.click();
        CommPage.sleep(1);
        EmployeePage03.selectPermissions.click();
        EmployeePage03.allowMatterCreation.click();
        CommPage.sleep(0.5);
        EmployeePage03.allowManualEntry.click();
        CommPage.sleep(2);
        EmployeePage02.escBtn();
        EmployeePage01.savebutton.click().then(function () {
            console.log("EMPLOYEE Updated sucessfully")
            expect('EMPLOYEE Updated sucessfully').toEqual('EMPLOYEE Updated sucessfully');
        })
        CommPage.sleep(2);
    })
    it('Should Signout The Admin ', function () {
        LoginPage.signOut();
        console.log("Admin Signout sucessfully")
        expect('Admin SignOut sucessfully').toEqual('Admin SignOut sucessfully');
    })
    it('Should Login a New EMPLOYEE ', function () {
        LoginPage.associateLogin();
        console.log("EMPLOYEE Login sucessfully")
        expect('EMPLOYEE Login sucessfully').toEqual('EMPLOYEE Login sucessfully');
        CommPage.sleep(3);
    })
    it('Should Create Create Through Matter Icon', function () {
        GCMPage03.iconMatterAssociate.click();
        CommPage.sleep(1);
        GCMPage01.associateIconMatterDropDown.click();
        CommPage.sleep(5);
        GCMPage01.matSelectGroup.click();
        CommPage.sleep(1);
        GCMPage01.secondOption.click();
        GCMPage01.matSelectClient.click();
        CommPage.sleep(1);
        GCMPage01.secondOption.click();
        GCMPage01.matterName.sendKeys(e.employee.NewL3.matterName,R);
        CommPage.sleep(1);
        GCMPage01.selectMember.sendKeys(e.employee.NewL3.selectFirstMember);
        CommPage.sleep(2);
        GCMPage01.firstOption.click();
        CommPage.sleep(1);
        GCMPage01.selectMember.sendKeys(e.employee.NewL3.selectSecondMember);
        CommPage.sleep(2);
        GCMPage01.firstOption.click();
        CommPage.sleep(1);
        GCMPage01.saveBtn.click().then(function () {
            console.log("Matter Created sucessfully")
            expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
        })
        CommPage.sleep(2);
    })
    it('Associate Should allow  Manual entry  ', async function () {
        HomePage.Timesheet();
        // CommPage.refresh();
        CommPage.sleep(3);
        TimesheetPage.manualEntryBtn();
        CommPage.sleep(2);
        TimesheetPage.selectMatter.click();
        CommPage.sleep(0.5);
        TimesheetPage.searchMatter.sendKeys(e.employee.NewL3.matterName,R)
        TimesheetPage.firstEntry.click();
        TimesheetPage.manualEntryName.sendKeys('New Timesheet');
        TimesheetPage.validateBtn.click().then(function () {
            console.log('Timesheet Entry Sucessfully Added')
            expect('Timesheet Entry Sucessfully Added').toEqual('Timesheet Entry Sucessfully Added');
        })
        CommPage.sleep(2);
    })
    it('Shouldnot Check financial Section of Matter Datails--571', function () {
        HomePage.associateMatter();
        CommPage.sleep(2);
        GCMPage01.searchMatterValue.sendKeys(e.employee.NewL3.matterName,R);
        GCMPage01.firstMatter.click();
        CommPage.sleep(3);
        GCMPage02.details.click();
        CommPage.sleep(2);
        expect(GCMPage02.financialDetails.isDisplayed()).toBe(false);
    })
    it('Should Signout The EMPLOYEE ', function () {
        LoginPage.signOut()
        expect('EMPLOYEE SignOut sucessfully').toEqual('EMPLOYEE SignOut sucessfully');
    })
    it('Should Login a New EMPLOYEE ', function () {
        LoginPage.login();
        console.log(" Login sucessfully")
        expect(' Login sucessfully').toEqual(' Login sucessfully');
        CommPage.sleep(3);
    })
    it('Should Search Matter Globally', function () {
        //CommPage.refresh();
        CommPage.sleep(3);
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(5);
        GCMPage03.matterSearchInput.sendKeys(e.employee.NewL3.matterName,R);
        CommPage.sleep(2);
    })
    it('Should check the Task Amount Against Manual entry', function () {
        GCMPage03.firstMatterSearch.click();
        CommPage.sleep(5);
        GCMPage02.details.click().then(function () {
            console.log("Matter Details")
            expect('Matter Details').toEqual('Matter Details');
        })
        CommPage.sleep(3);
    })
})
describe('EMPLOYEE MODULE: HIERARCHY ', function () {
    it('Should Open & Download the Employee Hierarchy--577', function () {
        HomePage.Administration();
        HomePage.Employee();
        CommPage.sleep(1);
        EmployeePage03.viewHierarchy.click();
        CommPage.sleep(2);
        EmployeePage03.expandHierarchy.click();
        CommPage.sleep(2);
        EmployeePage03.downloadHierarchy.click();
        CommPage.sleep(5);
        EmployeePage03.closeHierarchy.click();
        CommPage.sleep(3);
    })
    it('Should SignOut the EMPLOYEE Module', function () {
        LoginPage.signOut();
        CommPage.sleep(5);
        console.log('Employee Module SignOut Successfully')
    })
})