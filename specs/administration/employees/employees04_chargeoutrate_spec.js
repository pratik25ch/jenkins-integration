var CommPage = require("../../../lib/CommLib/comm_lib")
var VerifyPage = require("../../../lib/actionLib/verify")
var LoginPage = require("../../../pages/login/login_po")
var HomePage = require("../../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../../pages/timesheets/timesheets01_po")
var EmployeePage01 = require("../../../pages/administration/employees/employees01_po")
var EmployeePage04 = require("../../../pages/administration/employees/eployees04_chargeoutRate_po.js")
var EmployeePage03 = require("../../../pages/administration/employees/employees03_role_po.js")
var e = require("../../../testData/DataDriven/01_employeeData")
var LoginData = require("../../../utils/jsonUtils/login.json");
var TimesheetPage01 = require("../../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../../pages/billing/billing_po")
let R = Math.random().toString(36).substring(7);
let RandomName = '';
/**
 * 1- Should Create Employee.
 * 2- Should check the plugin Integration Enabled by default.
 * 3- Should Display Error Message without email-id.
 * 4- Should Edit Employee.
 * 
 * 1- Should Disable Employee.
 * 2- Should Enable Employee.
 * 
 * 1- Should Download Employee List.
 * 2- Should Ascending & Descnding Employee.
 * 3- Should Ascending & Descnding Employee Online Status.
 * 4- Should Check Online Status of an Employee
 *  
 */

beforeAll(function () {
    // LoginPage.GetURL();
    CommPage.sleep(15);

    LoginPage.login();
    CommPage.sleep(5);
    HomePage.Administration();
    CommPage.sleep(5);
    HomePage.Employee();
    CommPage.sleep(5);
})

// describe('EMPLOYEE MODULE: Create & Edit Employee', function () {
//     let newEmail = 'new-' + R + '@test.com'
//     let editEmail = 'edit-' + R + '@test.com'
//     let startDateText = '';
//     let startDateTextCOR = '';
//     let endDateText = '';
//     let endDateTextCOR = '';

//     it('Should Edit Historical charge-out rate of Employee', async function () {
//         CommPage.sleep(13);
//         EmployeePage01.searchEmp.clear().sendKeys(LoginData.Demo_Admin_Login.Email);
//         //edit 1st emp
//         CommPage.sleep(15);
//         EmployeePage01.editEmp.click();
//         CommPage.sleep(5);
//         CommPage.scrollDown1();
//         CommPage.sleep(5);

//         EmployeePage01.historicalChargeOutRateCheck.click();

//         EmployeePage01.historicalChargeOutRate.clear().sendKeys(e.employee.EditEmployee.historicalChargeOutRate);
//         EmployeePage01.StartDate.click();
//         CommPage.sleep(2);

//         EmployeePage01.StartDateSelect.click();


//         EmployeePage01.EndDate.click();
//         CommPage.sleep(2);

//         EmployeePage01.EndDateSelect.click();
//         CommPage.sleep(5);

//         EmployeePage01.savebutton.click().then(async function () {
//             console.log('User Updated Sucessfully');
//             CommPage.sleep(3);
//             expect(await EmployeePage01.successMessage.getText()).toEqual('User updated Successfully');

//             expect('User Updated Sucessfully').toEqual('User Updated Sucessfully');
//         })
//         CommPage.sleep(3);


//     })

//     it('Should able to sync Historical charge-out rate of Employee with current charge-out rate', async function () {
//         CommPage.refresh();
//         CommPage.sleep(13);
//         EmployeePage01.searchEmp.clear().sendKeys(LoginData.Demo_Admin_Login.Email);
//         //edit 1st emp
//         CommPage.sleep(15);
//         EmployeePage01.editEmp.click();
//         CommPage.sleep(10);
//         CommPage.scrollDown1();
//         CommPage.sleep(10);
//         try {
//             EmployeePage01.historicalChargeOutRateCheck.click();

//         } catch (error) {
//             CommPage.sleep(15);
//             EmployeePage01.editEmp.click();
//             CommPage.sleep(10);
//             CommPage.scrollDown1();
//             CommPage.scrollDown1();

//             CommPage.sleep(5);
//             EmployeePage01.historicalChargeOutRateCheck.click();

//         }
//         EmployeePage01.chargeOutRate.clear().sendKeys('200');
//         CommPage.sleep(5);

//         EmployeePage01.syncChargeOutRate.click();
//         CommPage.sleep(5);

//         EmployeePage01.historicalChargeOutRate.clear().sendKeys(e.employee.EditEmployee.historicalChargeOutRate);
//         CommPage.sleep(5);

//         EmployeePage01.syncChargeOutRate.click();
//         CommPage.sleep(5);

//         EmployeePage01.StartDate.click();
//         CommPage.sleep(2);

//         EmployeePage01.StartDateSelect.click();


//         EmployeePage01.EndDate.click();
//         CommPage.sleep(2);

//         EmployeePage01.EndDateSelect.click();
//         CommPage.sleep(5);
//         startDateText = EmployeePage01.StartDate.getAttribute('max')
//         endDateText = EmployeePage01.EndDate.getAttribute('max')

//         EmployeePage01.savebutton.click().then(async function () {
//             console.log('User Updated Sucessfully');
//             CommPage.sleep(3);
//             expect(await EmployeePage01.successMessage.getText()).toEqual('User updated Successfully');

//             expect('User Updated Sucessfully').toEqual('User Updated Sucessfully');
//         })
//         CommPage.sleep(3);


//     })


//     it('Should able to verify past changed historical charge-out rate', async function () {
//         CommPage.refresh();
//         CommPage.sleep(13);
//         EmployeePage01.searchEmp.clear().sendKeys(LoginData.Demo_Admin_Login.Email);
//         CommPage.sleep(3);

//         EmployeePage04.PastHistoricalCOR.click();

//         startDateTextCOR = EmployeePage04.StartDateSelectCOR.getText();
//         endDateTextCOR = EmployeePage04.endDateSelectCOR.getText();
//         rateTextCOR = EmployeePage04.rateCOR.getText();

//         // expect(startDateTextCOR).toEqual(startDateText);
//         // expect(endDateTextCOR).toEqual(endDateText);
//         expect(rateTextCOR).toEqual(''+e.employee.EditEmployee.historicalChargeOutRate+'');


//     })
// })
describe('BILLING Module:--Add Timesheet for Generating Invoice ', function () {
    it('Should Create a New Matter', async function () {
        HomePage.Matter();
        GCMPage01.firstClient.click();
        CommPage.sleep(1);
        GCMPage01.newMatterBtn.click();
        CommPage.sleep(1);
         RandomName = '-01 Billing Automation Test-' + R + '-';
        GCMPage01.matterName.sendKeys(RandomName);
        CommPage.sleep(5);
        GCMPage01.selectMember.sendKeys('Pratikdemo Test');
        CommPage.sleep(3);
        GCMPage01.firstOption.click();
        GCMPage01.saveBtn.click();
        CommPage.sleep(2);
        console.log("Matter Created sucessfully")
        expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
    })
    it('Should Add Manual entry', async function () {
        CommPage.sleep(2);
        CommPage.refresh();
        CommPage.sleep(13);
        HomePage.Timesheet();
        CommPage.sleep(3);
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(2);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.searchMatter.sendKeys(RandomName)
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('New Timesheet');
        TimesheetPage01.validateBtn.click();
        CommPage.sleep(2);
        console.log('Timesheet Entry Sucessfully Added')
        expect('Timesheet Entry Sucessfully Added').toEqual('Timesheet Entry Sucessfully Added');
    })
    it('Should Search the matter on billing Page', async function () {
        HomePage.Billing();
        CommPage.sleep(5);
        CommPage.refresh();
        BillingPage.hideZeroAmount.click();
        BillingPage.billingsSearchMatter.sendKeys(RandomName);
        CommPage.sleep(2);
        console.log('Matter Search worked Successfully')
        expect('Matter Search worked Successfully').toEqual('Matter Search worked Successfully');
    })
})

describe('BILLING Module:--New Generate & Update Invoice', function () {
    it('Should Generate the Invoice', async function () {
        CommPage.sleep(1);
        BillingPage.hideZeroAmount.click();
        BillingPage.firstMatter_Outstanding.click();
        BillingPage.generatingInvoice.click();
        CommPage.sleep(3);
        BillingPage.invoiceId.sendKeys(R)
        CommPage.sleep(1);
        BillingPage.perticularTitle.sendKeys("No Title");
        CommPage.sleep(1);
        BillingPage.particularCost.sendKeys('500');
        CommPage.sleep(1);
        BillingPage.addCost.click();
        CommPage.sleep();
        BillingPage.outOfPocketExpenses.sendKeys(4000);
        CommPage.sleep(1);
        // BillingPage.professionalFee.sendKeys(5000);
        // CommPage.sleep(0.5);
        BillingPage.createInvoice.click();
        CommPage.sleep(5);
        console.log('Invoice generated Successfully')
        expect('Invoice generated Successfully').toEqual('Invoice generated Successfully');
    })

    it('Should Add amount on Invoice', async function () {
        BillingPage.billingsSearchMatter.clear().sendKeys(RandomName);
        CommPage.sleep(1);
        BillingPage.firstMatter_UpdateInvoice.click();
        CommPage.sleep(3);
        BillingPage.amount.sendKeys('3000');
        BillingPage.proof.sendKeys('Communication');
        BillingPage.addAmount.click();
        CommPage.sleep(1);
        BillingPage.amount.sendKeys('4000');
        BillingPage.proof.sendKeys('Fuel');
        BillingPage.addAmount.click();
        CommPage.sleep(1);
        BillingPage.updateInvoice.click()
        CommPage.sleep(0.5);
        console.log('Invoice updated Successfully')
        expect('Invoice updated Successfully').toEqual('Invoice updated Successfully');
    })
    it('Should Download the Updated Invoice', async function () {
        CommPage.sleep(1);
        BillingPage.downloadInvoice.click();
        console.log('Invoice Downloaded Successfully')
        expect('Invoice Downloaded Successfully').toEqual('Invoice Downloaded Successfully');
    })
    it('Should Mark As Completed the invoice', async function () {
        BillingPage.billingsSearchMatter.clear().sendKeys(RandomName);
        CommPage.sleep(1);
        BillingPage.firstMatter_UpdateInvoice.click();
        CommPage.sleep(3);
        BillingPage.markAsCompleted.click();
        CommPage.sleep(1);
        BillingPage.updateInvoice.click()
        CommPage.sleep(0.5);
        console.log('Invoice updated Successfully')
        expect('Invoice updated Successfully').toEqual('Invoice updated Successfully');
    })
    it('Should move to Completed Invoice', async function () {
        BillingPage.billingsSearchMatter.clear().sendKeys('-01 Billing Automation Test-' + R + '-');
        CommPage.sleep(1);
        BillingPage.firstMatter_UpdateInvoice.click();
        CommPage.sleep(5);
        BillingPage.closeInvoice.click();
        CommPage.sleep(1);
        console.log('Invoice Completed Successfully')
        expect('Invoice Completed Successfully').toEqual('Invoice Completed Successfully');
    })
    it('Should Download the Completed Invoice', async function () {
        CommPage.sleep(3);
        BillingPage.downloadInvoice.click();
        console.log('Completed Invoice Downloaded Successfully')
        expect('Completed Invoice Downloaded Successfully').toEqual('Completed Invoice Downloaded Successfully');
    })
    xit('Should Customize the columns of Completed Invoice', async function () {
        CommPage.refresh();
        CommPage.sleep(1);
        BillingPage.completed.click();
        CommPage.sleep(1);
        BillingPage.customizeSetting.click();
        CommPage.sleep(1);
        BillingPage.creationDate.click();
        CommPage.sleep(0.5);
        BillingPage.receivedAmount.click();
        CommPage.sleep(0.5);
        BillingPage.margin.click();
        CommPage.sleep(0.5);
        BillingPage.download.click();
        CommPage.sleep(2);
        BillingPage.creationDate.click();
        CommPage.sleep(0.5);
        BillingPage.receivedAmount.click();
        CommPage.sleep(0.5);
        BillingPage.margin.click();
        CommPage.sleep(0.5);
        BillingPage.download.click();
        CommPage.tabBtn();
    })
})
xdescribe('BILLING Module:Financial ANALYTICS', function () {
    it('Should Check On Financial Analytics', async function () {
        CommPage.sleep(1);
        HomePage.Analytics();
        CommPage.sleep(5);
        CommPage.refresh();
        AanlyticsPage02.financialAnlytics.click();
        CommPage.sleep(2);
        AanlyticsPage02.selectMatter.click();
        CommPage.sleep(7);
        AanlyticsPage02.searchForMatter.sendKeys(RandomName);
        AanlyticsPage02.firstOptionMatter.click();
        CommPage.escBtn();
        AanlyticsPage01.startDate.click();
        AanlyticsPage01.FirstCalender();
        CommPage.sleep(0.5);
        AanlyticsPage01.todaysDate.click();
        CommPage.sleep(2);
        AanlyticsPage01.analyzeReport.click();
        CommPage.sleep(2);
        console.log('Financial Analytics Analyzed Successfully')
        expect('Financial Analytics Analyzed Successfully').toEqual('Financial Analytics Analyzed Successfully');
    })
    it('Should Check On Financial Analytics Graph', async function () {
        AanlyticsPage02.graph.click();
        console.log('Financial Analytics Graph Displayed Successfully')
        expect('Financial Analytics Graph Displayed Successfully').toEqual('Financial Analytics Graph Displayed Successfully');
    })
    it('Should Check On Financial Analytics Table', async function () {
        AanlyticsPage02.scrollEnd();
        CommPage.sleep(3);
        console.log('Financial Analytics Graph Displayed Successfully')
        expect('Financial Analytics Graph Displayed Successfully').toEqual('Financial Analytics Graph Displayed Successfully');
    })
})