var CommPage = require("../../../lib/CommLib/comm_lib");
var LoginPage = require("../../../pages/login/login_po");
var HomePage = require("../../../lib/homepageLib/homepage_lib")
var SettingsEmpPage = require("../../../pages/administration/settings/settings01_emp_po");

let R = Math.random().toString(36).substring(7);

// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     CommPage.sleep(3);
//     HomePage.Administration();
//     CommPage.sleep(0.5);
//     HomePage.Settings();
//     CommPage.sleep(3);
// });
describe('Setting Module Login', function () {
    it('Should Login Again', async function () {
        LoginPage.GetURL();
        LoginPage.login()
        CommPage.sleep(3);
        HomePage.Administration();
        CommPage.sleep(0.5);
        HomePage.Settings();
        CommPage.sleep(5);
    })
})
describe('SETTINGS MODULE: Employee Section', function () {
    it('Should Check Employee Defaults', async function () {
        SettingsEmpPage.employee_Defaults(267,47,11,8);
        expect('updated Sucessfully').toEqual('updated Sucessfully');
    })
    xit('Should Check Financial Details', async function () {
        await SettingsEmpPage.overheadfactordate();
        CommPage.sleep(1);
        await SettingsEmpPage.profitabilityfactordate();
        SettingsEmpPage.employee_Finances();
        expect('settings updated Sucessfully').toEqual('settings updated Sucessfully');
    })
    it('Should Change the Terminology--455', async function () {
        SettingsEmpPage.language_Terminology("GROUP","CLIENT","MATTER","EMPLOYEE");
        expect('added Sucessfully').toEqual('added Sucessfully');
    })
})