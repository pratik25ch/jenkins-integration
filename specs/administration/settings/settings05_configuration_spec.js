var CommPage = require("../../../lib/CommLib/comm_lib");
var LoginPage = require("../../../pages/login/login_po");
var HomePage = require("../../../lib/homepageLib/homepage_lib")
var SettingsConfigurationPage = require("../../../pages/administration/settings/settings05_configuration_po");
var SettingsEmpPage = require("../../../pages/administration/settings/settings01_emp_po");
var EmployeePage01 = require("../../../pages/administration/employees/employees01_po")
let R = Math.random().toString(36).substring(7);

// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     CommPage.sleep(3);
//     HomePage.Administration();
//     CommPage.sleep(0.5);
//     HomePage.Settings();
//     CommPage.sleep(3);
// });

describe('SETTINGS MODULE: Configuration Section', function () {
    it('Should Move to Configuration Section', async function () {
        SettingsConfigurationPage.settings_Configuration();
        expect('Configuration Opened Sucessfully').toEqual('Configuration Opened Sucessfully');
    })
    it('Should Update Flowace SMTP Configuration', async function () {
        SettingsConfigurationPage.flowaceConfiguration();
        expect('Email Configuration Edited Successfully').toEqual('Email Configuration Edited Successfully');
    })
    it('Should Update Gmail SMTP Configuration', async function () {
        SettingsConfigurationPage.gmailConfiguration();
        expect('Updated Sucessfully').toEqual('Updated Sucessfully');
    })
    it('Should Update Timesheet Email & Alearts', async function () {
        SettingsConfigurationPage.timesheetEmailAlearts();
        expect('Updated Sucessfully').toEqual('Updated Sucessfully');
    })
    it('Should Update Default Plugin Time', async function () {
        SettingsConfigurationPage.defaultPlugInIdleTime();
        expect('Updated Sucessfully').toEqual('Updated Sucessfully');
    })
})

describe('EMPLOYEE MODULE: New Employee Section', function () {
    it('Should Move to Employee', async function () {
        SettingsEmpPage.settings_employee();
        expect('Employee Opened Sucessfully').toEqual('Employee Opened Sucessfully');
    })
    it('Should Upate Employee Defaults', async function () {
        SettingsEmpPage.employee_Modified();
        expect('updated Sucessfully').toEqual('updated Sucessfully');
    })
    it('Should Check The New Employee Page', async function () {
        //Check the employee page
        HomePage.Administration();
        HomePage.Employee();
        CommPage.sleep(3);
        EmployeePage01.newEmpBtn();
        console.log('Employee Defaults Updated Sucessfully')
        expect('Employee Defaults Updated Sucessfully').toEqual('Employee Defaults Updated Sucessfully');

        CommPage.sleep(5);
        EmployeePage01.cancelbutton.click();
        CommPage.sleep(2);
    })
    it('Should SignOut the Settings Module', function () {
        LoginPage.signOut();
        console.log('Settings Module SignOut Successfully')
        CommPage.sleep(5);
    })
})