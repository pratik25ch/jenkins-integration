var CommPage = require("../../../lib/CommLib/comm_lib");
var LoginPage = require("../../../pages/login/login_po");
var HomePage = require("../../../lib/homepageLib/homepage_lib")
var SettingsIntegrationPage = require("../../../pages/administration/settings/settings04_integration_po");

let R = Math.random().toString(36).substring(7);

// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     CommPage.sleep(3);
//     HomePage.Administration();
//     CommPage.sleep(0.5);
//     HomePage.Settings();
//     CommPage.sleep(3);
// });

describe('SETTINGS MODULE: Integration Section', function () {
    it('Should Move to Integration Section', async function () {
        SettingsIntegrationPage.settings_integration();
        expect('Integration Opened Sucessfully').toEqual('Integration Opened Sucessfully');

    })
})