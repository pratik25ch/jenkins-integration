var CommPage = require("../../../lib/CommLib/comm_lib");
var LoginPage = require("../../../pages/login/login_po");
var HomePage = require("../../../lib/homepageLib/homepage_lib")
var SettingsWebSitePage = require("../../../pages/administration/settings/settings02_website_po");

let R = Math.random().toString(36).substring(7);

// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     CommPage.sleep(3);
//     HomePage.Administration();
//     CommPage.sleep(0.5);
//     HomePage.Settings();
//     CommPage.sleep(3);
// });

describe('SETTINGS MODULE: Websites Section', function () {

    it('Should Move to Websites section', async function () {
        SettingsWebSitePage.settings_website();
        expect('Websites list Opened Sucessfully').toEqual('Websites list Opened Sucessfully');
    })
    it('Should Add the Blocked Websites', async function () {
        SettingsWebSitePage.newBlacklist_websites();
        expect('added Sucessfully').toEqual('added Sucessfully');
    })
    it('Should Edit the Blocked Websites', async function () {
        SettingsWebSitePage.editBlacklist_websites();
        expect('updated Sucessfully').toEqual('updated Sucessfully');
    })
    it('Should Remove the Blocked Websites', async function () {
        SettingsWebSitePage.removeBlacklist_websites();
        expect('Removed Sucessfully').toEqual('Removed Sucessfully');
    })

    it('Should Add the WhiteList Websites', async function () {
        CommPage.refresh();
        SettingsWebSitePage.settings_website();
        SettingsWebSitePage.newWhitelist_websites();
        expect('added Sucessfully').toEqual('added Sucessfully');
    })
    it('Should Edit the WhiteList Websites', async function () {
        SettingsWebSitePage.editWhitelist_websites();
        expect('updated Sucessfully').toEqual('updated Sucessfully');
    })
    it('Should Remove the WhiteList Websites', async function () {
        SettingsWebSitePage.removeWhitelist_websites();
        expect('Removed Sucessfully').toEqual('Removed Sucessfully');
    })

})