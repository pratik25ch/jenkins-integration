var CommPage = require("../../../lib/CommLib/comm_lib")
var LoginPage = require("../../../pages/login/login_po")
var HomePage = require("../../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../../pages/billing/billing_po")
var ContactsPage01 = require("../../../pages/administration/contacts/contacts01_po")
var ContactsPage02 = require("../../../pages/administration/contacts/contacts02_userDetails_po")
var ContactsPage03 = require("../../../pages/administration/contacts/contacts03_tag_po")

let R = Math.random().toString(36).substring(7);
/**
 * Should Select Contact.
 * Should Tag Contact.
 * Should Check the Matter.
 * Should Delete Contact.
 * 
 */
// beforeAll(async function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     CommPage.sleep(3);

//     HomePage.Administration();
//     CommPage.sleep(0.5);
//     HomePage.Contacts()
//     CommPage.sleep(1);
// })

describe('CONTACTS MODULE :--- Tag Contacts Section', function () {
    it('Should Select Contact ', function () {
        ContactsPage02.clearFilter.click();
        CommPage.sleep(1);
        ContactsPage01.searchContact.sendKeys('001 Frontend')
        ContactsPage03.firstCheckbox.click();
        console.log('Contact Selected Successfully')
        expect('Contact Selected Successfully').toEqual('Contact Selected Successfully');
    })
    it('Should Tag Contact ', function () {
        ContactsPage03.selectDone.click();
        CommPage.sleep(5);
        ContactsPage03.tagGroup.click();
        CommPage.sleep(0.5);
        ContactsPage03.firstSelect.click();
        ContactsPage03.tagClient.click();
        CommPage.sleep(0.5);
        ContactsPage03.firstSelect.click();
        ContactsPage03.tagMatter.click();
        CommPage.sleep(0.5);
        ContactsPage03.firstSelect.click();
        ContactsPage03.tagContact.click();
        CommPage.sleep(1);
        console.log('Contact tagged Successfully')
        expect('Contact tagged Successfully').toEqual('Contact tagged Successfully');
    })
    //Matter
    it('Should Check the Matter ', function () {
        CommPage.sleep(1);
        HomePage.Matter();
        GCMPage01.firstClient.click();
        CommPage.sleep(2);
        GCMPage03.contactDetails.click();
        CommPage.sleep(2);
        GCMPage03.closeContact.click();
        CommPage.sleep(1);
        GCMPage01.firstMatter.click();
        GCMPage03.contactDetails.click();
        CommPage.sleep(2);
        GCMPage03.closeContact.click();
        console.log('Contact Checked ')
        expect('Contact Checked ').toEqual('Contact Checked ');
    })
    it('Should Delete Contact', function () {
        //HomePage.Administration();
        HomePage.Contacts();
        CommPage.sleep(1);
        ContactsPage01.searchContact.sendKeys('001 Frontend');
        ContactsPage01.editOption.click();
        CommPage.sleep(1);
        ContactsPage03.deleteContact.click();
        CommPage.sleep(1);
        ContactsPage03.confirmBtn.click();
        CommPage.sleep(2);
        console.log('Contact has been removed Sucessfully ')
        expect('Contact has been removed Sucessfully ').toEqual('Contact has been removed Sucessfully ');
    })
    it('Should SignOut the Contact Module', function () {
        LoginPage.signOut();
        CommPage.sleep(5);
        console.log('Contact Module SignOut Successfully')
    })
})