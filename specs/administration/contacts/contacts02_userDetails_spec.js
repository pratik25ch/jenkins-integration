var CommPage = require("../../../lib/CommLib/comm_lib")
var VerifyPage = require("../../../lib/actionLib/verify")
var LoginPage = require("../../../pages/login/login_po")
var HomePage = require("../../../lib/homepageLib/homepage_lib")
var TimesheetPage = require("../../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../../pages/billing/billing_po")
var ContactsPage01 = require("../../../pages/administration/contacts/contacts01_po")
var ContactsPage02 = require("../../../pages/administration/contacts/contacts02_userDetails_po")
var ContactsPage03 = require("../../../pages/administration/contacts/contacts03_tag_po")

let R = Math.random().toString(36).substring(7);
/**
 * 1- Should Go to Profile Details.
 * 2- Should Check the Default address.
 * 3- Should Create a Badge.
 * 4- Should try to Create a Duplicate Badge.
 * 5- Should check the Badge On User Profile.
 * 6- Should Edit the User Information.
 * 7- Should Edit the User Personal Information.
 * 8- Should Edit the User Contact Information.
 * 9- Should Check the Updated User Profile Serching Through Badge.
 * 10- Should Check the Tag GCM of User Profile.
 * 
 * 1- Should Filer Contact by Date.
 * 2- Should Filer Contact by Badge.
 * 
 */

// beforeAll(async function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     CommPage.sleep(3);

//     HomePage.Administration();
//     CommPage.sleep(0.5);
//     HomePage.Contacts()
//     CommPage.sleep(1);
// })

describe('CONTACTS MODULE :---- Use Profile Section', function () {
    it('Should Go to Profile Details', function () {
        CommPage.refresh();
        ContactsPage01.searchContact.sendKeys('5435588894');
        ContactsPage02.firstUser.click().then(function () {
            CommPage.sleep(2);
            console.log('User Profile Details')
            expect('User Profile Details').toEqual('User Profile Details');
        })
    })
    it('Should Check the Default address--528', function () {
        expect(VerifyPage.userAddress_verify.isPresent()).toBeTruthy();
        console.log('User Default Address displayed Successfully')
    })
    it('Should Create a Badge', function () {
        ContactsPage02.createBadgeIcon.click();
        CommPage.sleep(2);
        ContactsPage02.createBadge.click();
        ContactsPage02.badgeName.sendKeys('Badge-', R);
        ContactsPage02.badgeBtn.click().then(function(){
            CommPage.sleep(3);
            console.log('Badge Added Successfully')
            expect('Badge Added Successfully').toEqual('Badge Added Successfully')
        })
    })
    it('Should try to Create a Duplicate Badge', function () {
        // ContactsPage02.createBadgeIcon.click();
        // CommPage.sleep(2);
        // ContactsPage02.createBadge.click();
        ContactsPage02.badgeName.sendKeys('Badge-', R);
        CommPage.sleep(1);
        ContactsPage02.badgeBtn.click();
        CommPage.sleep(2);
        console.log('Badge Already Exist')
        expect('Badge Already Exist').toEqual('Badge Already Exist');
    })
    it('Should check the Badge On User Profile ', function () {
        CommPage.sleep(1);
        CommPage.back();
        ContactsPage02.addBadgedropdown.click();
        CommPage.sleep(2);
        ContactsPage02.badgeInput.sendKeys('Badge-', R);
        ContactsPage01.firstInput.click();
        CommPage.sleep(0.5);
        ContactsPage02.escBtn();
        CommPage.sleep(0.5);
        ContactsPage02.tagDone.click();
        console.log('Badge Added Successfully')
        expect('Badge Added Successfully').toEqual('Badge Added Successfully');
    })
    it('Should Edit the User Information--597', function () {
        CommPage.refresh();
        //ContactsPage02.firstUser.click();
        //User Title Edit 
        ContactsPage02.userTitleEdit.click();
        CommPage.sleep(2);
        ContactsPage02.userFirstName.clear().sendKeys('001 Frontend');
        ContactsPage02.userLastName.clear().sendKeys('Automation Testing');
        ContactsPage02.companyName.clear().sendKeys('Gozo Tech Services Pvt Ltd.')
        ContactsPage02.userProfileSaveBtn.click();
        CommPage.sleep(2);
        console.log('Information Updated Successfully')
        expect('Information Updated Successfully').toEqual('Information Updated Successfully');
    })
    it('Should Edit the User Personal Information ', async function () {
        CommPage.refresh();
        await ContactsPage02.personalInfoEdit.click();
        CommPage.sleep(1);
        await ContactsPage02.profession.sendKeys("QA Automation");
        CommPage.sleep(1);
        await ContactsPage02.calenderIcon.click();
        ContactsPage02.dobClick.click();
        ContactsPage02.genderIcon.click();
        ContactsPage02.genderMale.click();
        CommPage.sleep(1);
        ContactsPage02.userProfileSaveBtn.click();
        CommPage.sleep(2);
        console.log('Personal Information Updated Successfully')
        expect('Personal Information Updated Successfully').toEqual('Personal Information Updated Successfully');
    })
    it('Should Edit the User Contact Information ', async function () {
        CommPage.sleep(1);
        ContactsPage02.contactInfoEdit.click();
        CommPage.sleep(2);
        await ContactsPage02.userEmail.sendKeys('automation@gmail.com');
        CommPage.sleep(2);
        await ContactsPage02.addEmail.click();
        CommPage.sleep(1);
        ContactsPage02.secondEmail.click();
        await ContactsPage02.deleteFirstEmail.click();
        CommPage.sleep(0.5);
        // ContactsPage02.address.sendKeys('Millenium Bussiness Tech Park');
        // ContactsPage02.addAddress.click();
        // ContactsPage02.primaryAddress.click();
        // CommPage.sleep(0.5);
        ContactsPage01.seconderyType.click();
        ContactsPage01.numbertype.click();
        ContactsPage01.selectcountry.click();
        ContactsPage01.country.click();
        ContactsPage01.inputnumber.sendKeys('5665673337');
        ContactsPage01.addnumber.click();
        CommPage.sleep(1);
        ContactsPage02.deletePhone.click();
        CommPage.sleep(0.5);
        ContactsPage02.validateBtn.click();
        CommPage.sleep(2);
        console.log(' Contact Information Updated Successfully')
        expect(' Contact Information Updated Successfully').toEqual(' Contact Information Updated Successfully');
    })
    it('Should Check the Updated User Profile Serching Through Badge', function () {
        HomePage.Administration();
        HomePage.Contacts();
        CommPage.sleep(2);
        ContactsPage01.searchContact.sendKeys('Badge-', R)
        CommPage.sleep(2)
        ContactsPage02.firstUser.click();
        console.log('User Updated Successfully')
        expect('User Updated Successfully').toEqual('User Updated Successfully');
    })
    it('Should Check the Tag GCM of User Profile', async function () {
        CommPage.sleep(3);
        ContactsPage02.matterBtn.click();
        CommPage.sleep(2);
        ContactsPage02.MatterTag.click();
        CommPage.sleep(1);
        ContactsPage02.ClientTag.click();
        CommPage.sleep(1);
        ContactsPage02.GroupTag.click();
        CommPage.sleep(1);
        ContactsPage02.MatterTag.click();
        CommPage.sleep(3);
        console.log('GCM Tagged Successfully')
        expect('GCM Tagged Successfully').toEqual('GCM Tagged Successfully');
    })
})
describe('CONTACTS MODULE :--- Filter Contact', function () {
    it('Should Filter Contact by Date ', function () {
        CommPage.refresh();
        CommPage.sleep(5);
        HomePage.Administration();
        CommPage.sleep(1)
        HomePage.Contacts();
        CommPage.sleep(3);
        ContactsPage02.filterBtn.click();
        CommPage.sleep(1);
        ContactsPage02.byDate.click();
        CommPage.sleep(1);
        ContactsPage02.calender.click();
        // ContactsPage02.todaysDateClick();
        ContactsPage02.todayClick.click();
        CommPage.sleep(1);
        ContactsPage02.todayClick.click();
        CommPage.sleep(1);
        ContactsPage02.clearFilter.click();
        CommPage.sleep(1);
        ContactsPage02.filterBtn.click();
        CommPage.sleep(1);
        ContactsPage02.byDate.click();
        ContactsPage02.calender.click();
        ContactsPage02.previousMonth.click();
        ContactsPage02.startDate.click();
        ContactsPage02.nextMonth.click();
        ContactsPage02.todaysdate.click();
        CommPage.sleep(2);
        console.log('User Fiter By Date')
        expect('User Fiter By Date').toEqual('User Fiter By Date');
    })
    it('Should Filer Contact by Badge ', function () {
        ContactsPage02.filterBtn.click();
        CommPage.sleep(3);
        ContactsPage02.byBadge.click();
        ContactsPage02.addBadgedropdown.click();
        CommPage.sleep(2);
        ContactsPage02.badgeInput.sendKeys('Badge-', R);
        ContactsPage01.firstInput.click();
        CommPage.sleep(0.5);
        ContactsPage02.escBtn();
        CommPage.sleep(0.5);
        console.log('User Fiter By Badge')
        expect('User Fiter By Badge').toEqual('User Fiter By Badge');
    })
})