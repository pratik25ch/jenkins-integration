var CommPage = require("../../../../lib/CommLib/comm_lib")
var LoginPage = require("../../../../pages/login/login_po")
var HomePage = require("../../../../lib/homepageLib/homepage_lib")
var LoginData = require("../../../../utils/jsonUtils/login.json");
var PermissionsPage01 = require("../../../../pages/administration/permissions/permissions_po")
var AnalyticsPage01 = require("../../../../pages/analytics/analytics01_matterAnalytics_po")
var AnalyticsPage02 = require("../../../../pages/analytics/analytics02_financialAnalytics_po")
var AnalyticsPage03 = require("../../../../pages/analytics/analytics03_memberAnalytics_po")
var EmployeePage01 = require("../../../../pages/administration/employees/employees01_po")
var AnalyticsPage04 = require("../../../../pages/analytics/analytics04_timesheetAnalytics_po")
var e = require("../../../../testData/DataDriven/01_employeeData")
let R = Math.random().toString(36).substring(7);


beforeAll(function () {
    // LoginPage.GetURL();
    LoginPage.login()
    CommPage.sleep(3);
    HomePage.Administration();
    CommPage.sleep(3);

})

describe('Permissions Module -- List tab analytics', function () {



    it('As a Admin, After providing all permissions different analytics are present or not for associate', async function () {
        HomePage.Permissions();
        CommPage.sleep(3);
        CommPage.sleep(25);
        PermissionsPage01.ListTab.click();
        CommPage.sleep(15);

        PermissionsPage01.searchField.clear().sendKeys(LoginData.Associate_Login.Email);
        CommPage.sleep(5);
        PermissionsPage01.clickOnCheckedCheckboxes();
        CommPage.sleep(5);

        PermissionsPage01.clickOnUncheckedCheckboxes();
        CommPage.sleep(5);

        PermissionsPage01.saveButton.click();
        CommPage.sleep(5);
        PermissionsPage01.signOutButton.click();
        CommPage.sleep(10);
        LoginPage.AssociateLogin()
        CommPage.sleep(13);
        HomePage.Analytics();
        CommPage.sleep(25);



        // expect(await AnalyticsPage01.matterAnalyticsTab.isPresent()).toBe(true);
        // expect(await AnalyticsPage02.financialAnlytics.isPresent()).toBe(true);
        // expect(await AnalyticsPage03.memberAnalytics.isPresent()).toBe(true);
        console.log('different analytics are present')

        CommPage.sleep(5);
        PermissionsPage01.signOutButton.click();

    })

    it('As a Admin, After removing all permissions different analytics are not present for associate', async function () {
        CommPage.sleep(10);
        LoginPage.login();
        CommPage.sleep(3);
        HomePage.Administration();
        CommPage.sleep(3);
        HomePage.Permissions();
        CommPage.sleep(3);
        CommPage.sleep(15);
        PermissionsPage01.ListTab.click();
        CommPage.sleep(15);
        PermissionsPage01.searchField.clear().sendKeys(LoginData.Staging_Associate_Login.Email);
        CommPage.sleep(5);

        PermissionsPage01.clickOnCheckedCheckboxes();
        CommPage.sleep(5);

        PermissionsPage01.saveButton.click();
        CommPage.sleep(5);
        PermissionsPage01.signOutButton.click();
        CommPage.sleep(10);
        LoginPage.AssociateLogin();

        CommPage.sleep(15);


        // HomePage.verifyAnalyticsLabelNotPresent();

        // expect(await AnalyticsPage01.matterAnalyticsTab.isPresent()).toBe(false);
        // expect(await AnalyticsPage02.financialAnlytics.isPresent()).toBe(false);
        // expect(await AnalyticsPage03.memberAnalytics.isPresent()).toBe(false);
        // expect(await AnalyticsPage04.timesheetAnalytics.isPresent()).toBe(false);
        console.log('different analytics are not present')

        CommPage.sleep(5);
        PermissionsPage01.signOutButton.click();
        CommPage.sleep(10);
        LoginPage.login();
        CommPage.sleep(3);
        HomePage.Administration();
        CommPage.sleep(3);
        HomePage.Permissions();
        CommPage.sleep(3);
        CommPage.sleep(5);
        PermissionsPage01.ListTab.click();
        CommPage.sleep(15);

        PermissionsPage01.searchField.clear().sendKeys(LoginData.Associate_Login.Email);
        CommPage.sleep(5);

        PermissionsPage01.clickOnUncheckedCheckboxes();
        CommPage.sleep(5);
        element(by.xpath("//input[@aria-checked='true' and contains(@id,'analytics')]/..")).click();
      
        PermissionsPage01.saveButton.click();
        CommPage.sleep(5);

    })

    it('As a Admin, should able to verify all analytics labels for associate', async function () {
        // CommPage.sleep(10);
        // LoginPage.login();
        // CommPage.sleep(3);
        // HomePage.Administration();
        CommPage.sleep(3);
        HomePage.Permissions();
        CommPage.sleep(3);
        PermissionsPage01.ListTab.click();
        CommPage.sleep(20);
        // expect(await PermissionsPage01.EmailLabel.isPresent()).toBe(true);
        // expect(await PermissionsPage01.FinancialLabel.isPresent()).toBe(true);
        // expect(await PermissionsPage01.MatterLabel.isPresent()).toBe(true);
        // expect(await PermissionsPage01.MemberLabel.isPresent()).toBe(true);
        // expect(await PermissionsPage01.TimesheetLabel.isPresent()).toBe(true);
    })

    it('As a Admin, should able to verify all Email labels for associate', async function () {
        HomePage.Permissions();
        CommPage.sleep(3);
        PermissionsPage01.ListTab.click();
        CommPage.sleep(15);
        // expect(await PermissionsPage01.ClassifyEmailLabel.isPresent()).toBe(true);
        // expect(await PermissionsPage01.classifyAttachmentsLabel.isPresent()).toBe(true);

    })

    it('As a Admin, should able change name of associate and same must reflect in permissions for associate', async function () {
        CommPage.sleep(3);

        HomePage.Employee();
        CommPage.sleep(6);
        EmployeePage01.searchEmp.clear().sendKeys(LoginData.Associate_Login.Email);
        //edit 1st emp
        CommPage.sleep(5);
        EmployeePage01.editEmp.click();
        CommPage.sleep(2);
        EmployeePage01.firstname.clear().sendKeys(e.employee.EditEmployee.firstName);
        EmployeePage01.lastname.clear().sendKeys(e.employee.EditEmployee.lastName);
        CommPage.sleep(2);
        EmployeePage01.savebutton.click().then(function () {
            console.log('User Updated Sucessfully');

        })

        CommPage.sleep(3);
        // HomePage.Administration();
        CommPage.sleep(3);
        HomePage.Permissions();
        CommPage.sleep(3);
        PermissionsPage01.ListTab.click();
        CommPage.sleep(15);
        PermissionsPage01.searchField.clear().sendKeys(LoginData.Associate_Login.Email);
        CommPage.sleep(5);
        var FullNametext = await PermissionsPage01.fullNameText.getText();
        console.log('Full name in permission section is : ' + FullNametext);
        // expect(await PermissionsPage01.ClassifyEmailLabel.isPresent()).toBe(true);
        // expect(await PermissionsPage01.classifyAttachmentsLabel.isPresent()).toBe(true);

    })

    it('As a Admin, should able to reset changes in permissions for associate', async function () {
        HomePage.Permissions();
        CommPage.sleep(3);
        CommPage.sleep(5);
        PermissionsPage01.ListTab.click();
        CommPage.sleep(15);
        PermissionsPage01.searchField.clear().sendKeys(LoginData.Associate_Login.Email);
        CommPage.sleep(5);

        PermissionsPage01.clickOnUncheckedCheckboxes();
        CommPage.sleep(5);

        PermissionsPage01.resetButton.click();
        CommPage.sleep(5);
        PermissionsPage01.searchField.clear();

        CommPage.sleep(5);

        PermissionsPage01.searchField.clear().sendKeys(LoginData.Associate_Login.Email);
        CommPage.sleep(5);

        PermissionsPage01.verifyCheckboxes();


    })

    it('As a Admin, should able change role of associate to admin and same must reflect in permissions for same user', async function () {
        CommPage.sleep(8);

        HomePage.Employee();
        CommPage.sleep(20);
        EmployeePage01.editEmp.isPresent().then(function () {
            EmployeePage01.searchEmp.sendKeys(LoginData.Associate_Login.Email);
        });
        //edit 1st emp
        CommPage.sleep(5);
        EmployeePage01.editEmp.click();
        CommPage.sleep(12);
        EmployeePage01.role.click();
        CommPage.sleep(2);

        EmployeePage01.managerBtnRole.click();
        CommPage.sleep(2);
        EmployeePage01.savebutton.click().then(function () {
            console.log('User Updated Sucessfully');

        })

        CommPage.sleep(3);
        // HomePage.Administration();
        CommPage.sleep(3);
        HomePage.Permissions();
        CommPage.sleep(3);
        PermissionsPage01.ListTab.click();
        CommPage.sleep(15);
        PermissionsPage01.MemberLabel.isPresent().then(function () {
            PermissionsPage01.searchField.sendKeys(LoginData.Associate_Login.Email);
        })
        CommPage.sleep(5);
        PermissionsPage01.verifyCheckboxesDisabled();

        // expect(await PermissionsPage01.ClassifyEmailLabel.isPresent()).toBe(true);
        // expect(await PermissionsPage01.classifyAttachmentsLabel.isPresent()).toBe(true);

    })

    it('As a Admin, should able change role of associate to manager and same must reflect in permissions for same user', async function () {
        CommPage.sleep(8);

        HomePage.Employee();
        CommPage.sleep(10);
        EmployeePage01.editEmp.isPresent().then(function () {
            EmployeePage01.searchEmp.sendKeys(LoginData.Associate_Login.Email);
        });
        //edit 1st emp
        CommPage.sleep(15);
        EmployeePage01.editEmp.click();
        CommPage.sleep(12);
        EmployeePage01.role.click();
        CommPage.sleep(2);

        EmployeePage01.managerBtnRole.click();
        CommPage.sleep(2);
        EmployeePage01.savebutton.click().then(function () {
            console.log('User Updated Sucessfully');

        })

        CommPage.sleep(3);
        // HomePage.Administration();
        CommPage.sleep(3);
        HomePage.Permissions();
        CommPage.sleep(3);
        PermissionsPage01.ListTab.click();
        CommPage.sleep(15);
        PermissionsPage01.MemberLabel.isPresent().then(function () {
            PermissionsPage01.searchField.clear().sendKeys(LoginData.Associate_Login.Email);
        })
        CommPage.sleep(5);
        PermissionsPage01.verifyCheckboxesDisabled();
        HomePage.Employee();
        CommPage.sleep(10);
        EmployeePage01.editEmp.isPresent().then(function () {
            EmployeePage01.searchEmp.clear().sendKeys(LoginData.Associate_Login.Email);
        });
        //edit 1st emp
        CommPage.sleep(15);
        EmployeePage01.editEmp.click();
        CommPage.sleep(12);
        EmployeePage01.role.click();
        EmployeePage01.employeeBtnRole.click();
        CommPage.sleep(2);
        EmployeePage01.savebutton.click().then(function () {
            console.log('User Updated Sucessfully');

        })
        // expect(await PermissionsPage01.ClassifyEmailLabel.isPresent()).toBe(true);
        // expect(await PermissionsPage01.classifyAttachmentsLabel.isPresent()).toBe(true);

    })

   

    it('As a Admin, After disabling associate ,user should not able see associate in permissions section', async function () {
        CommPage.sleep(8);

        HomePage.Employee();
        CommPage.sleep(20);
        EmployeePage01.editEmp.isPresent().then(function () {
            EmployeePage01.searchEmp.clear().sendKeys(LoginData.Associate_Login.Email);
        });
        //edit 1st emp
        CommPage.sleep(5);
        CommPage.sleep(20);
        var EC = protractor.ExpectedConditions;
        // Waits for the element with id 'abc' to be visible on the dom.
        browser.wait(EC.visibilityOf(EmployeePage01.editEmp), 90000).then(function () {
            EmployeePage01.editEmp.click();
        })
        CommPage.sleep(5);
        EmployeePage01.disable.click().then(function () {
            console.log('User disabled Sucessfully')
            // expect('User disabled Sucessfully').toEqual('User disabled Sucessfully');
        })

        CommPage.sleep(3);
        // HomePage.Administration();
        CommPage.sleep(3);
        HomePage.Permissions();
        CommPage.sleep(3);
        PermissionsPage01.ListTab.click();
        CommPage.sleep(20);
        PermissionsPage01.MemberLabel.isPresent().then(function () {
            PermissionsPage01.searchField.clear().sendKeys(LoginData.Associate_Login.Email);
        })
        CommPage.sleep(5);

        browser.wait(EC.visibilityOf(PermissionsPage01.MemberLabel), 90000).then(function () {
            PermissionsPage01.searchField.clear().sendKeys(LoginData.Associate_Login.Email);
        })
        // PermissionsPage01.MemberLabel.isDisplayed().then(function () {
        //     PermissionsPage01.searchField.sendKeys(LoginData.Manager_Login.Email);
        // })
        CommPage.sleep(15);
        // ;

        // PermissionsPage01.verifyNoFullNameList(); // expect(await PermissionsPage01.ClassifyEmailLabel.isPresent()).toBe(true);
        // expect(await PermissionsPage01.classifyAttachmentsLabel.isPresent()).toBe(true);

    })

    it('As a Admin, After enabling associate ,user should  able see associate in permissions section', async function () {
        CommPage.sleep(8);

        HomePage.Employee();
        CommPage.sleep(10);
        EmployeePage01.editEmp.isPresent().then(function () {
            EmployeePage01.searchEmp.clear().sendKeys(LoginData.Associate_Login.Email);
        });
        //edit 1st emp
        CommPage.sleep(5);
        EmployeePage01.disabled.click();

        CommPage.sleep(20);
        var EC = protractor.ExpectedConditions;
        // Waits for the element with id 'abc' to be visible on the dom.
        browser.wait(EC.visibilityOf(EmployeePage01.editEmp), 90000).then(function () {
            EmployeePage01.editEmp.click();
        })
        CommPage.sleep(2);
        EmployeePage01.enable.click().then(function () {
            console.log('User enabled Sucessfully')
            // expect('User disabled Sucessfully').toEqual('User disabled Sucessfully');
        })

        CommPage.sleep(3);
        // HomePage.Administration();
        CommPage.sleep(3);
        HomePage.Permissions();
        CommPage.sleep(3);
        PermissionsPage01.ListTab.click();
        CommPage.sleep(5);
        PermissionsPage01.MemberLabel.isPresent().then(function () {
            PermissionsPage01.searchField.clear().sendKeys(LoginData.Associate_Login.Email);
        })
        CommPage.sleep(5);

        browser.wait(EC.visibilityOf(PermissionsPage01.MemberLabel), 90000).then(function () {
            PermissionsPage01.searchField.clear().sendKeys(LoginData.Associate_Login.Email);
        })
        // PermissionsPage01.MemberLabel.isDisplayed().then(function () {
        //     PermissionsPage01.searchField.sendKeys(LoginData.Manager_Login.Email);
        // })
        CommPage.sleep(15);
        // ;

        PermissionsPage01.verifyFullNameList();
        PermissionsPage01.screenshotTab.click();
        CommPage.sleep(20);
        browser.wait(EC.visibilityOf(PermissionsPage01.screenshotEmailLabel), 90000).then(function () {
            PermissionsPage01.searchField.clear().sendKeys(LoginData.Associate_Login.Email);
        })

        CommPage.sleep(15);
        // PermissionsPage01.verifyFullName();

    })



})

describe('Permissions Module -- Screenshot tab', function () {
    it('As a Admin, should able to click on On Off slider and save for associate', async function () {
        HomePage.Permissions();
        CommPage.sleep(3);
        PermissionsPage01.screenshotTab.click();

        CommPage.sleep(5);
        PermissionsPage01.searchField.clear();
        CommPage.sleep(5);
        PermissionsPage01.searchField.sendKeys(LoginData.Associate_Login.Email);
        CommPage.sleep(5);

        PermissionsPage01.onOffButton.click();
        CommPage.sleep(2);

        PermissionsPage01.saveScreenshotButton.click();
        CommPage.sleep(0.5);

        // expect(await PermissionsPage01.SuccessToast).toEqual('Sucessfully Updated');

    })
    it('As a Admin, should able to set interval value of screenshot for associate', async function () {
        HomePage.Permissions();
        CommPage.sleep(3);
        PermissionsPage01.screenshotTab.click();

        CommPage.sleep(5);
        PermissionsPage01.searchField.clear();
        CommPage.sleep(5);
        PermissionsPage01.searchField.sendKeys(LoginData.Associate_Login.Email);
        CommPage.sleep(5);

        PermissionsPage01.IntervalField.sendKeys('30');


    })

    it('As a Admin, should able to edit screnshot permissions and save for associate', async function () {
        // CommPage.sleep(3);

        // LoginPage.login()
        // CommPage.sleep(3);
        // HomePage.Administration();
        // CommPage.sleep(3);
        // HomePage.Permissions();
        // CommPage.sleep(3);
        HomePage.Permissions();
        PermissionsPage01.screenshotTab.click();
        CommPage.sleep(3);

        PermissionsPage01.searchField.clear();
        PermissionsPage01.searchField.sendKeys(LoginData.Associate_Login.Email);
        CommPage.sleep(5);

        PermissionsPage01.editButton.click();
        CommPage.sleep(2);

        PermissionsPage01.IntervalFieldEditOption.sendKeys('20');
        CommPage.sleep(0.5);
        PermissionsPage01.onOffEditOption.click();
        CommPage.sleep(2);
        PermissionsPage01.DayButton.click();
        CommPage.sleep(2);
        PermissionsPage01.sliderDragDrop();
        CommPage.sleep(2);
        PermissionsPage01.saveEditButton.click();
        CommPage.sleep(8);

    })

    it('As a Admin, should able to check screenshot and list tab for associate', async function () {
        HomePage.Permissions();
        CommPage.sleep(3);
        expect(await PermissionsPage01.ListTab.isPresent()).toBe(true);
        expect(await PermissionsPage01.screenshotTab.isPresent()).toBe(true);
        // expect(await AnalyticsPage03.memberAnalytics.isPresent()).toBe(true);

    })

    it('As a Admin, should able to verify email id and full name of employee for associate', async function () {
        HomePage.Permissions();
        CommPage.sleep(3);
        PermissionsPage01.screenshotTab.click();

        PermissionsPage01.searchField.clear();
        CommPage.sleep(5);

        PermissionsPage01.searchField.sendKeys(LoginData.Associate_Login.Email);
        CommPage.sleep(5);
        expect(await PermissionsPage01.Emailid.isPresent()).toBe(true);
        expect(await PermissionsPage01.fullName.isPresent()).toBe(true);

    })

    it('As a Admin, should able to verify all column labels for associate', async function () {
        HomePage.Permissions();
        CommPage.sleep(3);
        PermissionsPage01.screenshotTab.click();

        PermissionsPage01.searchField.clear();
        CommPage.sleep(5);

        PermissionsPage01.searchField.clear().sendKeys(LoginData.Associate_Login.Email);
        CommPage.sleep(5);
        expect(await PermissionsPage01.screenshotActionLabel.isPresent()).toBe(true);
        expect(await PermissionsPage01.screenshotEmailLabel.isPresent()).toBe(true);
        expect(await PermissionsPage01.screenshotFullnameLabel.isPresent()).toBe(true);
        expect(await PermissionsPage01.screenshotIntervalLabel.isPresent()).toBe(true);
        expect(await PermissionsPage01.ScreenshotOnoffLabel.isPresent()).toBe(true);
    })

    it('As a Admin, should able change name of associate and same must reflect in permissions for associate', async function () {
        CommPage.sleep(3);

        HomePage.Employee();
        CommPage.sleep(6);
        EmployeePage01.searchEmp.clear().sendKeys(LoginData.Associate_Login.Email);
        //edit 1st emp
        CommPage.sleep(5);
        EmployeePage01.editEmp.click();
        CommPage.sleep(2);
        EmployeePage01.firstname.clear().sendKeys(e.employee.EditEmployee.firstName);
        EmployeePage01.lastname.clear().sendKeys(e.employee.EditEmployee.lastName);
        CommPage.sleep(2);
        EmployeePage01.savebutton.click().then(function () {
            console.log('User Updated Sucessfully');

        })

        CommPage.sleep(3);
        // HomePage.Administration();
        CommPage.sleep(3);
        HomePage.Permissions();
        CommPage.sleep(3);
        PermissionsPage01.screenshotTab.click();
        CommPage.sleep(5);
        PermissionsPage01.searchField.clear().sendKeys(LoginData.Associate_Login.Email);
        CommPage.sleep(5);
        var FullNametext = await PermissionsPage01.fullName.getText();
        console.log('Full name in permission section is : ' + FullNametext);
        // expect(await PermissionsPage01.ClassifyEmailLabel.isPresent()).toBe(true);
        // expect(await PermissionsPage01.classifyAttachmentsLabel.isPresent()).toBe(true);

    })

    it('As a Admin, should able change email of associate and same must reflect in permissions for associate', async function () {
        CommPage.sleep(3);

        HomePage.Employee();
        CommPage.sleep(6);
        EmployeePage01.searchEmp.clear().sendKeys(LoginData.Associate_Login.Email);
        //edit 1st emp
        CommPage.sleep(5);
        EmployeePage01.editEmp.click();
        CommPage.sleep(6);
        EmployeePage01.email.clear().sendKeys(LoginData.Associate_Login.editEmail);
        CommPage.sleep(2);
        EmployeePage01.savebutton.click().then(function () {
            console.log('User Updated Sucessfully');

        })

        CommPage.sleep(3);
        // HomePage.Administration();
        CommPage.sleep(3);
        HomePage.Permissions();
        CommPage.sleep(3);
        PermissionsPage01.screenshotTab.click();
        CommPage.sleep(5);
        PermissionsPage01.searchField.clear().sendKeys(LoginData.Associate_Login.editEmail);
        CommPage.sleep(5);
        var Emailtext = await PermissionsPage01.Emailid.getText();
        console.log('Emailtext in permission section is : ' + Emailtext);
        // expect(await PermissionsPage01.ClassifyEmailLabel.isPresent()).toBe(true);
        // expect(await PermissionsPage01.classifyAttachmentsLabel.isPresent()).toBe(true);
        HomePage.Employee();
        CommPage.sleep(6);
        EmployeePage01.searchEmp.clear().sendKeys(  LoginData.Associate_Login.editEmail);
        //edit 1st emp
        CommPage.sleep(5);
        CommPage.sleep(5);
        EmployeePage01.editEmp.click();
        CommPage.sleep(7);
        EmployeePage01.email.clear().sendKeys(LoginData.Associate_Login.Email);
        CommPage.sleep(2);
        EmployeePage01.savebutton.click().then(function () {
            console.log('User Updated Sucessfully');

        })

    })

    it('As a associate, user should not able to see permissions option', async function () {
        CommPage.sleep(15);
        PermissionsPage01.signOutButton.click();
        CommPage.sleep(15);
        LoginPage.AssociateLogin()
        CommPage.sleep(20);
        HomePage.verifyPermissionsOptionNotPresent();

    })

})