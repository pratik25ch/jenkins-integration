var CommPage = require("../../../lib/CommLib/comm_lib")
var LoginPage = require("../../../pages/login/login_po");
var VerifyPage = require("../../../lib/actionLib/verify");
var HelperPage = require("../../../utils/helperUtils/helper");
var HomePage = require("../../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../../pages/timesheets/timesheets01_po")
var TemplatePage01 = require("../../../pages/administration/templates/templates01_po")
var TemplatePage02 = require("../../../pages/administration/templates/templates02_applyToAll_po")
var TemplatePage03 = require("../../../pages/administration/templates/templates03_defaultTemp_po")
//var SettingsPage = require("../../../pages/administration/settings/settings_po")

let R = Math.random().toString(36).substring(7);
/**
 * Should Add Client.
 * Should Add Matter.
 * Should Check The Task.
 * Should Check The METADATA.
 * Should Add Apply To All Matter Task.
 * Should Check Apply to All Matter Task.
 * Should Check The METADATA After New Attribute
 * 
 */

// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     CommPage.sleep(3);
//     HomePage.Administration();
//     CommPage.sleep(0.5);
//     HomePage.Template();
//     CommPage.sleep(5);
// })

describe('Template Module:--- Apply To All Matter', function () {
    it('Should Add Client', function () {
        HomePage.Matter();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        GCMPage01.mouseMoveFirst();
        GCMPage01.newClientBtn.click();
        CommPage.sleep(2);

        GCMPage01.clientName.sendKeys("New Client-", R);
        GCMPage01.panCard.sendKeys('ABCDE1234J');
        // GCMPage01.clientTagContacts.sendKeys('Soham');
        // CommPage.sleep(5);
        // GCMPage01.firstOption.click();

        GCMPage01.saveBtn.click().then(function () {
            console.log("CLIENT created sucessfully")
            expect('CLIENT created sucessfully').toEqual('CLIENT created sucessfully');
        })
        CommPage.sleep(2);
    });
    it('Should Add Matter', function () {
        GCMPage01.firstClient.click();

        GCMPage01.newMatterBtn.click();
        CommPage.sleep(3);
        GCMPage01.matterName.sendKeys('Test Case-', R);
        CommPage.sleep(5);
        GCMPage01.selectMember.sendKeys('Debasish Patra');
        CommPage.sleep(3)
        GCMPage01.firstOption.click();
        CommPage.sleep(1);
        GCMPage01.templateOption.click();
        GCMPage01.categoryName.click();
        GCMPage01.firstOption.click();
        GCMPage01.templateName.click();
        CommPage.sleep(0.5);
        GCMPage01.firstOption.click();
        CommPage.sleep(2);
        GCMPage01.saveBtn.click().then(function () {
            console.log("Matter Created sucessfully")
            expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
        })
        CommPage.sleep(2);
    })
    it('Should Check The Task', function () {
        GCMPage01.searchMatterValue.sendKeys('Test Case-', R);
        GCMPage01.firstMatter.click();
        CommPage.sleep(5);
        TemplatePage02.taskDetails();
        console.log("Task Added Sucessfully")
        expect('Task Added Sucessfully').toEqual('Task Added Sucessfully');
    })
    it('Should Check The METADATA', function () {
        //Details
        GCMPage02.details.click();
        CommPage.sleep(1);
        GCMPage02.metaData.click().then(function () {
            console.log("MetaData Displayed Sucessfully")
            expect('MetaData Displayed Sucessfully').toEqual('MetaData Displayed Sucessfully');
        })
        CommPage.sleep(2);
    })
    it('Should Add Apply To All Matter Task', function () {
        GCMPage02.closeMetaData.click();
        CommPage.sleep(1);
        HomePage.Administration();
        HomePage.Template();
        TemplatePage01.firstTemp.click();
        CommPage.sleep(1);
        //Add attribute
        TemplatePage01.attributeName.sendKeys('New Attribute', R);
        TemplatePage01.addAttribute.click();
        CommPage.sleep(1);
        //add attribute by LIST
        TemplatePage01.attributeName.sendKeys('Attribute Changed');
        TemplatePage01.attributeType.click();
        TemplatePage01.matVauleOption.click();
        TemplatePage01.commaOptions.sendKeys('Test ,').click();
        TemplatePage01.addAttribute.click();
        CommPage.sleep(2);
        //add Task
        TemplatePage01.taskName.sendKeys('APPLY TO ALL UNIT test ', R);
        TemplatePage01.taskDescription.sendKeys('Checking the Functionality of an Application');
        TemplatePage01.addTaskBtn.click();
        TemplatePage02.applytoAllCheckbox.click();
        CommPage.sleep(2);
        TemplatePage01.saveTemplate.click().then(function () {
            console.log("APPLY TO all MATTERS Added")
            expect('APPLY TO all MATTERS Added').toEqual('APPLY TO all MATTERS Added');
        })
    })
    it('Should Check Apply to All Matter Task', function () {
        CommPage.sleep(1);
        CommPage.back();
        CommPage.sleep(3);
        TemplatePage02.taskDetails();
        console.log("APPLY TO all MATTERS Displayed")
        expect('APPLY TO all MATTERS Displayed').toEqual('APPLY TO all MATTERS Displayed');
        CommPage.sleep(2);
    })
    it('Should Check The METADATA After New Attribute', function () {
        //Details
        GCMPage02.details.click();
        CommPage.sleep(1);
        GCMPage02.metaData.click().then(function () {
            console.log("MetaData Displayed Sucessfully")
            expect(element(by.xpath("//p[contains(text(),'Attribute Changed:')]")).isPresent()).toBe(true);
        })
        CommPage.sleep(2);
    })
})