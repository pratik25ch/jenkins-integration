var CommPage = require("../../../lib/CommLib/comm_lib")
var VerifyPage = require("../../../lib/actionLib/verify")
var HelperPage = require("../../../utils/helperUtils/helper")
var LoginPage = require("../../../pages/login/login_po")
var HomePage = require("../../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../../pages/timesheets/timesheets01_po")
var TemplatePage01 = require("../../../pages/administration/templates/templates01_po")
var TemplatePage02 = require("../../../pages/administration/templates/templates02_applyToAll_po")
var TemplatePage03 = require("../../../pages/administration/templates/templates03_defaultTemp_po")
//var SettingsPage = require("../../../pages/administration/settings/settings_po")

let R = Math.random().toString(36).substring(7);
/**
 * Should Add as Default Template.
 * Should Add Matter.
 * Should Check the Task.
 * 
 */
// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     CommPage.sleep(3);
//     HomePage.Administration();
//     CommPage.sleep(0.5);
//     HomePage.Template();
//     CommPage.sleep(5);
// })

describe('Template Module:--- Default Template Task', function () {
    it('Should Add as Default Template', function () {
        GCMPage02.closeMetaData.click();
        HomePage.Template();
        CommPage.sleep(1);
        TemplatePage01.firstTemp.click();
        CommPage.sleep(1);
        TemplatePage03.addDefaultTemplate.click();
        TemplatePage01.saveTemplate.click().then(function () {
            console.log('Template Updated Successfully');
            expect('Template Updated Successfully').toEqual('Template Updated Successfully');
        })
    })
    it('Should Add Matter', function () {
        HomePage.Matter();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        GCMPage01.firstClient.click();
        //CommPage.refresh();
        CommPage.sleep(5)
        GCMPage01.newMatterBtn.click();
        CommPage.sleep(3)
        GCMPage01.matterName.sendKeys('Default Temp-', R);
        CommPage.sleep(5);
        GCMPage01.selectMember.sendKeys('Debasish Patra');
        CommPage.sleep(3)
        GCMPage01.firstOption.click();
        CommPage.sleep(1);
        GCMPage01.templateOption.click();
        CommPage.sleep(2);
        GCMPage01.saveBtn.click().then(function () {
            console.log("Matter Created sucessfully")
            expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
        })
        CommPage.sleep(1);
    })
    it('Should Check the Task ', function () {
        GCMPage01.viewMatter.click().then(function () {
            CommPage.sleep(3);
            console.log("Matter Displayed sucessfully")
            expect('Matter Displayed sucessfully').toEqual('Matter Displayed sucessfully');
        })
    })
    it('Should SignOut the TEMPLATE Module', function () {
        LoginPage.signOut();
        CommPage.sleep(5);
        console.log('Template Module SignOut Successfully')
    })
})