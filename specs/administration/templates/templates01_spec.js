var CommPage = require("../../../lib/CommLib/comm_lib")
var LoginPage = require("../../../pages/login/login_po")
var HomePage = require("../../../lib/homepageLib/homepage_lib")
var GCMPage = require("../../../pages/gcm/gcm01_main_po")
var TimesheetPage = require("../../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../../pages/billing/billing_po")
var TemplatePage01 = require("../../../pages/administration/templates/templates01_po")
var TemplatePage02 = require("../../../pages/administration/templates/templates02_applyToAll_po")
var TemplatePage03 = require("../../../pages/administration/templates/templates03_defaultTemp_po")
//var SettingsPage = require("../../../pages/administration/settings/settings_po")

let R = Math.random().toString(36).substring(7);
/**
 * Should Create Category.
 * Duplicate Category : Should Display "Category Already Exist.
 * Should Edit Catagory.
 * Should Add New Template.
 * Duplicate Template : Should Display "Template Already Exist.
 * Should Edit Template.
 * 
 */
    // beforeAll(function () {
    //     LoginPage.GetURL();
    //     LoginPage.login()
    //     CommPage.sleep(3);
    //     HomePage.Administration();
    //     CommPage.sleep(0.5);
    //     HomePage.Template();
    //     CommPage.sleep(5);
    // })
describe('Template Module Login', function () {
    it('Should Login Again', async function () {
        LoginPage.GetURL();
        LoginPage.login()
        CommPage.sleep(3);
        HomePage.Administration();
        CommPage.sleep(0.5);
        HomePage.Template();
        CommPage.sleep(5);
    })
})
describe('Template Module:--- Add & Edit Category', function () {
    it('Should Create Category', async function () {
        //Create Category
        TemplatePage01.newCategoryBtn.click();
        TemplatePage01.categoryInput.sendKeys('-A Category-', R);
        TemplatePage01.saveCategory.click().then(function () {
            console.log('Category Created Sucessfully')
            expect('Category Created Sucessfully').toEqual('Category Created Sucessfully');
        })
        CommPage.sleep(2);
    })
    it('Duplicate Category : Should Display "Category Already Exist" ', async function () {
        TemplatePage01.newCategoryBtn.click();
        TemplatePage01.categoryInput.sendKeys('-A Category-', R);

        TemplatePage01.saveCategory.click().then(function () {
            console.log('Category Already Exist');
            expect('Category Already Exist').toEqual('Category Already Exist');
        })
        //duplicate category cancel btn
        TemplatePage01.discardBtn.click();
    })
    it('Should Edit Catagory', function () {
        CommPage.sleep(2);
        //CommPage.refresh()                
        //Edit Category
        TemplatePage01.mousemove();
        TemplatePage01.editCategoryBtn.click();
        CommPage.sleep(1);
        //clear category & edit
        TemplatePage01.categoryInput.clear().sendKeys('-A New Category-', R)

        TemplatePage01.saveCategory.click().then(function () {
            console.log('Category Updated Sucessfully')
            expect('Category Updated Sucessfully').toEqual('Category Updated Sucessfully');
        })
        CommPage.sleep(2);
    })
})
describe('Template Module:--- Add & Edit Template', function () {
    it('Should Add New Template', function () {
        CommPage.refresh();
        CommPage.sleep(2);
        TemplatePage01.mousemove();
        TemplatePage01.newTemplate.click();
        //Temp Name
        TemplatePage01.templateName.sendKeys('A New Temp ', R);
        //Add attribute
        TemplatePage01.attributeName.sendKeys('Check Automation');
        TemplatePage01.addAttribute.click();
        //add attribute by LIST
        TemplatePage01.attributeName.sendKeys('Attribute Check');
        TemplatePage01.attributeType.click();
        TemplatePage01.matVauleOption.click();
        TemplatePage01.commaOptions.sendKeys('Unit Test ,').click();
        CommPage.sleep(2);
        TemplatePage01.commaOptions.sendKeys('Regression Test ,').click();
        TemplatePage01.addAttribute.click();
        CommPage.sleep(2);
        //add Task
        TemplatePage01.taskName.sendKeys('Template test');
        TemplatePage01.taskDescription.sendKeys('Checking the Functionality of an Application');
        TemplatePage01.addTaskBtn.click();
        //Save Template
        TemplatePage01.saveTemplate.click().then(function () {
            console.log('Template Created Sucessfully')
            expect('Template Created Sucessfully').toEqual('Template Created Sucessfully');
        })
        CommPage.sleep(2);
    })
    it('Duplicate Template : Should Display "Template Already Exist" ', async function () {
        TemplatePage01.mousemove();
        TemplatePage01.newTemplate.click();
        //Temp Nmae
        TemplatePage01.templateName.sendKeys('A New Temp ', R);
        TemplatePage01.taskName.sendKeys('Unit test', R);
        TemplatePage01.saveTemplate.click().then(function () {
            console.log('Template Already Exist')
            expect('Template Already Exist').toEqual('Template Already Exist');
        })
    })
    it('Should Edit Template', function () {
        TemplatePage01.cancelBtn.click();
        //CommPage.refresh();
        CommPage.sleep(2);
        //click on Temp
        TemplatePage01.firstTemp.click();
        TemplatePage01.templateName.clear().sendKeys('Edit Temp ', R);
        //Add attribute
        TemplatePage01.attributeName.sendKeys('Flowace ', R);
        TemplatePage01.addAttribute.click();
        //add attribute by LIST
        TemplatePage01.attributeName.sendKeys('AON ', R);
        TemplatePage01.attributeType.click();
        TemplatePage01.matVauleOption.click();
        TemplatePage01.commaOptions.sendKeys('Unit Test ,').click();
        CommPage.sleep(1);
        TemplatePage01.addAttribute.click();
        //remove attributes
        TemplatePage01.removeBtn.click();
        //add Task
        TemplatePage01.taskName.sendKeys('Unit test', R);
        TemplatePage01.taskDescription.sendKeys('Checking the Functionality of an Application');
        TemplatePage01.addTaskBtn.click();
        TemplatePage01.taskName.sendKeys('Intergration test', R);
        TemplatePage01.taskDescription.sendKeys('Checking the Application');
        TemplatePage01.addTaskBtn.click();
        CommPage.sleep(2);
        TemplatePage01.saveTemplate.click().then(function () {
            console.log('Template Updated Sucessfully')
            expect('Template Updated Sucessfully').toEqual('Template Updated Sucessfully');
        })
        CommPage.sleep(1);
    });
})