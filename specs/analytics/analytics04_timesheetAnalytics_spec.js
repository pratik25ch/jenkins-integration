var CommPage = require("../../lib/CommLib/comm_lib");
var LoginPage = require("../../pages/login/login_po");
var HomePage = require("../../lib/homepageLib/homepage_lib");
var VerifyPage = require("../../lib/actionLib/verify");
var HelperPage = require("../../utils/helperUtils/helper");
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var TimesheetPage01 = require("../../pages/timesheets/timesheets01_po");
var BillingPage = require("../../pages/billing/billing_po");
var AnalyticsPage01 = require("../../pages/analytics/analytics01_matterAnalytics_po");
var AnalyticsPage02 = require("../../pages/analytics/analytics02_financialAnalytics_po");
var AnalyticsPage03 = require("../../pages/analytics/analytics03_memberAnalytics_po");
var AnalyticsPage04 = require("../../pages/analytics/analytics04_timesheetAnalytics_po");

let R = Math.random().toString(36).substring(7);

// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     HomePage.Timesheet();
//     HelperPage.waitForDisplay(VerifyPage.timesheetlogs_verify)
// })
describe('ANALYTICS MODULE', function () {
    it('Should Go to Timesheet', async function () {
        HomePage.Timesheet();
        // HelperPage.waitForDisplay(VerifyPage.timesheetlogs_verify)
    })
})

describe('ANALYTICS MODULE :--- TIMESHEET Analytics--611', function () {
        let t1,t2;
    it('Should check the Classsified & Unclassified Time', async function () {
        t1 = await TimesheetPage01.timesheetUnclassifiedTime();
        console.log('Timesheet Unclassified Hours ', t1);
        t2 = await TimesheetPage01.timesheetClassifiedTime();
        console.log('Timesheet Classified Hours ', t2);
        // expect(VerifyPage.timesheetlogs_verify.isPresent()).toBe(true);
    })
    it('Should move to the Analytics Section', async function () {
        HomePage.Analytics();
        HelperPage.waitForDisplay(VerifyPage.matterAnalytics_verify);
        AnalyticsPage04.timesheetAnalytics();
        HelperPage.waitForDisplay(VerifyPage.timesheetAnalytics_verify);
        expect(VerifyPage.timesheetAnalytics_verify.isPresent()).toBe(true);
    })
    it('Should Analyse Todays Data', async function () {
        AnalyticsPage01.startDate.click();
        AnalyticsPage01.FirstCalender();
        CommPage.sleep(0.5);
        AnalyticsPage01.todaysDate.click();
        CommPage.sleep(2);
        AnalyticsPage04.getReport.click();
        CommPage.sleep(2);
        expect("Data Displayed Successfully").toEqual("Data Displayed Successfully");
    })
    it('Should Search the User name', async function () {
        AnalyticsPage04.searchMember.sendKeys("Debasish").then(function () {
            console.log("User searched Successfully");
            expect(VerifyPage.timesheetAnalytics_verify.isPresent()).toBe(true);
        })
    })
    it('Should compare the time with Analytics Section', async function () {
        let v1 = await AnalyticsPage04.TimesheetAnalytics_UCF();
        console.log('Timesheet Analytics Unclassified Hours ', v1);
        let v2 = await AnalyticsPage04.TimesheetAnalytics_CF();
        console.log('Timesheet Analytics Classified Hours ', v2);
        expect(t1).toEqual(v1);
        expect(t2).toEqual(v2);
    })
})
describe('ANALYTICS MODULE :--- TIMESHEET Analytics', function () {
    it('Should Check the Accending & Decending Timesheet Analytics', async function () {
        CommPage.refresh();
        CommPage.sleep(3);
        AnalyticsPage04.timesheetAnalytics();
        HelperPage.waitForDisplay(VerifyPage.timesheetAnalytics_verify);
        AnalyticsPage04.timesheetAnalytics_AD();
    })
})