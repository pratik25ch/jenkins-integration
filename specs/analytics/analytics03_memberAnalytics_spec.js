var CommPage = require("../../lib/CommLib/comm_lib")
var LoginPage = require("../../pages/login/login_po")
var VerifyPage = require("../../lib/actionLib/verify")
var HelperPage = require("../../utils/helperUtils/helper")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")
var AnalyticsPage01 = require("../../pages/analytics/analytics01_matterAnalytics_po")
var AnalyticsPage02 = require("../../pages/analytics/analytics02_financialAnalytics_po")
var AnalyticsPage03 = require("../../pages/analytics/analytics03_memberAnalytics_po")
var AnalyticsPage04 = require("../../pages/analytics/analytics04_timesheetAnalytics_po")

let R = Math.random().toString(36).substring(7);

// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login();
//     HomePage.Analytics();
//     HelperPage.waitForDisplay(VerifyPage.matterAnalytics_verify);
// })

describe('ANALYTICS MODULE :--- MEMBER Analyitcs--548', function () {
    it('Should go to Member Anlytics', function () {
        CommPage.refresh();
        HelperPage.waitForDisplay(VerifyPage.matterAnalytics_verify);
        AnalyticsPage03.memberAnalytics();
        HelperPage.waitForVisibility(VerifyPage.memberAnalytics_verify);
        expect(VerifyPage.memberAnalytics_verify.isPresent()).toBe(true);
    })
    it('Should Check The Previous Month Anlytics', function () {
        AnalyticsPage03.previousMonth.click();
        CommPage.sleep(3);
        AnalyticsPage03.allEntriesText();
        expect(VerifyPage.memberAnalytics_verify.isPresent()).toBe(true);
    })
    it('Should Check Current Month Anlytics-500', function () {
        AnalyticsPage03.nextMonth.click();
        CommPage.sleep(3);
        AnalyticsPage03.allEntriesText();
        expect(VerifyPage.memberAnalytics_verify.isPresent()).toBe(true);
    })
    it('Should Change the user name & check the Anlytics', function () {
        AnalyticsPage03.selectUser.click();
        AnalyticsPage03.userInput.sendKeys('namrata@flowace.in');
        AnalyticsPage03.firstOption.click();
        CommPage.sleep(3);
    })


    

    
})