var CommPage = require("../../lib/CommLib/comm_lib")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")
var AnalyticsPage01 = require("../../pages/analytics/analytics01_matterAnalytics_po")
var AnalyticsPage02 = require("../../pages/analytics/analytics02_financialAnalytics_po")
var AnalyticsPage03 = require("../../pages/analytics/analytics03_memberAnalytics_po")
var AnalyticsPage04 = require("../../pages/analytics/analytics04_timesheetAnalytics_po")

let R = Math.random().toString(36).substring(7);

beforeAll(function () {
    // LoginPage.GetURL();
    LoginPage.login()
    CommPage.sleep(3);
    HomePage.Analytics();
    CommPage.sleep(3);
})

describe('ANALYTICS MODULE :--- Download Report', function () {
    it('Should Analyze Report', function () {
        CommPage.sleep(5);
        AnalyticsPage01.selectAssociate.click();
        AnalyticsPage01.associateInput.sendKeys("qa1@flowace.in");
        AnalyticsPage01.firstOption.click();
        //click on calender Icon
        AnalyticsPage01.startDate.click();
        CommPage.sleep(2);
        AnalyticsPage01.FirstCalender();
        CommPage.sleep(0.5);
        AnalyticsPage01.todaysDate.click();
        CommPage.sleep(2);
        AnalyticsPage01.analyzeReport.click();
        CommPage.sleep(1);
        console.log('Report Analysed Sucessfully')
        expect('Report Analysed Sucessfully').toEqual('Report Analysed Sucessfully');
    })
    it('Should Download Report', function () {
        AnalyticsPage01.analyzeData();
        //new Matter Data
        AnalyticsPage01.newMatterData.click();
        CommPage.sleep(2);
        AnalyticsPage01.closeModel.click();
        AnalyticsPage01.downloadReport.click();
        CommPage.sleep(1);

        console.log('Anlytics Report Downloaded Sucessfully')
        expect('Anlytics Report Downloaded Sucessfully').toEqual('Anlytics Report Downloaded Sucessfully');
    })
})
describe('ANALYTICS MODULE :--- Matter OverView', function () {
    it('Should Create Matter through matter icon', function () {
        CommPage.refresh();
        GCMPage01.gcm_plusIcon.click();
        CommPage.sleep(1);
        GCMPage01.iconMatterDropDown.click();
        CommPage.sleep(5);
        GCMPage01.matSelectGroup.click();
        GCMPage01.secondOption.click();
        GCMPage01.matSelectClient.click();
        GCMPage01.secondOption.click();
        GCMPage01.matterName.sendKeys('-Icon Matter-', R)
        GCMPage01.selectMember.sendKeys("QA One");
        CommPage.sleep(3);
        GCMPage01.firstOption.click();
        GCMPage01.saveBtn.click();
        CommPage.sleep(2);
        console.log("Matter Created sucessfully")
        expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
    })
    it('Should Show The Matter Overview Data', function () {
        CommPage.refresh();
        CommPage.sleep(5);
        AnalyticsPage01.selectAssociate.click();
        AnalyticsPage01.associateInput.sendKeys("qa1@flowace.in");
        AnalyticsPage01.firstOption.click();
        //click on calender Icon
        AnalyticsPage01.startDate.click();
        AnalyticsPage01.FirstCalender();
        CommPage.sleep(0.5);
        AnalyticsPage01.todaysDate.click();
        CommPage.sleep(2);
        AnalyticsPage01.analyzeReport.click();
        CommPage.sleep(1);
        AnalyticsPage01.analyzeData();
        console.log("Matter Overview Data")
        expect('Matter Overview Data').toEqual('Matter Overview Data');
    })
    it('Should Show New Matter Data', function () {
        //new Matter Data
        AnalyticsPage01.newMatterData.click();
        CommPage.sleep(2);
        console.log("Total New Matter Data")
        expect('Total New Matter Data').toEqual('Total New Matter Data');
    })
    it('Should Show Close Matter Data', function () {
        AnalyticsPage01.closeModel.click();
        AnalyticsPage01.analyzeData();
        CommPage.sleep(2);
        //Closed Matter Data
        AnalyticsPage01.closedMatterData.click();
        CommPage.sleep(2);
        console.log("Total Closed Matter Data")
        expect('Total Closed Matter Data').toEqual('Total Closed Matter Data');
    })
    it('Should Show Active Matter Data', function () {
        AnalyticsPage01.closeModel.click();
        AnalyticsPage01.analyzeData();
        CommPage.sleep(2);
        //Closed Matter Data
        AnalyticsPage01.activeMatterData.click();
        CommPage.sleep(2);
        console.log("Total Active Matter Data")
        expect('Total Active Matter Data').toEqual('Total Active Matter Data');
    })
})
describe('ANALYTICS MODULE :--- Modified Meta Data', function () {
    it('Should Count & Descending Active Matter Data', function () {
        AnalyticsPage01.matterList();
        console.log("Total Matter list Counted ")
        expect('Total Matter list Counted').toEqual('Total Matter list Counted');
        AnalyticsPage01.descending.click();
        CommPage.sleep(1);
        console.log("Descending Active Matter Data")
        expect('Descending Active Matter Data').toEqual('Descending Active Matter Data');
    })
    it('Should Open a New Window', function () {
        AnalyticsPage01.firstMatter.click();
        CommPage.sleep(5);
        AnalyticsPage01.childWindow();
        console.log("New Window Opened Sucessfullly")
        expect('New Window Opened Sucessfullly').toEqual('New Window Opened Sucessfullly');
    })
    it('Should Captrured Created & Modified Matter Data', function () {
        GCMPage02.details.click();
        //AnalyticsPage01.details.click();
        CommPage.sleep(1);
        console.log("Matter Details Opened Sucessfullly")
        expect('Matter Details Opened Sucessfullly').toEqual('Matter Details Opened Sucessfullly');
    })
    it('Should Archive the Matter', function () {
        GCMPage01.archiveBtn.click();
        //AnalyticsPage01.archive.click()
        CommPage.sleep(0.5);
        GCMPage01.confirmBtn.click();
        //AnalyticsPage01.confirmBtn.click();
        CommPage.sleep(2);
        console.log("Matter Archived Sucessfullly")
        expect('Matter Archived Sucessfullly').toEqual('Matter Archived Sucessfullly');
    })
    it('Should Again Check the Matter View', function () {
        AnalyticsPage01.parentWindow();
        CommPage.sleep(1);
        AnalyticsPage01.closeModel.click();
        CommPage.sleep(2);
        CommPage.refresh();

        CommPage.sleep(5);
        AnalyticsPage01.selectAssociate.click();
        AnalyticsPage01.associateInput.sendKeys("qa1@flowace.in");
        AnalyticsPage01.firstOption.click();
        //click on calender Icon
        AnalyticsPage01.startDate.click();
        AnalyticsPage01.FirstCalender();

        CommPage.sleep(0.5);
        AnalyticsPage01.todaysDate.click();
        CommPage.sleep(2);
        AnalyticsPage01.analyzeReport.click();
        CommPage.sleep(3);
        console.log('Matter Overview Data')
        expect('Matter Overview Data').toEqual('Matter Overview Data');
    })
    it('Should Check Closed Matter', function () {
        AnalyticsPage01.closedMatterData.click();
        CommPage.sleep(2);
        console.log("Total Closed Matter Data")
        expect('Total Closed Matter Data').toEqual('Total Closed Matter Data');
    })
    it('Should UNARCHIVE the Matter', function () {
        AnalyticsPage01.firstMatter.click();
        CommPage.sleep(5);
        AnalyticsPage01.childWindow();
        GCMPage01.unarchiveBtn.click();
        //AnalyticsPage01.unarchive.click();
        CommPage.sleep(0.5);
        GCMPage01.confirmBtn.click();
        //AnalyticsPage01.confirmBtn.click();
        console.log("Matter UnArchived Sucessfullly")
        expect('Matter UnArchived Sucessfullly').toEqual('Matter UnArchived Sucessfullly');
    })
    it('Should Again Check the Matter View', function () {
        CommPage.sleep(3);
        AnalyticsPage01.parentWindow();
        CommPage.sleep(1);
        AnalyticsPage01.closeModel.click();
        CommPage.sleep(2);
        CommPage.refresh();

        CommPage.sleep(5);
        AnalyticsPage01.selectAssociate.click();
        AnalyticsPage01.associateInput.sendKeys("qa1@flowace.in");
        AnalyticsPage01.firstOption.click();
        //click on calender Icon
        AnalyticsPage01.startDate.click();
        AnalyticsPage01.FirstCalender();

        CommPage.sleep(0.5);
        AnalyticsPage01.todaysDate.click();
        CommPage.sleep(2);
        AnalyticsPage01.analyzeReport.click();
        CommPage.sleep(3);
        console.log('Matter Overview Data')
        expect('Matter Overview Data').toEqual('Matter Overview Data');
    })
    it('Should check the timesheet & Analytics', function () {
        HomePage.Timesheet();
        CommPage.sleep(2);

        TimesheetPage.manualEntryBtn();
        CommPage.sleep(2);

        TimesheetPage.associateName.clear();
        TimesheetPage.associateName.sendKeys('qa1@flowace.in');
        CommPage.sleep(0.5);
        TimesheetPage.firstOption.click();
        CommPage.sleep(1);

        TimesheetPage.selectMatter.click();
        TimesheetPage.searchMatter.sendKeys('-Icon Matter-', R);
        TimesheetPage.firstEntry.click();

        TimesheetPage.manualEntryName.sendKeys('Analytics Timesheet Check');
        TimesheetPage.validateBtn.click();
        CommPage.sleep(2);
        console.log('Timesheet Entry Sucessfully Added')
        expect('Timesheet Entry Sucessfully Added').toEqual('Timesheet Entry Sucessfully Added');
        HomePage.Analytics();
        CommPage.sleep(3);

        AnalyticsPage01.selectAssociate.click();
        AnalyticsPage01.associateInput.sendKeys("qa1@flowace.in");
        AnalyticsPage01.firstOption.click();

        //click on calender Icon
        AnalyticsPage01.startDate.click();
        AnalyticsPage01.FirstCalender();

        CommPage.sleep(0.5);
        AnalyticsPage01.todaysDate.click();
        CommPage.sleep(2);
        AnalyticsPage01.analyzeReport.click();
        CommPage.sleep(1);

        AnalyticsPage01.searchMatter_Analytics.sendKeys('-Icon Matter-', R)
        CommPage.sleep(3);
        console.log('Analytics Matter Search Displayed Sucessfully')
        expect('Analytics Matter Search Displayed Sucessfully').toEqual('Analytics Matter Search Displayed Sucessfully');
    })
})