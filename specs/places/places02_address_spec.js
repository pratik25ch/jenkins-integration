var CommPage = require("../../lib/CommLib/comm_lib")
var VerifyPage = require("../../lib/actionLib/verify")
var HelperPage = require("../../utils/helperUtils/helper")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")
var PlacesPage01 = require("../../pages/places/places01_viewer_po")
var PlacesPage02 = require("../../pages/places/places02_address_po")
var ContactsPage01 = require("../../pages/administration/contacts/contacts01_po")
var ContactsPage03 = require("../../pages/administration/contacts/contacts03_tag_po")
var EmployeePage01 = require("../../pages/administration/employees/employees01_po")
var EmployeePage02 = require("../../pages/administration/employees/employees02_assign_po.js")
var EmployeePage03 = require("../../pages/administration/employees/employees03_role_po.js")

let R = Math.random().toString(36).substring(7);
/**
 * 1- Should Create a Group Through Icon.
 * 2- Should Move to Places Section.
 * 3- Should Display the Location of New Group
 * 4- Should Detete the Place Group
 * 
 * 1- Should Create a Client.
 * 2- Should Move to Places Section.
 * 3- Should Display the Location of New Client
 * 4- Should Detete the Place Client.
 * 
 * 1- Should Create a MATTER.
 * 2- Should Move to Places Section.
 * 3- Should Display the Location of New MATTER
 * 4- Should Detete the Place MATTER.
 * 
 * 
 */
// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     CommPage.sleep(3);
// })
describe('Address Module: Group Section', function () {
    it('Should Create a Group Through Icon', async function () {
        HomePage.Matter();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        GCMPage01.gcm_plusIcon.click();
        CommPage.sleep(1);
        GCMPage01.iconGroupDropDown.click();
        await GCMPage01.groupName.sendKeys("Places Group-", R);
        await GCMPage01.groupTagContacts.sendKeys('Debasish Patra');
        CommPage.sleep(2);
        await GCMPage01.firstOption.click();
        GCMPage01.placeType.click();
        GCMPage01.firstOption.click();
        CommPage.sleep(1);
        GCMPage01.address.sendKeys('flowace');
        CommPage.sleep(1);
        CommPage.downArrow();
        CommPage.sleep(2);
        CommPage.enterBtn();
        CommPage.sleep(4)
        await GCMPage01.saveBtn.click().then(function () {
            expect(VerifyPage.newGroup_msg.getText()).toContain("created Successfully");
            console.log('GROUP created sucessfully');
        })
        CommPage.sleep(2);
    })
    it('Should Move to Address Section', async function () {
        CommPage.refresh();
        HomePage.Places();
        CommPage.sleep(5)
        PlacesPage02.address();
        expect(VerifyPage.addressPage_verify.isPresent()).toBe(true);
    })
    it('Should Display the Location of New Group', async function () {
        CommPage.sleep(2);
        PlacesPage02.groupAddress.click();
        CommPage.sleep(2);
        PlacesPage02.mouseMoveLast();
        CommPage.sleep(2);
        PlacesPage02.lastElement.click().then(function () {
            expect("Group Places Displayed Successfully").toEqual("Group Places Displayed Successfully");
            console.log('Group Places Displayed Successfully');
        })
        CommPage.sleep(3);
    })
    it('Should Detete the New Group', async function () {
        HomePage.Matter();
        CommPage.sleep(2);
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(1);
        GCMPage03.globalGroupSearch.click();
        CommPage.sleep(1);
        GCMPage03.matterSearchInput.sendKeys("Places Group-", R);
        CommPage.sleep(1);
        GCMPage01.globalSearchMouseMoveFirst();
        CommPage.sleep(1);
        GCMPage01.editGlobalGroup.click();
        CommPage.sleep(1);
        GCMPage04.deleteBtn.click();
        CommPage.sleep(5);
        GCMPage04.deleteBtn.click().then(function () {
            console.log("Group Deleted sucessfully")
        })
        expect(VerifyPage.deleteGroup_msg.isPresent()).toBe(true);
        CommPage.sleep(4);
        CommPage.refresh();
        CommPage.sleep(3);
    })
})
describe('Address Module: Client Section', function () {
    it('Should Create a Client', async function () {
        GCMPage01.mouseMoveFirst();
        GCMPage01.newClientBtn.click();
        CommPage.sleep(2);
        GCMPage01.clientName.sendKeys("Places Client-", R);
        GCMPage01.placeType.click();
        GCMPage01.firstOption.click();
        CommPage.sleep(1);
        GCMPage01.address.sendKeys('flowace');
        CommPage.sleep(1);
        CommPage.downArrow();
        CommPage.sleep(2);
        CommPage.enterBtn();
        CommPage.sleep(4)
        await GCMPage01.saveBtn.click().then(function () {
            console.log("CLIENT created sucessfully")
        })
        expect(VerifyPage.newClient_msg.getText()).toContain('created Successfully');
        CommPage.sleep(2);
    })
    it('Should Move to Address Section', async function () {
        CommPage.refresh();
        HomePage.Places();
        CommPage.sleep(5)
        PlacesPage02.address();
        expect(VerifyPage.addressPage_verify.isPresent()).toBe(true);
    })
    it('Should Display the Location of New Client', async function () {
        CommPage.sleep(2);
        PlacesPage02.clientAddress.click();
        CommPage.sleep(2);
        PlacesPage02.mouseMoveLast();
        CommPage.sleep(2);
        PlacesPage02.lastElement.click().then(function () {
            expect("Client Places Displayed Successfully").toEqual("Client Places Displayed Successfully");
            console.log('Client Places Displayed Successfully');
        })
        CommPage.sleep(3);
    })
    it('Should Delete a Client', async function () {
        HomePage.Matter();
        CommPage.refresh();
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(2);
        GCMPage03.globalClientSearch.click();
        GCMPage03.matterSearchInput.sendKeys("Places Client-", R);
        CommPage.sleep(2);
        GCMPage03.globalSearch_firstClient.click();
        GCMPage01.editClient.click();
        CommPage.sleep(3);
        GCMPage04.deleteBtn.click();
        CommPage.sleep(1);
        GCMPage04.deleteBtn.click().then(function () {
            console.log("Client Deleted sucessfully")
        })
        expect(VerifyPage.deleteClient_msg.isPresent()).toBe(true);
        CommPage.sleep(5);
    })
})
describe('Address Module: MATTER Section', function () {
    it('Should Add Matter', async function () {
        GCMPage01.firstClient.click();
        GCMPage01.newMatterBtn.click();
        CommPage.sleep(5);
        await GCMPage01.matterName.sendKeys("Places Matter-", R);
        CommPage.sleep(1);
        GCMPage01.selectMember.sendKeys('Debasish Patra');
        CommPage.sleep(3);
        GCMPage01.firstOption.click();
        CommPage.sleep(3);
        GCMPage01.placeType.click();
        GCMPage01.firstOption.click();
        CommPage.sleep(1);
        GCMPage01.address.sendKeys('flowace');
        CommPage.sleep(1);
        CommPage.downArrow();
        CommPage.sleep(2);
        CommPage.enterBtn();
        CommPage.sleep(4)
        await GCMPage01.saveBtn.click().then(function () {
            console.log("Matter Created sucessfully")
        })
        expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
        CommPage.sleep(1);
    })
    it('Should Move to Address Section', async function () {
        CommPage.refresh();
        HomePage.Places();
        CommPage.sleep(5)
        PlacesPage02.address();
        expect(VerifyPage.addressPage_verify.isPresent()).toBe(true);
    })
    it('Should Display the Location of New Matter', async function () {
        CommPage.sleep(2);
        PlacesPage02.matterAddress.click();
        CommPage.sleep(2);
        PlacesPage02.mouseMoveLast();
        CommPage.sleep(2);
        PlacesPage02.lastElement.click().then(function () {
            expect("Matter Places Displayed Successfully").toEqual("Matter Places Displayed Successfully");
            console.log('Matter Places Displayed Successfully');
        })
        CommPage.sleep(3);
    })
    it('Should Delete Matter', async function () {
        HomePage.Matter();
        HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
        CommPage.refresh();
        CommPage.sleep(5);
        GCMPage03.globalSearchIcon.click();
        CommPage.sleep(2);
        GCMPage03.matterSearchInput.sendKeys("Places Matter-", R);
        GCMPage03.firstMatterSearch.click();
        CommPage.sleep(3);
        GCMPage04.deleteTooltip.click();
        CommPage.sleep(2)
        GCMPage04.deleteMatterBtn.click();
        CommPage.sleep(2);
        GCMPage04.deleteMatterData.click();
        GCMPage04.validateBtn.click();
        CommPage.sleep(1);
        GCMPage04.validateBtn.click();
        CommPage.sleep(2);
        GCMPage04.confirmMatterName.sendKeys("Places Matter-", R)
        CommPage.sleep(2);
        GCMPage04.validateBtn.click().then(function () {
            console.log("MATTER Deleted sucessfully");
        })
        expect(VerifyPage.deleteMatter_msg.isDisplayed()).toBeTruthy();
        CommPage.sleep(2);
    })
})
describe('Address Module: CONTACT Section', function () {
    it('Should Create a Contact', async function () {
        //CommPage.refresh();
        HomePage.Administration();
        HomePage.Contacts();
        ContactsPage01.newContact.click();
        CommPage.sleep(3);
        ContactsPage01.firstname.sendKeys("Places");
        ContactsPage01.lastname.sendKeys("Contact-", R);
        ContactsPage01.company.sendKeys('INTEL');
        ContactsPage01.primaryNumber.sendKeys('1122445588');
        CommPage.sleep(1);
        GCMPage01.placeType.click();
        GCMPage01.firstOption.click();
        CommPage.sleep(1);
        GCMPage01.address.sendKeys('flowace');
        CommPage.sleep(1);
        CommPage.downArrow();
        CommPage.sleep(2);
        CommPage.enterBtn();
        CommPage.sleep(4)
        ContactsPage01.saveBtn.click().then(function () {
            console.log('Contact Created Sucessfully')
            expect('Contact Created Sucessfully').toEqual('Contact Created Sucessfully');
        })
        CommPage.sleep(3);
    })
    it('Should Move to Address Section', async function () {
        CommPage.refresh();
        HomePage.Places();
        CommPage.sleep(5)
        PlacesPage02.address();
        expect(VerifyPage.addressPage_verify.isPresent()).toBe(true);
    })
    it('Should Display the Location of New Contact', async function () {
        CommPage.sleep(2);
        PlacesPage02.contactAddress.click();
        CommPage.sleep(2);
        PlacesPage02.mouseMoveLast();
        CommPage.sleep(2);
        PlacesPage02.lastElement.click().then(function () {
            expect("Matter Places Displayed Successfully").toEqual("Matter Places Displayed Successfully");
            console.log('Matter Places Displayed Successfully');
        })
        CommPage.sleep(3);
    })
    it('Should Delete Contact', function () {
        CommPage.refresh();
        HomePage.Administration();
        HomePage.Contacts();
        CommPage.sleep(1);
        ContactsPage01.searchContact.sendKeys("Places Contact-", R);
        ContactsPage01.editOption.click();
        CommPage.sleep(1);
        ContactsPage03.deleteContact.click();
        CommPage.sleep(1);
        ContactsPage03.confirmBtn.click();
        CommPage.sleep(2);
        console.log('Contact has been removed Sucessfully ')
        expect('Contact has been removed Sucessfully ').toEqual('Contact has been removed Sucessfully ');
    })
})
describe('Address Module: CONTACT Section--565', function () {
    it('Should Go to Employee', async function () {
        CommPage.refresh();
        HomePage.Administration();
        HomePage.Employee();
        CommPage.sleep(2);
        EmployeePage01.searchEmp.sendKeys('qa1@flowace.in');
        CommPage.sleep(2);
        EmployeePage01.editEmp.click();
        CommPage.sleep(2);
        EmployeePage01.placeType.click();
        CommPage.sleep(0.5);
        EmployeePage01.firstOption.click();
        CommPage.sleep(1);
        EmployeePage01.address.clear();
        CommPage.sleep(2)
        EmployeePage01.address.sendKeys('Millenium Bussiness Park');
        CommPage.sleep(2);
        CommPage.downArrow();
        CommPage.sleep(2);
        CommPage.enterBtn();
        CommPage.sleep(4)
        EmployeePage01.savebutton.click().then(function () {
            console.log('User Updated Sucessfully');
            expect('User Updated Sucessfully').toEqual('User Updated Sucessfully');
        })
    })
    it('Should Move to Address Section', async function () {
        CommPage.refresh();
        HomePage.Places();
        CommPage.sleep(5)
        PlacesPage02.address();
        expect(VerifyPage.addressPage_verify.isPresent()).toBe(true);
    })
    it('Should Display the Location of Employee', async function () {
        CommPage.sleep(2);
        PlacesPage02.employeeAddress.click();
        CommPage.sleep(2);
        PlacesPage02.mouseMoveLast();
        CommPage.sleep(2);
        PlacesPage02.lastElement.click().then(function () {
            expect("Matter Places Displayed Successfully").toEqual("Matter Places Displayed Successfully");
            console.log('Matter Places Displayed Successfully');
        })
        CommPage.sleep(3);
    })
    it('Should Go to Employee & Disable the Employee', async function () {
        HomePage.Administration();
        HomePage.Employee();
        CommPage.sleep(2);
        EmployeePage01.searchEmp.sendKeys('qa1@flowace.in');
        CommPage.sleep(1);
        EmployeePage01.editEmp.click();
        CommPage.sleep(1);
        EmployeePage01.disable.click().then(function () {
            expect("Empoyee Disable Successfully").toEqual("Empoyee Disable Successfully");
            console.log('Empoyee Disable Successfully');
        })
    })
    it('Should Move to Address Section', async function () {
        CommPage.refresh();
        HomePage.Places();
        CommPage.sleep(5)
        PlacesPage02.address();
        expect(VerifyPage.addressPage_verify.isPresent()).toBe(true);
        console.log('Address Page Displayed Successfully');
    })
    it('Should Display the Location of Employee', async function () {
        CommPage.sleep(2);
        PlacesPage02.employeeAddress.click();
        CommPage.sleep(2);
        expect(VerifyPage.Emp_verify.isPresent()).toBe(false);
        console.log('The User is not displayed');
        CommPage.sleep(3);
    })
    it('Should Go to Employee & Enable the Employee', async function () {
        HomePage.Administration();
        HomePage.Employee();
        CommPage.sleep(2);
        EmployeePage01.disabled.click();
        EmployeePage01.searchEmp.sendKeys('qa1@flowace.in');
        CommPage.sleep(1);
        EmployeePage01.editEmp.click();
        CommPage.sleep(1);
        EmployeePage01.enable.click().then(function () {
            expect("User Activated Successfully").toEqual("User Activated Successfully");
            console.log('User Activated Successfully');
        })
    })


})