var CommPage = require("../../lib/CommLib/comm_lib")
var VerifyPage = require("../../lib/actionLib/verify")
var HelperPage = require("../../utils/helperUtils/helper")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")
var PlacesPage01 = require("../../pages/places/places01_viewer_po")
var PlacesPage02 = require("../../pages/places/places02_address_po")

let R = Math.random().toString(36).substring(7);

beforeAll(function () {
    LoginPage.GetURL();
    LoginPage.login()
    CommPage.sleep(3);
    HomePage.Places();
    HelperPage.waitForDisplay(VerifyPage.viewerPage_verify);
})

describe('Places Module', function () {
    it('Should Move To Address', async function () {
        PlacesPage02.address();
        CommPage.sleep(3);
        console.log("Adderss Section Opened Successfully");
    })
    it('Should Move To Viewer', async function () {
        PlacesPage01.viewer();
        CommPage.sleep(3)
        console.log("Viewer Section Opened Successfully");
    })
})