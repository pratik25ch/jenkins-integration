var CommPage = require("../../lib/CommLib/comm_lib")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var TimesheetPage = require("../../pages/timesheets/timesheets01_po")
var VerifyPage = require("../../lib/actionLib/verify");
var TimesheetPage03 = require("../../pages/timesheets/timesheets03_classify_po")


let R = Math.random().toString(36).substring(7);



beforeAll(async function () {
    // LoginPage.GetURL();
    LoginPage.login()
    CommPage.sleep(3);
    // HomePage.Administration();
    CommPage.sleep(0.5);
    HomePage.Timesheet();
    CommPage.sleep(5);
})


describe('Timesheet MODULE :--- Classify timesheet', function () {
    //     it('Should able to get disabled button as no entry to classify', function () {
    //         TimesheetPage.unclassifiedButton.isPresent().then(function () {
    //             console.log('No entry to classify verified successfully')
    //         });
    //     })
    //     it('Should add timesheet entry', async function () {
    //         CommPage.sleep(3);

    //         TimesheetPage.manualEntryBtn();
    //         TimesheetPage.selectMatter.click();
    //         CommPage.sleep(3);

    //         // browser.actions().sendKeys(protractor.Key.ENTER).perform();

    //         TimesheetPage.firstEntry.click();
    //         CommPage.sleep(3);

    //         TimesheetPage.manualEntryName.sendKeys('Associate Timesheet');
    //         CommPage.sleep(3);

    //         TimesheetPage.validateBtn.click().then(function () {
    //             console.log('Timesheet Entry Added Sucessfully')
    //         })
    //         expect(VerifyPage.createManualEntry_msg.isPresent()).toBe(true);
    //         CommPage.sleep(3);

    //     })

    //     it('Should Check classify Time is Present or not', async function () {
    //         HomePage.Timesheet();
    //         let C1 = await TimesheetPage03.classifyBtnEnable();
    //         if (C1) {
    //             TimesheetPage03.classifyBtn();
    //         } else {
    //             console.log("There is no Data to Classify");
    //         }
    //     })
    //     it('Should Classify the Unclassified Entry', async function () {
    //         // browser.executeScript('window.open()');
    //         CommPage.sleep(3);

    //         LoginPage.GetURL();
    //         CommPage.sleep(15);

    //         LoginPage.gozo_login();
    //         CommPage.sleep(8);
    //         HomePage.Timesheet();
    //         CommPage.sleep(5);
    //         let enableClassify = await TimesheetPage03.classifyBtnEnable();
    //         if (enableClassify) {
    //             TimesheetPage03.classifyBtn();
    //             TimesheetPage.searchByGCM.sendKeys('W');
    //             TimesheetPage.firstOption.click();
    //             TimesheetPage.firstCheckbox.click();

    //             TimesheetPage.classifyDone.click();
    //             CommPage.sleep(1);
    //             TimesheetPage.confirmRecords.click().then(function () {
    //                 console.log('Unclassified Records Classified Sucessfully')
    //                 expect('Unclassified Records Classified Sucessfully').toEqual('Unclassified Records Classified Sucessfully');
    //             })

    //         } else {
    //             console.log("There is no Data to Classify");
    //         }

    //         CommPage.sleep(2);
    //     })


    //     it('Should Classify two Unclassified Entry', async function () {
    //         CommPage.sleep(3);

    //         // LoginPage.GetURL();
    //         // CommPage.sleep(15);

    //         // LoginPage.gozo_login();
    //         // CommPage.sleep(8);
    //         HomePage.Timesheet();
    //         CommPage.sleep(5);
    //         let enableClassify = await TimesheetPage03.classifyBtnEnable();
    //         if (enableClassify) {
    //             TimesheetPage03.classifyBtn();
    //             TimesheetPage.searchByGCM.sendKeys('W');
    //             TimesheetPage.firstOption.click();
    //             TimesheetPage.firstCheckbox.click();
    //             CommPage.sleep(2);

    //             TimesheetPage.secondCheckbox.click();
    //             // TimesheetPage01.thirdCheckbox.click();
    //             // TimesheetPage01.fourthCheckbox.click();
    //             // TimesheetPage01.fifthCheckbox.click();
    //             TimesheetPage.classifyDone.click();
    //             CommPage.sleep(1);
    //             TimesheetPage.confirmRecords.click().then(function () {
    //                 console.log('Two Unclassified Records Classified Sucessfully')
    //                 expect('Unclassified Records Classified Sucessfully').toEqual('Unclassified Records Classified Sucessfully');
    //             })

    //         } else {
    //             console.log("There is no Data to Classify");
    //         }
    //         // TimesheetPage03.classifyBtn();

    //         CommPage.sleep(2);
    //     })

    // it('Should Classify all Unclassified Entry', async function () {

    //     CommPage.sleep(3);

    //     LoginPage.GetURL();
    //     CommPage.sleep(15);

    //     LoginPage.gozo_login();
    //     CommPage.sleep(8);
    //     HomePage.Timesheet();
    //     CommPage.sleep(5);
    //     let enableClassify = await TimesheetPage03.classifyBtnEnable();
    //     if (enableClassify) {
    //         TimesheetPage03.classifyBtn();
    //         TimesheetPage.searchByGCM.sendKeys('W');
    //         TimesheetPage.firstOption.click();
    //         TimesheetPage.selectAllCheckboxes.click();
    //         CommPage.sleep(2);


    //         // TimesheetPage01.thirdCheckbox.click();
    //         // TimesheetPage01.fourthCheckbox.click();
    //         // TimesheetPage01.fifthCheckbox.click();
    //         TimesheetPage.classifyDone.click();
    //         CommPage.sleep(1);
    //         TimesheetPage.confirmRecords.click().then(function () {
    //             console.log('Two Unclassified Records Classified Sucessfully')
    //             expect('Unclassified Records Classified Sucessfully').toEqual('Unclassified Records Classified Sucessfully');
    //         })

    //     } else {
    //         console.log("There is no Data to Classify");
    //     }
    //     // TimesheetPage03.classifyBtn();

    //     CommPage.sleep(2);
    // })

    it('Should Classify all Unclassified Entry', async function () {

        CommPage.sleep(3);

        LoginPage.GetURL();
        CommPage.sleep(15);

        LoginPage.gozo_login();
        CommPage.sleep(8);
        HomePage.Timesheet();
        CommPage.sleep(5);
        let enableClassify = await TimesheetPage03.classifyBtnEnable();
        if (enableClassify) {
            TimesheetPage03.classifyBtn();
            TimesheetPage.searchByGCM.sendKeys('W');
            TimesheetPage.firstOption.click();
            TimesheetPage03.filterInput.clear().sendKeys("1");
            CommPage.sleep(2);

            TimesheetPage.selectAllCheckboxes.click();
            CommPage.sleep(2);


            // TimesheetPage01.thirdCheckbox.click();
            // TimesheetPage01.fourthCheckbox.click();
            // TimesheetPage01.fifthCheckbox.click();
            TimesheetPage.classifyDone.click();
            CommPage.sleep(1);
            TimesheetPage.confirmRecords.click().then(function () {
                console.log('Two Unclassified Records Classified Sucessfully')
                expect('Unclassified Records Classified Sucessfully').toEqual('Unclassified Records Classified Sucessfully');
            })

        } else {
            console.log("There is no Data to Classify");
        }
        // TimesheetPage03.classifyBtn();

        CommPage.sleep(2);
    })


})