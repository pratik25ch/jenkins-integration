var CommPage = require("../../lib/CommLib/comm_lib")
var VerifyPage = require("../../lib/actionLib/verify")
var LoginPage = require("../../pages/login/login_po")
var LoginPage01 = require("../../pages/login/login01_invalid_po")

describe('LOGIN MODULE', function () {
    it('Should Login--570', async function () {
        LoginPage.GetURL();
        LoginPage01.login();
        expect(VerifyPage.invalidLigin_msg.isPresent()).toBe(true);
        CommPage.sleep(3);
    })
})