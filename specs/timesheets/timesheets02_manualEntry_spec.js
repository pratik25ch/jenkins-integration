var CommPage = require("../../lib/CommLib/comm_lib")
var VerifyPage = require("../../lib/actionLib/verify")
var HelperPage = require("../../utils/helperUtils/helper")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var TimesheetPage01 = require("../../pages/timesheets/timesheets01_po")
var TimesheetPage02 = require("../../pages/timesheets/timesheets02_manualEntry_po");
var BillingPage = require("../../pages/billing/billing_po")
var ReportingPage = require("../../pages/reporting/reporting_po")
var AnalyticsPage01 = require("../../pages/analytics/analytics01_matterAnalytics_po")
var AnalyticsPage02 = require("../../pages/analytics/analytics02_financialAnalytics_po")

let R = Math.random().toString(36).substring(7);
/**
 * 1- Should Enter the Manual Entry Through Timer.
 * 2- Should Add manual Entry.
 * 3- Should Check The Entry through Manual Entry Filter.
 * 4- Remove the Stopwatch Timer.(Start,refresh,start,stop,Remove Timer,Again timesheet Added)
 */
beforeAll(function () {
    LoginPage.GetURL();
    LoginPage.login()
    CommPage.sleep(3);
    HomePage.Timesheet();
    CommPage.sleep(2);
})
describe('TIMESHEET MODULE --Manual Entry', function () {
    it('Should Check the Manual Entry Description--673,605', async function () {
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('Timesheet Description Check');
        TimesheetPage02.manualEntryDescription();
        TimesheetPage01.validateBtn.click().then(function () {
            console.log('Timesheet Entry Sucessfully Added')
        })
        expect(VerifyPage.createManualEntry_msg.isPresent()).toBe(true);
        CommPage.sleep(2);
    })
    it('Should Verify the  Description', async function () {
        CommPage.refresh();
        TimesheetPage01.filter.click();
        CommPage.sleep(2);
        TimesheetPage01.byType.click();
        CommPage.sleep(0.5);
        TimesheetPage01.manualEntry.click().then(function () {
                console.log('filter-Manual Timesheet Entry Sucessfully Added')
        })
        expect(VerifyPage.filter_ManualEntry_verify.isPresent()).toBe(true);
        CommPage.sleep(2);
        CommPage.tabBtn();
        CommPage.sleep(2);
        TimesheetPage02.mouseMoveLastTimesheet();
        CommPage.sleep(5);
    })
})
describe('TIMESHEET MODULE --StopWatch Timer Manual Entry', function () {
    it('Should Enter the Manual Entry Through Timer', async function () {
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage02.gcmName();
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('StopWatch Timer');
        CommPage.sleep(2);
        TimesheetPage02.stopwatchTimer.click();
        CommPage.sleep(3);
        TimesheetPage02.startTimer.click();
        CommPage.sleep(20);
        TimesheetPage02.stopTimer.click();
        CommPage.sleep(2);
        TimesheetPage02.manualEntryType.click();
        TimesheetPage02.firstDropdownEntry.click();
        TimesheetPage01.validateBtn.click();

        var S1 = VerifyPage.createManualEntry_msg.getText();
        HelperPage.waitForPresent(S1);
        expect(VerifyPage.createManualEntry_msg.isPresent()).toBe(true);
    })
    it('Should Add Manual entry', async function () {
        CommPage.refresh();
        CommPage.sleep(2);
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('New Timesheet');
        TimesheetPage01.validateBtn.click().then(function () {
            console.log('Timesheet Entry Sucessfully Added')
        })
        expect(VerifyPage.createManualEntry_msg.isPresent()).toBe(true);
        CommPage.sleep(2);
    })
    it('Should Check The Entry through Manual Entry Filter', async function () {
        CommPage.sleep(5)
        CommPage.refresh();
        TimesheetPage01.filter.click();
        CommPage.sleep(2);
        TimesheetPage01.byType.click();
        CommPage.sleep(1);
        TimesheetPage01.manualEntry.click();
        expect(VerifyPage.filter_ManualEntry_verify.isPresent()).toBe(true);
        CommPage.tabBtn();
        CommPage.sleep(3);
        CommPage.sleep(2);
    })
    it('Should Remove the stpwatch Timer', async function () {
        CommPage.refresh();
        CommPage.sleep(2);
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('New Timesheet');
        TimesheetPage02.stopwatchTimer.click();
        CommPage.sleep(3);
        TimesheetPage02.startTimer.click();
        CommPage.sleep(5);
        TimesheetPage02.refreshTimer.click();
        TimesheetPage02.startTimer.click();
        CommPage.sleep(10);
        TimesheetPage02.stopTimer.click();
        CommPage.sleep(2);
        TimesheetPage02.manualEntryType.click();
        TimesheetPage02.firstDropdownEntry.click();
        CommPage.sleep(2);
        TimesheetPage02.removeTimer.click();
        CommPage.sleep(3);
        TimesheetPage02.startTimer.click();
        CommPage.sleep(5);
        TimesheetPage02.stopTimer.click();
        CommPage.sleep(2);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log('Timesheet Entry Sucessfully Added')
        })
        expect(VerifyPage.createManualEntry_msg.isPresent()).toBe(true);
        CommPage.sleep(2);
    })


})