var CommPage = require("../../lib/CommLib/comm_lib")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var TimesheetPage01 = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")
var ReportingPage = require("../../pages/reporting/reporting_po")
var AnalyticsPage01 = require("../../pages/analytics/analytics01_matterAnalytics_po")
var AnalyticsPage02 = require("../../pages/analytics/analytics02_financialAnalytics_po")

let R = Math.random().toString(36).substring(7);

beforeAll(function () {
    LoginPage.GetURL();
    LoginPage.login()
    CommPage.sleep(3);
    HomePage.Timesheet();
    CommPage.sleep(2);
})

describe('TIMESHEET MODULE -- Override Manual Entry', function () {
    it('Should Enter 1st Timesheet', async function () {
        
        //TimesheetPage01.PreviousDate.click();
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T1');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("07 00");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('07 05');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("1st Timesheet Created sucessfully")
        })
        expect('1st Timesheet Created sucessfully').toEqual('1st Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })

    it('Should Enter 2nd Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.secondEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T2');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("06 55");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('07 10');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("2nd Timesheet Created sucessfully")
        })
        expect('2nd Timesheet Created sucessfully').toEqual('2nd Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })

    it('Should Enter 3rd Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.thirdEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T3');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("06 50");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('07 15');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("3rd Timesheet Created sucessfully")
        })
        expect('3rd Timesheet Created sucessfully').toEqual('3rd Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })
    it('Should Enter 4th Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.fourthEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T4');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("06 45");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('07 20');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("4th Timesheet Created sucessfully")
        })
        expect('4th Timesheet Created sucessfully').toEqual('4th Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })
    it('Should Enter 5th Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.fifthEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T5');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("06 40");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('07 25');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("5th Timesheet Created sucessfully")
        })
        CommPage.sleep(2);
        HomePage.Matter();
        HomePage.Timesheet();
        // CommPage.sleep(10);
        // CommPage.refresh();
        CommPage.sleep(5);
        // TimesheetPage01.timesheetView();
        expect('5th Timesheet Created sucessfully').toEqual('5th Timesheet Created sucessfully');
    })
})
describe('TIMESHEET MODULE -- Override Manual Entry', function () {
    it('Should Enter 1st Timesheet', async function () {

        //TimesheetPage01.PreviousDate.click();
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T6');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("06 35");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('07 30');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("1st Timesheet Created sucessfully")
        })
        expect('1st Timesheet Created sucessfully').toEqual('1st Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })

    it('Should Enter 2nd Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.secondEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T7');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("06 30");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('07 35');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("2nd Timesheet Created sucessfully")
        })
        expect('2nd Timesheet Created sucessfully').toEqual('2nd Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })

    it('Should Enter 3rd Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.thirdEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T8');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("06 25");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('07 40');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("3rd Timesheet Created sucessfully")
        })
        expect('3rd Timesheet Created sucessfully').toEqual('3rd Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })
    it('Should Enter 4th Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.fourthEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T9');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("06 20");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('07 45');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("4th Timesheet Created sucessfully")
        })
        expect('4th Timesheet Created sucessfully').toEqual('4th Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })
    it('Should Enter 5th Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.fifthEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T10');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("06 15");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('07 50');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("5th Timesheet Created sucessfully")
        })
        CommPage.sleep(2);
        HomePage.Matter();
        HomePage.Timesheet();
        // CommPage.sleep(10);
        // CommPage.refresh();
        CommPage.sleep(5);
        // TimesheetPage01.timesheetView();
        expect('5th Timesheet Created sucessfully').toEqual('5th Timesheet Created sucessfully');
    })
})
describe('TIMESHEET MODULE -- Override Manual Entry', function () {
    it('Should Enter 1st Timesheet', async function () {
        //TimesheetPage01.PreviousDate.click();
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T11');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("06 10");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('07 55');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("1st Timesheet Created sucessfully")
        })
        expect('1st Timesheet Created sucessfully').toEqual('1st Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })

    it('Should Enter 2nd Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.secondEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T12');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("06 05");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('08 00');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("2nd Timesheet Created sucessfully")
        })
        expect('2nd Timesheet Created sucessfully').toEqual('2nd Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })

    it('Should Enter 3rd Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.thirdEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T13');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("06 00");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('08 05');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("3rd Timesheet Created sucessfully")
        })
        expect('3rd Timesheet Created sucessfully').toEqual('3rd Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })
    it('Should Enter 4th Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.fourthEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T14');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("05 55");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('08 10');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("4th Timesheet Created sucessfully")
        })
        expect('4th Timesheet Created sucessfully').toEqual('4th Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })
    it('Should Enter 5th Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.fifthEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T15');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("05 50");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('08 15');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("5th Timesheet Created sucessfully")
        })
        CommPage.sleep(2);
        HomePage.Matter();
        HomePage.Timesheet();
        // CommPage.sleep(10);
        // CommPage.refresh();
        CommPage.sleep(5);
        // TimesheetPage01.timesheetView();
        expect('5th Timesheet Created sucessfully').toEqual('5th Timesheet Created sucessfully');
    })
})
describe('TIMESHEET MODULE -- Override Manual Entry', function () {
    it('Should Enter 1st Timesheet', async function () {

        //TimesheetPage01.PreviousDate.click();
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T16');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("05 45");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('08 20');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("1st Timesheet Created sucessfully")
        })
        expect('1st Timesheet Created sucessfully').toEqual('1st Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })

    it('Should Enter 2nd Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.secondEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T17');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("05 40");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('08 30');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("2nd Timesheet Created sucessfully")
        })
        expect('2nd Timesheet Created sucessfully').toEqual('2nd Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })

    it('Should Enter 3rd Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.thirdEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T18');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("05 35");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('08 35');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("3rd Timesheet Created sucessfully")
        })
        expect('3rd Timesheet Created sucessfully').toEqual('3rd Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })
    it('Should Enter 4th Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.fourthEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T19');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("05 30");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('08 40');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("4th Timesheet Created sucessfully")
        })
        expect('4th Timesheet Created sucessfully').toEqual('4th Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })
    it('Should Enter 5th Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.fifthEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T20');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("05 25");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('08 45');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("5th Timesheet Created sucessfully")
        })
        CommPage.sleep(2);
        HomePage.Matter();
        HomePage.Timesheet();
        // CommPage.sleep(10);
        // CommPage.refresh();
        CommPage.sleep(5);
        // TimesheetPage01.timesheetView();
        expect('5th Timesheet Created sucessfully').toEqual('5th Timesheet Created sucessfully');
    })
})
describe('TIMESHEET MODULE -- Override Manual Entry', function () {
    it('Should Enter 1st Timesheet', async function () {

        //TimesheetPage01.PreviousDate.click();
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T21');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("05 20");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('08 50');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("1st Timesheet Created sucessfully")
        })
        expect('1st Timesheet Created sucessfully').toEqual('1st Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })

    it('Should Enter 2nd Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.secondEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T22');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("05 15");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('08 55');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("2nd Timesheet Created sucessfully")
        })
        expect('2nd Timesheet Created sucessfully').toEqual('2nd Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })

    it('Should Enter 3rd Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.thirdEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T23');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("05 10");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('09 00');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("3rd Timesheet Created sucessfully")
        })
        expect('3rd Timesheet Created sucessfully').toEqual('3rd Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })
    it('Should Enter 4th Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.fourthEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T24');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("05 05");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('09 05');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("4th Timesheet Created sucessfully")
        })
        expect('4th Timesheet Created sucessfully').toEqual('4th Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })
    it('Should Enter 5th Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.fifthEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T25');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("05 00");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('09 10');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("5th Timesheet Created sucessfully")
        })
        CommPage.sleep(2);
        HomePage.Matter();
        HomePage.Timesheet();
        // CommPage.sleep(10);
        // CommPage.refresh();
        CommPage.sleep(5);
        // TimesheetPage01.timesheetView();
        expect('5th Timesheet Created sucessfully').toEqual('5th Timesheet Created sucessfully');
    })
})
describe('TIMESHEET MODULE -- Override Manual Entry', function () {
    it('Should Enter 1st Timesheet', async function () {

        //TimesheetPage01.PreviousDate.click();
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T26');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("04 55");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('09 15');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("1st Timesheet Created sucessfully")
        })
        expect('1st Timesheet Created sucessfully').toEqual('1st Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })

    it('Should Enter 2nd Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.secondEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T27');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("04 50");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('09 20');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("2nd Timesheet Created sucessfully")
        })
        expect('2nd Timesheet Created sucessfully').toEqual('2nd Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })

    it('Should Enter 3rd Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.thirdEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T28');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("04 45");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('09 25');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("3rd Timesheet Created sucessfully")
        })
        expect('3rd Timesheet Created sucessfully').toEqual('3rd Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })
    it('Should Enter 4th Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.fourthEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T29');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("04 40");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('09 30');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("4th Timesheet Created sucessfully")
        })
        expect('4th Timesheet Created sucessfully').toEqual('4th Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })
    it('Should Enter 5th Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.fifthEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T30');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("04 35");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('09 35');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("5th Timesheet Created sucessfully")
        })
        CommPage.sleep(2);
        HomePage.Matter();
        HomePage.Timesheet();
        // CommPage.sleep(10);
        // CommPage.refresh();
        CommPage.sleep(5);
        // TimesheetPage01.timesheetView();
        expect('5th Timesheet Created sucessfully').toEqual('5th Timesheet Created sucessfully');
    })
})
describe('TIMESHEET MODULE -- Override Manual Entry', function () {
    it('Should Enter 1st Timesheet', async function () {

        //TimesheetPage01.PreviousDate.click();
        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.firstEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T31');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("04 30");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('09 40');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("1st Timesheet Created sucessfully")
        })
        expect('1st Timesheet Created sucessfully').toEqual('1st Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })

    it('Should Enter 2nd Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.secondEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T32');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("04 25");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('09 45');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("2nd Timesheet Created sucessfully")
        })
        expect('2nd Timesheet Created sucessfully').toEqual('2nd Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })

    it('Should Enter 3rd Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.thirdEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T33');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("04 20");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('09 50');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("3rd Timesheet Created sucessfully")
        })
        expect('3rd Timesheet Created sucessfully').toEqual('3rd Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })
    it('Should Enter 4th Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.fourthEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T34');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("04 15");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('09 55');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("4th Timesheet Created sucessfully")
        })
        expect('4th Timesheet Created sucessfully').toEqual('4th Timesheet Created sucessfully');
        CommPage.sleep(5);
        CommPage.refresh();
        CommPage.sleep(5);
    })
    it('Should Enter 5th Timesheet', async function () {

        TimesheetPage01.manualEntryBtn();
        CommPage.sleep(3);
        TimesheetPage01.selectMatter.click();
        TimesheetPage01.fifthEntry.click();
        TimesheetPage01.manualEntryName.sendKeys('T35');
        CommPage.sleep(2);
        TimesheetPage01.startTime.sendKeys("04 10");
        TimesheetPage01.startTime.sendKeys("AM")
        CommPage.sleep(1);
        TimesheetPage01.endTime.sendKeys('10 00');
        TimesheetPage01.endTime.sendKeys("AM");
        CommPage.sleep(1);
        TimesheetPage01.validateBtn.click().then(function () {
            console.log("5th Timesheet Created sucessfully")
        })
        CommPage.sleep(2);
        HomePage.Matter();
        HomePage.Timesheet();
        // CommPage.sleep(10);
        // CommPage.refresh();
        CommPage.sleep(5);
        // TimesheetPage01.timesheetView();
        expect('5th Timesheet Created sucessfully').toEqual('5th Timesheet Created sucessfully');
    })
})