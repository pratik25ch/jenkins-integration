var CommPage = require("../../lib/CommLib/comm_lib");
var VerifyPage = require("../../lib/actionLib/verify");
var HelperPage = require("../../utils/helperUtils/helper");
var LoginPage = require("../../pages/login/login_po");
var HomePage = require("../../lib/homepageLib/homepage_lib");
var TimesheetPage01 = require("../../pages/timesheets/timesheets01_po");
var TimesheetPage04 = require("../../pages/timesheets/timesheets04_delete_po")
var BillingPage = require("../../pages/billing/billing_po");
var ReportingPage = require("../../pages/reporting/reporting_po");
var AnalyticsPage01 = require("../../pages/analytics/analytics01_matterAnalytics_po");
var AnalyticsPage02 = require("../../pages/analytics/analytics02_financialAnalytics_po");

let R = Math.random().toString(36).substring(7);

beforeAll(function () {
    LoginPage.GetURL();
    LoginPage.login()
    CommPage.sleep(3);
    HomePage.Timesheet();
    CommPage.sleep(5);
})
describe('TIMESHEET MODULE --Delete Timesheet', function () {
    it('Should Delete Timesheet Btn ', async function () {
        HelperPage.waitForDisplay(VerifyPage.timesheetPage_verify);          
        TimesheetPage04.timesheetTooltip();
        CommPage.sleep(2);
        TimesheetPage04.deleteTimesheetBtn.click();
        expect(await TimesheetPage04.cancelDeleteTimesheet.isPresent()).toBe(true);
        CommPage.sleep(3);
        console.log("Timesheet delete btn is working fine");
    })
    it('Should Cancel the delete timesheet', function () {
        TimesheetPage04.cancelDeleteTimesheet.click().then(function () {
            console.log('Cancel Timesheet Btn is working');
        })
        CommPage.sleep(2);
    })
    xit('Should enabled the Timesheet Checkbox', async function () {
        TimesheetPage04.timesheetTooltip();
        CommPage.sleep(3)
        TimesheetPage04.deleteTimesheetBtn.click();
        expect(await TimesheetPage04.firstCheckbox.isSelected()).toBe(false);
        TimesheetPage04.firstCheckbox.click();
        TimesheetPage04.secondCheckbox.click();
        CommPage.sleep(2)
        // expect(await TimesheetPage04.firstCheckboxSelected.isSelected()).toBe(true);
        CommPage.sleep(5);

    });
    xit('Should enabled the Timesheet Checkbox', async function () {
        TimesheetPage04.timesheetTooltip();
        CommPage.sleep(3)
        TimesheetPage04.deleteTimesheetBtn.click();
        expect(await TimesheetPage04.firstCheckbox.isSelected()).toBe(false);
        TimesheetPage04.firstCheckbox.click();
        TimesheetPage04.secondCheckbox.click();
        CommPage.sleep(2)
        // expect(await TimesheetPage04.firstCheckboxSelected.isSelected()).toBe(true);
        CommPage.sleep(5);

    });
    















    
})