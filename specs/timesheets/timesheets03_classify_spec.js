var CommPage = require("../../lib/CommLib/comm_lib")
var VerifyPage = require("../../lib/actionLib/verify")
var HelperPage = require("../../utils/helperUtils/helper")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var TimesheetPage01 = require("../../pages/timesheets/timesheets01_po")
var TimesheetPage02 = require("../../pages/timesheets/timesheets02_manualEntry_po");
var TimesheetPage03 = require("../../pages/timesheets/timesheets03_classify_po")

let R = Math.random().toString(36).substring(7);

beforeAll(function () {
    LoginPage.GetURL();
    LoginPage.login()
    CommPage.sleep(3);
    HomePage.Timesheet();
    CommPage.sleep(2);
})
describe('TIMESHEET MODULE --Classify Filter Time', function () {
    it('Should Check classify Time is Present or not', async function () {
        HomePage.Timesheet();
        let C1 = await TimesheetPage03.classifyBtnEnable();
        if (C1) {
            TimesheetPage03.classifyBtn();
        } else {
            console.log("There is no Data to Classify");
            CommPage.close();
        }
    })
    it('Should Classify the Unclassified Entry', async function () {

        TimesheetPage01.searchByGCM.sendKeys('A');
        TimesheetPage01.firstOption.click();
        TimesheetPage01.firstCheckbox.click();
        // TimesheetPage01.secondCheckbox.click();
        // TimesheetPage01.thirdCheckbox.click();
        // TimesheetPage01.fourthCheckbox.click();
        // TimesheetPage01.fifthCheckbox.click();
        TimesheetPage01.classifyDone.click();
        CommPage.sleep(1);
        TimesheetPage01.confirmRecords.click().then(function () {
            console.log('Unclassified Records Classified Sucessfully')
            expect('Unclassified Records Classified Sucessfully').toEqual('Unclassified Records Classified Sucessfully');
        })
        CommPage.sleep(2);
    })
    it('Should Check Manual Timesheet Entry', async function () {
        CommPage.back();
        CommPage.sleep(2);
        console.log('Timesheet')
        expect('Timesheet Data').toEqual('Timesheet Data');
    })
    it('Should View All Timesheet Entry', async function () {
        TimesheetPage01.viewAll.click();
        CommPage.sleep(0.5);
        TimesheetPage01.firstDropdown.click();
        CommPage.sleep(2);
        TimesheetPage01.firstDropdown.click();
        console.log('Dropdown View all Viewed Sucessfully');
        expect('It will Show All Timesheet').toEqual('It will Show All Timesheet');
        CommPage.escBtn(); // Remove Previous Dropdown
        CommPage.refresh();
        CommPage.sleep(0.5);
    })


























})