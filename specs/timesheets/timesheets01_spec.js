var CommPage = require("../../lib/CommLib/comm_lib");
var VerifyPage = require("../../lib/actionLib/verify");
var HelperPage = require("../../utils/helperUtils/helper");
var LoginPage = require("../../pages/login/login_po");
var HomePage = require("../../lib/homepageLib/homepage_lib");
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var TimesheetPage01 = require("../../pages/timesheets/timesheets01_po");
var BillingPage = require("../../pages/billing/billing_po");
var ReportingPage = require("../../pages/reporting/reporting_po");
var AnalyticsPage01 = require("../../pages/analytics/analytics01_matterAnalytics_po");
var AnalyticsPage02 = require("../../pages/analytics/analytics02_financialAnalytics_po");

let R = Math.random().toString(36).substring(7);
/**
 * 1- Should Open Timesheet Page.
 * 2- Should Search another User Timesheet.
 * 3- Should Add Manual entry.
 * 4- Should Display Duplicate Manual entry.(CONFLICT)
 * 
 * 1- Should Filter by Manual Entry.
 * 2- Should Filter by Application entry.
 * 3- Should Filter by Time.(Pending)
 * 
 * 1- Should Add a manual Entry of Previous Date.
 * 2- Go to Previous Date & filter Preavious Date Manual entry.
 * 3- Should Check Manual Entry should Overiride the Unclassified Entry.
 * 4- Should Filter by Time - today Entry Filter.
 * 
 * 1- Should Compare The Timesheet Value with Analytics Timesheet Value.
 */
beforeAll(function () {
        LoginPage.GetURL();
        LoginPage.login()
        CommPage.sleep(3);
        HomePage.Timesheet();
        CommPage.sleep(5);
})
describe('TIMESHEET MODULE --Admin & Other Manual Entry,Manual entry Conflicts', function () {
        it('Should Search Timesheet', async function () {
                await TimesheetPage01.timesheetView().then(function () {
                        console.log('Timesheet Entry')
                        expect(VerifyPage.timesheetPage_verify.isPresent()).toBe(true);
                })
        })
        it('Should Search another User Timesheet', function () {
                TimesheetPage01.userSelect.click();
                TimesheetPage01.userInput.sendKeys('qa1@flowace.in');
                TimesheetPage01.firstOption.click();
                CommPage.sleep(2);
                TimesheetPage01.timesheetCalender.click();
                TimesheetPage01.todaysDate.click().then(function () {
                        console.log('Timesheet Entry')
                })
                CommPage.sleep(5);
                expect(VerifyPage.changeAssociate_verify.isPresent()).toBe(true);
        })
        it('Should Add Associate Manual Entry', function () {
                TimesheetPage01.manualEntryBtn();
                CommPage.sleep(3);
                TimesheetPage01.selectMatter.click();
                TimesheetPage01.firstEntry.click();
                TimesheetPage01.manualEntryName.sendKeys('Associate Timesheet');
                TimesheetPage01.validateBtn.click().then(function () {
                        console.log('Timesheet Entry Sucessfully Added')
                })
                expect(VerifyPage.createManualEntry_msg.isPresent()).toBe(true);
                CommPage.sleep(3);
        })
        it('Should check the Associate Timesheet', function () {
                TimesheetPage01.backwordDate.click();
                CommPage.sleep(3);
                TimesheetPage01.forwordDate.click();
                CommPage.sleep(2)
                TimesheetPage01.timesheetView().then(function () {
                        console.log('Associte Timesheet Entry')
                        expect(VerifyPage.timesheetPage_verify.isPresent()).toBe(true);
                })
        })
        it('Should Add Manual entry of Admin', async function () {
                CommPage.refresh();
                CommPage.sleep(2);
                TimesheetPage01.manualEntryBtn();
                CommPage.sleep(3);
                TimesheetPage01.selectMatter.click();
                TimesheetPage01.firstEntry.click();
                TimesheetPage01.manualEntryName.sendKeys('New Timesheet');
                TimesheetPage01.validateBtn.click().then(function () {
                        console.log('Timesheet Entry Sucessfully Added')
                })
                expect(VerifyPage.createManualEntry_msg.isPresent()).toBe(true);
                CommPage.sleep(2);
        })
        it('Should Display Duplicate Manual entry', async function () {
                CommPage.refresh();
                CommPage.sleep(3);
                TimesheetPage01.manualEntryBtn();
                CommPage.sleep(3);
                TimesheetPage01.selectMatter.click();
                CommPage.sleep(0.5);
                TimesheetPage01.firstEntry.click();
                TimesheetPage01.manualEntryName.sendKeys('New Timesheet');
                TimesheetPage01.validateBtn.click().then(function () {
                        console.log('Duplicate Timesheet Entry')
                })
                expect(VerifyPage.duplicateManualEntry_msg.isPresent()).toBe(true);
                CommPage.sleep(2);
        })
})
describe('TIMESHEET MODULE:-- Filter Module', function () {
        it('Should Filter by Manual entry', async function () {
                TimesheetPage01.cancelBtn.click();
                CommPage.refresh();
                TimesheetPage01.filter.click();
                CommPage.sleep(2);
                TimesheetPage01.byType.click();
                CommPage.sleep(0.5);
                TimesheetPage01.manualEntry.click().then(function () {
                        console.log('filter-Manual Timesheet Entry Sucessfully Added')
                })
                expect(VerifyPage.filter_ManualEntry_verify.isPresent()).toBe(true);
                CommPage.sleep(2);
        })
        it('Should Filter by Application entry', async function () {
                CommPage.tabBtn();
                CommPage.sleep(2);
                TimesheetPage01.removeFilterIcon.click();
                CommPage.sleep(2);
                TimesheetPage01.filter.click();
                CommPage.sleep(1);
                TimesheetPage01.byApplication.click();
                CommPage.sleep(0.5);
                TimesheetPage01.allAppFilter();
                console.log('Timesheet by Application')
                expect('Timesheet by Application').toEqual('Timesheet by Application');
                CommPage.sleep(4);
        })
        it('Should Check the Admin user', async function () {
                TimesheetPage01.applicationList();
                CommPage.sleep(2);
                CommPage.tabBtn();
                CommPage.sleep(2);
                TimesheetPage01.backwordDate.click();
                CommPage.sleep(3);
                TimesheetPage01.forwordDate.click();
                CommPage.sleep(2)
                expect('Refresh').toEqual('Refresh');
        })
        it('Should Filter by Plugin Idle', async function () {
                CommPage.sleep(2);
                TimesheetPage01.filter.click();
                CommPage.sleep(2);
                TimesheetPage01.byType.click();
                CommPage.sleep(0.5);
                TimesheetPage01.pluginIdleFilter()
                expect('Plugin Idle').toEqual('Plugin Idle');
                CommPage.sleep(2);
                TimesheetPage01.removeIcon();
        })
        it('Should Check the Plugin Classified & Unclassified Entry', async function () {
                TimesheetPage01.filter.click();
                CommPage.sleep(2);
                TimesheetPage01.byType.click();
                CommPage.sleep(0.5);
                TimesheetPage01.pluginFilter();
                expect('Plugin Entries').toEqual('Plugin Entries');
                CommPage.tabBtn();
                CommPage.sleep(3);
        })
})
describe('TIMESHEET MODULE:---Previous Day Entry', function () {
        it('should Create a matter for pravious date timesheet test', async function () {
                HomePage.Matter();
                HelperPage.waitForDisplay(VerifyPage.firstGroup_verify);
                await GCMPage01.firstClient.click();
                CommPage.sleep(2);
                GCMPage01.newMatterBtn.click();
                CommPage.sleep(5);
                GCMPage01.matterName.sendKeys("Previous timesheet", R);
                CommPage.sleep(3);
                GCMPage01.selectMember.sendKeys('Debasish Patra');
                CommPage.sleep(2)
                GCMPage01.firstOption.click();
                CommPage.sleep(2);
                await GCMPage01.saveBtn.click().then(function () {
                        console.log("Matter Created sucessfully");
                })
        })
        it('Should Add a manual Entry of Previous Date--560', async function () {
                HomePage.Timesheet();
                CommPage.sleep(3)
                TimesheetPage01.PreviousDate.click();
                CommPage.sleep(0.5);
                TimesheetPage01.manualEntryBtn();
                CommPage.sleep(3);
                TimesheetPage01.selectMatter.click();
                TimesheetPage01.firstEntry.click();
                TimesheetPage01.manualEntryName.sendKeys('Previous Date Manual Timesheet');
                TimesheetPage01.validateBtn.click();
                console.log('Timesheet Entry Sucessfully Added')
                expect('Timesheet Entry Sucessfully Added').toEqual('Timesheet Entry Sucessfully Added');
        })
        it('Should Filter Preavious Date Manual entry', async function () {
                CommPage.sleep(2);
                TimesheetPage01.filter.click();
                CommPage.sleep(2);
                TimesheetPage01.byType.click();
                CommPage.sleep(0.5);
                TimesheetPage01.manualEntry.click();
                CommPage.sleep(2);
                CommPage.tabBtn();
                TimesheetPage01.timesheetView();
                console.log('Manual Timesheet Entry Sucessfully Added')
                expect('Manual Timesheet Entry Sucessfully Added').toEqual('Manual Timesheet Entry Sucessfully Added');
        })
        it('Should Filter Previous Date PluginIdle Entries--651 ', async function () {
                TimesheetPage01.removeFilterIcon.click();
                TimesheetPage01.filter.click();
                CommPage.sleep(2);
                TimesheetPage01.byType.click();
                CommPage.sleep(2);
                TimesheetPage01.pluginFilter();
                console.log('PlugIn Entry Filter working Successfully')
                expect('PlugIn Entry Filter working Successfully').toEqual('PlugIn Entry Filter working Successfully');
        })
        xit('Should Filter by Time', async function () {
                TimesheetPage01.backwordDate.click();
                CommPage.sleep(3);
                TimesheetPage01.forwordDate.click();
                CommPage.sleep(2)
                TimesheetPage01.filter.click();
                CommPage.sleep(1);
                TimesheetPage01.byTime.click();
                CommPage.sleep(2);
                TimesheetPage01.startTime.sendKeys("08 00 A");
                CommPage.sleep(4);
                CommPage.tabBtn();
                TimesheetPage01.endTime.sendKeys("11 00 A");
                CommPage.sleep(2);
        })
})
describe('TIMESHEET MODULE:--- Timesheet-Analytics Comparision', function () {
        it('Should Compare The Timesheet with Analytics', async function () {
                CommPage.refresh();
                CommPage.sleep(2);
                let t1 = await TimesheetPage01.timesheetUnclassifiedTime();
                console.log('Timesheet Unclassified Hours ', t1);
                let t2 = await TimesheetPage01.timesheetClassifiedTime();
                console.log('Timesheet Classified Hours ', t2);
                HomePage.Analytics();
                CommPage.sleep(5);
                AnalyticsPage01.selectAssociate.click();
                AnalyticsPage01.associateInput.sendKeys("debasish@flowace.in");
                AnalyticsPage01.firstOption.click();
                //click on calender Icon
                AnalyticsPage01.startDate.click();
                AnalyticsPage01.FirstCalender();
                CommPage.sleep(0.5);
                AnalyticsPage01.todaysDate.click();
                CommPage.sleep(2);
                AnalyticsPage01.analyzeReport.click();
                CommPage.sleep(2);
                let v1 = await AnalyticsPage01.AnalyticsUnclassifiedTime();
                console.log('Analytics Unclassified Hours ', v1);
                let v2 = await AnalyticsPage01.AnalyticsClassifiedTime();
                console.log('Analytics Classified Hours ', v2);
                expect(t1).toEqual(v1);
                expect(t2).toEqual(v2);
        })
})