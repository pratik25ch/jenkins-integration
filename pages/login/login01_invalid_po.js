var LoginData = require("../../utils/jsonUtils/login.json");
var LoginPO01 = function () {

    let email = element(by.name('email'));
    let password = element(by.name('password'));
    let loginbutton = element(by.xpath("//button[@name='login-button']"));
    let logOut = element(by.name('sign-out-button'))

    this.login = function () {
        email.sendKeys(LoginData.Invalid_Login.Email);
        password.sendKeys(LoginData.Invalid_Login.Password);
        loginbutton.click();
        console.log("Login Failed");
    }
}
module.exports = new LoginPO01();