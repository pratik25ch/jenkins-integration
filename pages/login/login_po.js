var LoginData = require("../../utils/jsonUtils/login.json");
var CommPage = require("../../lib/CommLib/comm_lib")
var LoginPO = function () {

        let email = element(by.name('email'));
        let password = element(by.name('password'));
        let loginbutton = element(by.xpath("//button[@name='login-button']"));
        let logOut = element(by.name('sign-out-button'))
        let termsAcceptBtn = element(by.xpath("//span[contains(text(),'ACCEPT')]"));

        this.GetURL = function () {
                // browser.get(LoginData.QA_Login.URL);

                browser.get(LoginData.Demo_Login.URL);
                console.log("URL Opened Successfully");
        };
        //679
        this.login = function () {
                email.clear().sendKeys(LoginData.Admin_Login.Email);
                password.clear().sendKeys(LoginData.Admin_Login.Password);
                loginbutton.click().then(function () {
                        console.log("User Login Successfully")
                })
                let dashboard = element(by.id('dashboard'));
                dashboard.isPresent().then(function (T) {
                        if (T) {
                                console.log("Dashboard Opened")
                        } else {
                                termsAcceptBtn.click().then(function () {
                                        console.log("Terms & Conditions Accepted")
                                })
                        }
                })
        }

        this.gozo_login = function () {
                email.clear().sendKeys(LoginData.gozo_Associate_Login.Email);
                password.clear().sendKeys(LoginData.gozo_Associate_Login.Password);
                loginbutton.click().then(function () {
                        console.log("User Login Successfully")
                })
                let dashboard = element(by.id('dashboard'));
                dashboard.isPresent().then(function (T) {
                        if (T) {
                                console.log("Dashboard Opened")
                        } else {
                                termsAcceptBtn.click().then(function () {
                                        console.log("Terms & Conditions Accepted")
                                })
                        }
                })
        }

        //679
        this.associateLogin = function () {
                email.clear().sendKeys(LoginData.Staging_Associate_Login.Email);
                password.clear().sendKeys(LoginData.Staging_Associate_Login.Password);
                loginbutton.click().then(function () {
                        console.log("Associate Login Successfully")
                })
                let dashboard = element(by.id('dashboard'));
                dashboard.isPresent().then(function (T) {
                        if (T) {
                                console.log("Dashboard Opened")
                        } else {
                                termsAcceptBtn.click().then(function () {
                                        console.log("Terms & Conditions Accepted")
                                })
                        }
                })
        }

        this.AssociateLogin = function () {
                email.sendKeys(LoginData.Associate_Login.Email);
                password.sendKeys(LoginData.Associate_Login.Password);
                loginbutton.click().then(function () {
                        console.log("Associate Login Successfully")
                })
                let dashboard = element(by.id('dashboard'));
                dashboard.isPresent().then(function (T) {
                        if (T) {
                                console.log("Dashboard Opened")
                        } else {
                                termsAcceptBtn.click().then(function () {
                                        console.log("Terms & Conditions Accepted")
                                })
                        }
                })
        }

        this.signOut = function () {
                logOut.click();
        }
}
module.exports = new LoginPO();