var Employee02PO = function () {

    //Assign Matter
    this.mouseMoveFirst = function () {
        browser.actions()
            .mouseMove(element(by.xpath("//tr[1]/td[1]")))
            .perform();
    }
    this.checkBox = element(by.id('row-select-0'));
    this.matIcon = element(by.xpath("//mat-icon[contains(text(),'done')]"));
    this.assignMatter= element(by.xpath("(//button[@role='menuitem'])[1]"));
    //group
    this.groupDropdown = element(by.xpath("(//div[@class='mat-select-value']//span)[1]"));
    this.groupCheckbox = element(by.xpath("(//span[@class='mat-option-text'])[2]"));

    this.escBtn = function () {
        browser.actions()
            .sendKeys(protractor.Key.ESCAPE)
            .perform();
    }
    //client
    this.clientDropdown = element(by.xpath("(//div[@class='mat-select-value']//span)[2]"));
    this.clientCheckbox = element(by.xpath("(//span[@class='mat-option-text'])[2]"));
    //maatter
    this.MatterDopdown = element(by.xpath("(//div[@class='mat-select-value']//span)[3]"));
    this.MatterCheckbox1 = element(by.xpath("(//span[@class='mat-option-text'])[2]"));
    this.MatterCheckbox2 = element(by.xpath("(//span[@class='mat-option-text'])[3]"));
    this.clearBtn = element(by.xpath("//span[contains(text(),'CLEAR')]"));
    this.cancelAssignBtn = element(by.id('cancel-btn'));
    this.assignBtn = element(by.id('validate-btn'));
    //Matter check
    this.firstClient = element(by.id('clients-0'));
    this.firstMatter = element(by.xpath("(//p[@class='matter-name'])[1]"));
    this.secondMatter = element(by.xpath("(//p[@class='matter-name'])[2]"));
    this.associatename = element.all(by.name('activeAssociate'));
}
module.exports = new Employee02PO();