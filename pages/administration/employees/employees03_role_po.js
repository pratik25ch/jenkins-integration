var Employee03PO = function () {

//Allow matter Creation
this.selectPermissions = element(by.id('select-permissions'));
        
this.allowGroupCreation = element(by.id('permission-list-1'));
this.allowClientCreation = element(by.id('permission-list-2'));
this.allowMatterCreation = element(by.id('permission-list-3'));
this.allowManualEntry = element(by.id('permission-list-4'));
//Select Integration
this.selectIntegrations = element(by.id('select-integrations'));
this.wordIntegration = element(by.id('integration-list-1'));
this.excelIntegration = element(by.id('integration-list-2'));
this.pptIntegration = element(by.id('integration-list-3'));
this.outlookIntegration = element(by.id('integration-list-4'));
this.acrobatIntegration = element(by.id('integration-list-5'));

//Hierarchy
this.viewHierarchy = element(by.id('hierarchy'));
this.expandHierarchy = element(by.id('expand-hierarchy'));
this.downloadHierarchy = element(by.id('view-hierarchy'));
this.closeHierarchy = element(by.id('close-hierarchy'));

}
module.exports = new Employee03PO();