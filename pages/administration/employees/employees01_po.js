    var Employee01PO = function () {

        //Create an NEW Employe
        let newEmpBtn = element(by.name('add-employee'));

        this.newEmpBtn = function () {
            newEmpBtn.click();
        }

        
        
        
        
        this.StartDate = element(by.xpath("//input[contains(@placeholder,'Start Date')]"));
        this.StartDateSelect = element(by.xpath("//td[@class='mat-calendar-body-cell ng-star-inserted']"));

        this.EndDate = element(by.xpath("//input[contains(@placeholder,'End Date')]"));
        this.EndDateSelect = element(by.xpath("//td[@class='mat-calendar-body-cell mat-calendar-body-active ng-star-inserted']"));

this.successMessage = element(by.xpath("//span[contains(text(),'User updated Successfully')]"));

        this.syncChargeOutRate = element(by.xpath("//span[contains(text(),'your present and historical chargeout rate')]/../div/div[1]/.."));

        this.historicalChargeOutRate = element(by.xpath("//input[contains(@placeholder,'Historical Charge Out Rate')]"));
        // this.historicalChargeOutRateCheck = element(by.xpath("//span[contains(text(),'Click here to change historical chargeout rate')]/../div/div[1]"));
        this.historicalChargeOutRateCheck = element(by.xpath("//span[contains(text(),'Click here to change historical chargeout rate')]/../div"));
        this.firstname = element(by.name('firstName'));
        this.lastname = element(by.name('lastName'));
        this.email = element(by.name('email'));
        this.yearlysalary = element(by.name('yearlySalary'));
        this.fulldays = element(by.name('fullDays'));
        this.halfdays = element(by.name('halfDays'));
        this.fulldaysHours = element(by.name('fullDaysHours'));
        this.halfdaysHours = element(by.name('halfDaysHours'));
        this.chargeOutRate = element(by.name('chargeOutRate'));
        this.role = element(by.name('role'));
        this.adminBtnRole = element(by.xpath("(//span[@class='mat-option-text'])[3]"));
        this.managerBtnRole = element(by.xpath("(//span[@class='mat-option-text'])[2]"));
        this.employeeBtnRole = element(by.xpath("(//span[@class='mat-option-text'])[1]"));
        this.contactNumber = element(by.name('contactNumber'));
        this.reportsTo = element(by.name('member'));
        this.reportslist = element(by.xpath("(//span[@class='mat-option-text'])[1]"));
        this.savebutton = element(by.xpath("//span[contains(text(),'SAVE')]"));
        this.cancelbutton = element(by.id('cancel-edit-employee'));

        this.firstOption = element(by.xpath("(//span[@class='mat-option-text'])[1]"));
        this.placeType = element(by.id('place-type'));
        this.address = element(by.id('address'));

        //EDIT Employee
        this.searchEmp = element(by.id('search-associates'));
        this.editEmp = element(by.xpath("(//span[@name='edit-employee'])[1]"));

        //Allow matter Creation
        this.selectPermissions = element(by.id('select-permissions'));
        this.allowGroupCreation = element(by.id('permission-list-1'));
        this.allowClientCreation = element(by.id('permission-list-2'));
        this.allowMatterCreation = element(by.id('permission-list-3'));
        this.allowManualEntry = element(by.id('permission-list-4'));

        //emp edit = new emp
//Integrations
        this.selectIntegration = element(by.xpath("//span[text()='Select Integrations']"));
        this.outlookIntegration = element(by.xpath("//span[text()=' outlook ']"));

        //Disable Employee
        this.disable = element(by.name('disable'));
        //Enable Employee
        //Disbled
        this.disabled = element(by.id('disabled'));
        this.enable = element(by.name('enable'));

        //Download
        this.download = element(by.xpath("//button[@id='exp-btn1']"));

        //ascending
        this.empAll = element.all(by.xpath("//span[@class='e-name']"));
        this.empHeader = element(by.id('emp-header'));
        this.onlineStatusHeader = element(by.id('online-status-header'));

        this.onlineStatus = function () {
            element(by.xpath("(//td[@role='gridcell']//i)[1]")).isDisplayed().then(function (isVisible) {
                if (isVisible) {
                    console.log('Employee is Online')
                } else {
                    console.log('Employee is Offline')
                }
            });
        }
    }
    module.exports = new Employee01PO();