var Contacts01PO = function () {

    //New Contact
    this.newContact = element(by.id('add-contact'));
    this.firstname = element(by.name('firstName'));
    this.lastname = element(by.name('lastName'));
    this.company = element(by.name('company'));
    this.primaryNumber = element(by.id('primaryPhone'));
    this.email = element(by.name('email'));
    this.firstInput = element(by.xpath("(//span[@class='mat-option-text'])[1]"));
    this.placeType = element(by.id('place-type'));
    this.address = element(by.id('address'));

    this.firstAddress = element(by.xpath("//div[@xpath='1']//input[1]"));
    this.seconderyType = element(by.xpath("//span[text()='Type']"));
    this.numbertype = element(by.xpath("(//span[@class='mat-option-text'])[1]"));
    this.selectcountry = element(by.id('mat-select-name'));
    this.countrySearch = element(by.xpath("//input[@placeholder='Country']"));
    this.country = element(by.xpath("//span[text()='India (91)']"));
    this.inputnumber = element(by.xpath("//input[@name='number']"));
    this.addnumber = element(by.xpath("//span[@class='mat-button-wrapper'][contains(text(),'Add')]"));
    this.saveBtn = element(by.xpath("//span[contains(text(),'save')]"));
    //Edit Contact
    this.searchContact = element(by.xpath("//input[@id='input-field']"));
    this.editOption = element(by.xpath("(//i[@class='fa fa-ellipsis-v'])[1]"));
    this.editcontact = element(by.xpath("(//button[@role='menuitem'])[1]"));
    //Edit Contact Tag Group
    this.tagsGroup = element(by.xpath("(//mat-panel-title[@class='mat-expansion-panel-header-title']//strong)[1]"));
    this.tagsGroupInput = element(by.name('group'));
    this.tagsClient = element(by.xpath("(//mat-panel-title[@class='mat-expansion-panel-header-title']//strong)[2]"));
    this.tagsClientInput = element(by.name('client'));
    this.tagsMatter = element(by.xpath("(//mat-panel-title[@class='mat-expansion-panel-header-title']//strong)[3]"));
    this.tagsMatterInput = element(by.name('matter'));

}
module.exports = new Contacts01PO();