var Contacts02PO = function () {

    //Edit User Details
    this.firstUser = element(by.xpath("//tr[1]/td[2]/span[1]/a[1]"));
    this.createBadgeIcon = element(by.xpath("//div[@class='left-bottom']//i[1]"));
    this.createBadge = element(by.xpath("//span[text()=' Create New Badge ']"));
    this.badgeName = element(by.id('tag-id'));
    this.badgeBtn = element(by.xpath("//span[contains(text(),'Create Badge')]"));
    this.addBadgedropdown = element(by.xpath("(//div[@class='mat-select-value']//span)[1]"));
    this.badgeInput = element(by.xpath("//input[@placeholder='Search for Badges...']"));

    this.escBtn = function () {
        browser.actions().sendKeys(protractor.Key.ESCAPE).perform();
    }

    this.tagDone = element(by.id('tags-done'));
    //Edit User Profile 
    this.userTitleEdit = element(by.id('user-title-edit'));
    this.userFirstName = element(by.id('first-name'));
    this.userLastName = element(by.id('last-name'));
    this.companyName = element(by.id('company-name'));
    this.userProfileSaveBtn = element(by.id('save-btn'));
    this.userAddress = element(by.css('div.contact-address'));
    //Edit Personl Information
    this.personalInfoEdit = element(by.id('personal-info-edit'));
    this.profession = element(by.id('profession'));
    this.calenderIcon = element(by.id('keyboard_arrow_down'));
    this.dobClick = element(by.xpath("//div[@class='mat-calendar-body-cell-content mat-calendar-body-today']"));
    this.genderIcon = element(by.xpath("(//div[@class='mat-select-arrow-wrapper'])[2]"));
    this.genderMale = element(by.id('male'));
    this.genderFemale = element(by.id('female'));
    //Edit Contact Information
    this.contactInfoEdit = element(by.id('contact-info-edit'));
    this.userEmail = element(by.id('email'));
    this.addEmail = element(by.id('add-email'));
    this.secondEmail = element(by.id('make-primary-email-2'));
    this.deleteFirstEmail = element(by.id('delete-email-icon-1'));
    this.address = element(by.id('address'));
    this.addAddress = element(by.id('add-address'));
    this.primaryAddress = element(by.id('make-primary-address-2'));
    this.deletePhone = element(by.id('delete-phone-icon-2'));
    this.validateBtn = element(by.id('validate-btn'));
    //MATTER
    this.matterBtn = element(by.xpath("(//span[@class='uppercase-text ng-star-inserted'])[1]"));
    this.MatterTag = element(by.xpath("(//span[@class='uppercase-text ng-star-inserted'])[2]"));
    this.ClientTag = element(by.xpath("(//span[@class='uppercase-text ng-star-inserted'])[3]"));
    this.GroupTag = element(by.xpath("(//span[@class='uppercase-text ng-star-inserted'])[4]"));
    //Filter
    this.filterBtn = element(by.id('filter-btn'));
    this.byDate = element(by.id('by-date'));
    this.byBadge = element(by.id('by-badge'));
    this.calender = element(by.id('datepicker-toggle'));
    this.clearFilter = element(by.id('clear-filter'));
    this.previousMonth = element(by.xpath("//button[@class='mat-calendar-previous-button mat-icon-button']"));
    this.nextMonth = element(by.xpath("//button[@class='mat-calendar-next-button mat-icon-button']"));
    this.startDate = element(by.xpath("(//sat-calendar//td[2]//div)[2]"));
    this.todaysdate = element(by.xpath("//div[@class='mat-calendar-body-cell-content mat-calendar-body-today']"));

    this.todaysDateClick = function () {
        browser.actions().
        doubleClick(element(by.css('div.mat-calendar-body-cell-content.mat-calendar-body-today'))).perform();
    }
    this.todayClick = element(by.css('div.mat-calendar-body-cell-content.mat-calendar-body-today'));
}
module.exports = new Contacts02PO();