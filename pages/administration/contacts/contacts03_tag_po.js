var Contacts03PO = function () {
    
    //Tag Contact
    this.firstCheckbox = element(by.xpath("//tr[1]//td[1]//mat-checkbox[1]"));
    this.selectDone = element(by.xpath("(//mat-icon[text()='done'])[2]"));
    this.tagGroup = element(by.xpath("(//div[@class='mat-select-arrow'])[2]"));
    this.tagClient = element(by.xpath("(//div[@class='mat-select-arrow'])[3]"));
    this.tagMatter = element(by.xpath("(//div[@class='mat-select-arrow'])[4]"));
    this.firstSelect = element(by.xpath("(//span[@class='mat-option-text'])[2]"));
    this.tagContact = element(by.xpath("//div[@id='tag-contact']//button[1]"));
    //Delete Contact
    this.deleteContact = element(by.xpath("//button[contains(text(),'Delete')]"));
    this.confirmBtn = element(by.xpath("//span[contains(text(),'CONFIRM')]"));

}
module.exports = new Contacts03PO();