var Permissions = function () {
    
    this.ListTab = element(by.xpath("//i[@id='list']"));
     this.Permissions = element(by.xpath("//span[text()=' Permissions ']"));

    this.searchField = element(by.id('search-value'));
    this.saveButton = element(by.xpath("//span[text()='Save']"));
    this.onOffButton = element(by.xpath("//mat-slide-toggle[@name='sliderWeb']"));
    this.saveScreenshotButton = element(by.id('save-button-0'));
    // this.SuccessToast = element(by.xpath("//div[contains(text(),'Sucessfully Updated')]")).getText();
    this.IntervalField = element(by.xpath("//input[@name='intervalWeb']"));
    this.editButton = element(by.id('edit-0'));
    this.IntervalFieldEditOption = element(by.xpath("//input[@id='interval']"));
    this.onOffEditOption = element(by.xpath("//input[@id='toggle-state-input']/.."));
    this.DayButton = element(by.xpath("//div[text()=' TUE ']"));
    this.saveEditButton = element(by.xpath("//span[text()='SAVE']"));
    this.screenshotTab = element(by.xpath("//div[text()=' Screenshot ']"));
    this.MatterLabel = element(by.xpath("//div[text()='Matter']"));
    this.FinancialLabel = element(by.xpath("//div[text()='Financial']"));
    this.MemberLabel = element(by.xpath("//div[text()='Member']"));
    this.EmailLabel = element(by.xpath("(//div[text()='Email'])[1]"));
    this.TimesheetLabel = element(by.xpath("//div[text()='Timesheet']"));
    this.ClassifyEmailLabel = element(by.xpath("//div[text()='Classify Email']"));
    this.classifyAttachmentsLabel = element(by.xpath("//div[text()='Classify Attachment']"));
    this.fullName = element(by.xpath("//td[@class='mat-cell cdk-column-fullName mat-column-fullName ng-star-inserted']"));
    this.Emailid = element(by.xpath("//td[@class='mat-cell cdk-column-email mat-column-email ng-star-inserted']"));
    this.resetButton = element(by.xpath("//span[text()='Reset']"));
    this.fullNameText = element(by.id('full-name-1')); 
    this.allFullNameText = element.all(by.xpath("//div[contains(@id,'full-name')]")); 

    this.screenshotEmailLabel = element(by.xpath("//button[text()=' Email ']"));
    this.screenshotActionLabel = element(by.xpath("//button[text()=' Action ']"));
    this.screenshotFullnameLabel = element(by.xpath("//button[text()=' Full Name ']"));
    this.ScreenshotOnoffLabel = element(by.xpath("//button[text()=' On/Off ']"));
    this.screenshotIntervalLabel = element(by.xpath("//button[text()=' Interval ']"));

    
    this.verifyNoFullName = async function () {
     
          expect(element(by.xpath("//td[@class='mat-cell cdk-column-fullName mat-column-fullName ng-star-inserted']")).isDisplayed()).toBe(false);
    
  }

  this.verifyNoFullNameList = async function () {
      
    expect(element(by.id('full-name-1')).isDisplayed()).toBe(false);

}

this.verifyNumberOfFullNameInList = async function () {
  // console.log('Number of names : '+element.all(by.xpath("//div[contains(@id,'full-name')]")).count())
  expect(element.all(by.xpath("//div[contains(@id,'full-name')]")).count()).toBe(10);

}

this.verifyNumberOfFullNameInScreenshot = async function () {
  // console.log('Number of names : '+element.all(by.xpath("//div[contains(@id,'full-name')]")).count())
  expect(element.all(by.xpath("//td[@class='mat-cell cdk-column-fullName mat-column-fullName ng-star-inserted']")).count()).toBe(50);

}

this.verifyFullName = async function () {
      
  expect(element(by.xpath("//td[@class='mat-cell cdk-column-fullName mat-column-fullName ng-star-inserted']")).isDisplayed()).toBe(true);

}

this.verifyFullNameList = async function () {

expect(element(by.id('full-name-1')).isDisplayed()).toBe(true);

}


    

    this.sliderDragDrop = function () {
        browser.actions().dragAndDrop(
            element(by.id('slider-range')),
            {x:100, y:0}
        ).perform();
      }
    
    // let analyticsUncheckedCheckboxes = ;/
    this.signOutButton = element(by.id('sign-out-button'));

    this.verifyCheckboxes = async function () {
        element.all(by.xpath("//input[@type='checkbox' and contains(@id,'analytics')]/..")).each(function(element, index) {
            expect(element.isPresent()).toBe(true);
      });
    }

    this.verifyCheckboxesDisabled = async function () {
        element.all(by.xpath("//input[@aria-checked='true' and contains(@id,'analytics')]")).each(function(element, index) {
            expect(element.isEnabled()).toBe(false);
      });
    }

    this.verifyEmailCheckboxesDisabled = async function () {
        element.all(by.xpath("//input[@aria-checked='true' and contains(@id,'email')]/..")).each(function(element, index) {
            expect(element.isEnabled()).toBe(false);
      });
    }

    this.clickOnUncheckedCheckboxes = function () {
      element(by.xpath("//input[@aria-checked='false' and contains(@id,'analytics')]/..")).isPresent().then(function () {
        element.all(by.xpath("//input[@aria-checked='false' and contains(@id,'analytics')]/..")).each(function(element, index) {
        element.click();
      });});
    }

    this.clickOnCheckedCheckboxes = function () {
        element.all(by.xpath("//input[@aria-checked='true' and contains(@id,'analytics')]/..")).each(function(element, index) {
        element.click();
      });
    }

    this.clickOnUncheckedEmailCheckboxes = function () {
        element.all(by.xpath("//input[@aria-checked='false' and contains(@id,'email')]/..")).each(function(element, index) {
        element.click();
      });
    }

    this.clickOnCheckedEmailCheckboxes = function () {
        element.all(by.xpath("//mat-checkbox[@id='email-classify-email-1' or @id='email-attachment-1']")).each(function(element, index) {
        element.click();
      });
    }
    
    this.verifyEmailCheckboxesEnabled = async function () {
        element.all(by.xpath("//mat-checkbox[@id='email-classify-email-1' or @id='email-attachment-1']")).each(function(element, index) {
            expect(element.isEnabled()).toBe(true);
      });
    }
    
      

  

}
module.exports = new Permissions();