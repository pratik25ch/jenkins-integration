var Template01PO = function () {
    //Click on New Category
    this.newCategoryBtn = element(by.id('newCat-btn'));
    //Add Catagory
    this.categoryInput = element(by.id('categoryTitle'));
    //Click on Save
    this.saveCategory = element(by.id('saveBtn'));
    //Click On Discard 
    this.discardBtn = element(by.id('discard-btn'));
    //Edit Category
    this.mousemove = function () {
        browser.actions().
        mouseMove(element(by.xpath("//div[@id='btn-div-0']"))).perform();
    }
    this.editCategoryBtn = element(by.xpath("//span[@id='temp0-0']"));
    //Add Template
    this.newTemplate = element(by.xpath("//a[@id='AddTemplate-btn-0']"));
    //temp name 
    this.templateName = element(by.name('name'));
    //attribute name 
    this.attributeName = element(by.name('attributeName'));
    this.addAttribute = element(by.name('addAttribute'));
    //attribute type
    this.attributeType = element(by.xpath("//mat-select[@id='attributeType']"));
    this.matVauleOption = element(by.xpath("//mat-option[@value='LIST']"));
    this.commaOptions = element(by.xpath("//input[@id='option']"));
    //Task
    this.taskName = element(by.id('taskName'));
    this.taskDescription = element(by.id('taskDescription'));
    this.addTaskBtn = element(by.name('addTask-btn'));
    //Save Template
    this.saveTemplate = element(by.name('save-btn'));
    this.cancelBtn = element(by.id('cancelTask-btn'));
    //Edit Template
    this.firstTemp = element(by.xpath("//span[@id='spn1-0-0']"));
    //remove button
    this.removeBtn = element(by.xpath("//i[@id='removebtn-1']"));

}
module.exports = new Template01PO();