var CommPage = require("../../../lib/CommLib/comm_lib")
let R = Math.random().toString(36).substring(7);

var SettingsWebsitePO = function () {

    //12=======WEBSITES
    let websites = element(by.id('mat-tab-label-0-1'));
    //Blocked Websites
    let addblocksites = element(by.xpath("(//span[contains(text(),'New')])[1]"));
    let websitename = element(by.xpath("//input[@name='websiteName']"));
    let websiteaddress = element(by.xpath("//input[@name='websiteAddress']"));
    let savewebsite = element(by.xpath("//button[@name='website-save']"));
    //Edit Websites 
    let editBlocksiteFirstBtn = element(by.xpath("(//tr[1]//td[2]//button[1])[1]"));
    //Remove Websites
    let removebutton = element(by.xpath("//button[@name='website-remove']"));
    //WhiteList Websites
    let addwhiteList = element(by.xpath("(//tr[1]/th[2])[2]/button[1]"));
    //Edit Websites
    let editWhiteListFirstBtn = element(by.xpath("(//tr[1]//td[2]//button[1])[2]"));


    this.settings_website = function () {
        websites.click().then(function () {
            console.log('Websites list Opened Sucessfully')
        })
    }
    this.newBlacklist_websites = function () {
        addblocksites.click();
        websitename.sendKeys('New-' + R + '-Blocked');
        websiteaddress.sendKeys('https://new' + R + '.in');
        savewebsite.click().then(function () {
            console.log('added Sucessfully')
        })
        CommPage.sleep(2);
    }
    this.editBlacklist_websites = function () {
        editBlocksiteFirstBtn.click();
        websitename.clear().sendKeys('Edit-' + R + '-BlockedGroup');
        websiteaddress.clear().sendKeys('https://edit' + R + '.in');

        savewebsite.click().then(function () {
            console.log('updated Sucessfully')
        })
        CommPage.sleep(2);
    }
    this.removeBlacklist_websites = function () {
        editBlocksiteFirstBtn.click();
        removebutton.click().then(function () {
            console.log('Removed Sucessfully')
        })
        CommPage.sleep(2);
    }
    this.newWhitelist_websites = function () {
        addwhiteList.click();
        websitename.sendKeys('Flipkart-' + R + '-India');
        websiteaddress.sendKeys('https://flipkartindia' + R + '.in');
        savewebsite.click().then(function () {
            console.log('added Sucessfully')
        })
        CommPage.sleep(2);
    }
    this.editWhitelist_websites = function(){
        editWhiteListFirstBtn.click();
        websitename.clear().sendKeys('Edit-' + R + '-whitelist');
        websiteaddress.clear().sendKeys('https://edit' + R + '.in');
        savewebsite.click().then(function () {
                console.log('updated Sucessfully')
        })
        CommPage.sleep(2);
    }
    this.removeWhitelist_websites = function(){
        editWhiteListFirstBtn.click();
        removebutton.click().then(function () {
                console.log('Removed Sucessfully')
        })
        CommPage.sleep(1);
    }
    





}
module.exports = new SettingsWebsitePO();