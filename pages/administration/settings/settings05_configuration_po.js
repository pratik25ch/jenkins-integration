var CommPage = require("../../../lib/CommLib/comm_lib")

var SettingsConfigurationPO = function () {

    let configuration = element(by.xpath("//div[@id='mat-tab-label-0-4']//div[1]"));
    //edit SMTP conf
    let editSMTPbutton = element(by.xpath("//button[@name='smtp-config']"));
    let flowacelogo = element(by.xpath("//div[@id='flowace-logo']//span[1]"));
    let saveSTMP = element(by.xpath("//span[contains(text(),'save')]"));

    let gmaillogo = element(by.id("gmail-logo"));
    let fromemail = element(by.id("from"));
    let useremail = element(by.id("user"));
    let userpassword = element(by.id("password"));
    //Timesheet & email alert
    let adminemail = element(by.xpath("//input[@id='report-to-admin-email']"));
    let reportingdays = element(by.xpath("//input[@name='reporting-days']"));
    let emailapplybutton = element(by.xpath("//button[@name='adminEmail-btn']"));
    //Default Plugin Time
    let plugin = element(by.name('idle-time'));
    let applyplugin = element(by.id('idleTimeBtn'));


    this.settings_Configuration = function () {
        configuration.click().then(function () {
            console.log('Integration Opened Sucessfully')
        })
        CommPage.sleep(1);
    }
    this.flowaceConfiguration = function () {
        editSMTPbutton.click();
        CommPage.sleep(2);
        flowacelogo.click();
        saveSTMP.click().then(function () {
            console.log('Email Configuration Edited Successfully')
        })
        CommPage.sleep(5);
    }
    this.gmailConfiguration = function () {
        editSMTPbutton.click();
        CommPage.sleep(3);
        gmaillogo.click();
        fromemail.clear().sendKeys('QA Team');
        useremail.clear().sendKeys('qa1@flowace.in');
        userpassword.clear().sendKeys('QA@12345')
        saveSTMP.click().then(function () {
            console.log('Updated Sucessfully')
        })
        CommPage.sleep(3);
    }
    this.timesheetEmailAlearts = function () {
        adminemail.clear().sendKeys('qa1@flowace.in');
        reportingdays.clear().sendKeys('1');
        emailapplybutton.click().then(function () {
            console.log('Updated Sucessfully')
        })
        CommPage.sleep(0.5);
    }
    this.defaultPlugInIdleTime = function(){
        CommPage.sleep(1);
        //Default Plugin Idle Time
        plugin.clear().sendKeys('10');
        applyplugin.click().then(function () {
                console.log('Updated Sucessfully');
        })
        CommPage.sleep(1);
    }
    
}
module.exports = new SettingsConfigurationPO()