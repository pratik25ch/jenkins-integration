var CommPage = require("../../../lib/CommLib/comm_lib")

var SettingsEmpPO = function () {
    //Employee Dafaults
    let fulldays = element(by.xpath("//input[@name='full-days']"));
    let halfdays = element(by.xpath("//input[@name='Half-Days']"));
    let fulldayhours = element(by.xpath("//input[@name='Full-day-hr']"));
    let halfdayshours = element(by.xpath("//input[@name='Half-Day-Hr']"));
    let applybutton = element(by.xpath("//button[@id='employee-btn']"));

    //Finances
    this.overheadfactordate = async function () {
        await element(by.xpath("//mat-datepicker-toggle[@id='overhead-datetoggle']//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted']")).click();
        await element(by.xpath("//div[@class='mat-calendar-arrow']")).click();
        await element(by.xpath("(//div[@class='mat-calendar-body-cell-content'])[4]")).click();
        await element(by.xpath("//div[contains(text(),'MAR')]")).click();
        await element(by.xpath("//div[contains(text(),'11')]")).click();
        CommPage.sleep(1);
    }
    this.profitabilityfactordate = async function () {
        await element(by.xpath("//mat-datepicker-toggle[@name='profit-picker1']")).click();
        await element(by.xpath("//div[@class='mat-calendar-arrow']")).click();
        await element(by.xpath("(//div[@class='mat-calendar-body-cell-content'])[4]")).click();
        await element(by.xpath("//div[contains(text(),'AUG')]")).click();
        await element(by.xpath("(//div[contains(text(),'20')])[2]")).click();
        CommPage.sleep(1);
    }
    let overheadFactor = element(by.xpath("//input[@name='over-factor']"));
    let profitFactor = element(by.xpath("//input[@id='profit-factor']"));
    //Apply Button
    let financebutton = element(by.id('finance-btn'));
    //Confirm Button
    let comfirmbutton = element(by.xpath("//span[contains(text(),'CONFIRM')]"));

    //TERMINOLOGY
    let terminology = element(by.xpath("//a[@id='Terminology-edit']"));
    //Group
    let L1 = element(by.xpath("//tr[@id='terminology-1']//td[@id='term-1']"));
    let L1input = element(by.xpath("//input[@id='terminput-1']"));
    //Client
    let L2 = element(by.xpath("//tr[@id='terminology-2']//td[@id='term-2']"));
    let L2input = element(by.xpath("//input[@id='terminput-2']"));
    //matter
    let L3 = element(by.xpath("//tr[@id='terminology-3']//td[@id='term-3']"));
    let L3input = element(by.xpath("//input[@id='terminput-3']"));
    //Employee
    let empterm = element(by.xpath("//tr[@id='terminology-4']//td[@id='term-4']"));
    let empterminput = element(by.xpath("//input[@id='terminput-4']"));
    //SaveButton
    let saveterminology = element(by.xpath("//button[@name='update-terminology']"));
    //1=======EMPLOYEE
    let employee = element(by.id('mat-tab-label-0-0'));


    this.settings_employee = function(){
        employee.click().then(function () {
            console.log('Employee Opened Sucessfully')
        })
    } 
    this.employee_Defaults = function (FD,HD,FDH,HDH) {
        fulldays.clear().sendKeys(FD);
        halfdays.clear().sendKeys(HD);
        CommPage.sleep(1);
        fulldayhours.clear().sendKeys(FDH);
        halfdayshours.clear().sendKeys(HDH);
        applybutton.click().then(function () {
            console.log('updated Sucessfully')
        })
        CommPage.sleep(3);
    }
    this.employee_Finances = async function () {
        overheadFactor.clear().sendKeys('2.3');
        profitFactor.clear().sendKeys('2');
        financebutton.click();
        CommPage.sleep(1);
        comfirmbutton.click().then(function () {
            console.log('settings updated Sucessfully')

        })
        CommPage.sleep(5);
    }
    this.language_Terminology = function (G,C,M,E) {
        terminology.click();
        L1.click();
        L1input.clear().sendKeys(G);
        CommPage.sleep(0.5);
        L2.click();
        L2input.clear().sendKeys(C);
        CommPage.sleep(0.5);
        L3.click();
        L3input.clear().sendKeys(M);
        CommPage.sleep(0.5);
        empterm.click();
        empterminput.clear().sendKeys(E);
        saveterminology.click().then(function () {
            console.log('added Sucessfully')
        })
        CommPage.sleep(2);
    }
    this.employee_Modified = function () {
        fulldays.clear().sendKeys('250');
        halfdays.clear().sendKeys('40');
        CommPage.sleep(1);
        fulldayhours.clear().sendKeys('8');
        halfdayshours.clear().sendKeys('4');
        applybutton.click().then(function () {
            console.log('updated Sucessfully')
        })
        CommPage.sleep(3);
    }

}
module.exports = new SettingsEmpPO();