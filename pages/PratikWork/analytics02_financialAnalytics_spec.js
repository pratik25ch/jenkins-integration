var CommPage = require("../../lib/CommLib/comm_lib")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var TimesheetPage = require("../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../pages/billing/billing_po")
var AnalyticsPage01 = require("../../pages/analytics/analytics01_matterAnalytics_po")
var AnalyticsPage02 = require("../../pages/PratikWork/analytics02_financialAnalytics_po")
var AnalyticsPage03 = require("../../pages/analytics/analytics03_memberAnalytics_po")
var AnalyticsPage04 = require("../../pages/analytics/analytics04_timesheetAnalytics_po")

let R = Math.random().toString(36).substring(7);

beforeAll(function () {
    LoginPage.GetURL();
    LoginPage.login()
    CommPage.sleep(3);
    HomePage.Analytics();
    CommPage.sleep(3);
})

describe('ANALYTICS Module:--- FINANCIAL Analytics', function () {
    it('Should Analyze Report', function () {
        CommPage.sleep(5);

        AnalyticsPage02.financialAnlytics.click();
        CommPage.sleep(3);

        AnalyticsPage02.matterNameOption.click();
        CommPage.sleep(3);

        AnalyticsPage02.matterSearch.sendKeys("demo pro ");
        CommPage.sleep(3);

        AnalyticsPage02.matterName.click();
        CommPage.sleep(3);

        AnalyticsPage02.matterSearch.sendKeys(protractor.Key.ESCAPE);;
        CommPage.sleep(3);

     

        // AnalyticsPage02.startDate.click();
        // CommPage.sleep(3);

        // AnalyticsPage02.todaysSelectedDate.click();
        // CommPage.sleep(3);

        AnalyticsPage02.EndDate.click();
        CommPage.sleep(3);

        AnalyticsPage02.todaysSelectedDate.click();
        CommPage.sleep(3);

        AnalyticsPage02.analyseButton.click();




    })
})

