var CommPage = require("../../lib/CommLib/comm_lib")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")

var EmployeePage01 = require("../../pages/administration/employees/employees01_po")

var e = require("../../testData/DataDriven/01_employeeData")

let R = Math.random().toString(36).substring(7);

// beforeAll(function () {
//     LoginPage.GetURL();
//     LoginPage.login()
//     CommPage.sleep(3);
//     HomePage.Administration();
//     CommPage.sleep(3);
//     HomePage.Employee();
//     CommPage.sleep(3);
// })

describe('ANALYTICS Module:--- FINANCIAL Analytics', function () {
    // let newEmail = 'new-' + R + '@test.com';
    let newEmail = 'pratik25c@gmail.com';
    let email = element(by.name('email'));
    let password = element(by.name('password'));
    let loginbutton = element(by.xpath("//button[@name='login-button']"));
    let logOut = element(by.name('sign-out-button'))
    let termsAcceptBtn = element(by.xpath("//span[contains(text(),'ACCEPT')]"));

    it('Should Create Employee', async function () {
        //new emp button
        LoginPage.GetURL();
    LoginPage.login()
    CommPage.sleep(3);
    HomePage.Administration();
    CommPage.sleep(3);
    HomePage.Employee();
    CommPage.sleep(3);
        EmployeePage01.newEmpBtn();
        CommPage.sleep(3);
        EmployeePage01.firstname.sendKeys(e.employee.NewEmployee.firstName);
        EmployeePage01.lastname.sendKeys(e.employee.NewEmployee.lastName);
        EmployeePage01.email.sendKeys(newEmail);
        EmployeePage01.yearlysalary.sendKeys(e.employee.NewEmployee.yearlySal);
        EmployeePage01.fulldays.clear().sendKeys(e.employee.NewEmployee.fullDays);
        EmployeePage01.halfdays.clear().sendKeys(e.employee.NewEmployee.halfDays);
        EmployeePage01.fulldaysHours.clear().sendKeys(e.employee.NewEmployee.fullHours);
        EmployeePage01.halfdaysHours.clear().sendKeys(e.employee.NewEmployee.halfHours);
        EmployeePage01.chargeOutRate.clear().sendKeys(e.employee.NewEmployee.chargeOutRate);
        EmployeePage01.role.click();
        EmployeePage01.employeeBtnRole.click();
        EmployeePage01.contactNumber.sendKeys(e.employee.NewEmployee.contactNumber);
        EmployeePage01.reportsTo.sendKeys(e.employee.NewEmployee.reportsTo);
        EmployeePage01.reportslist.click();
        //save
        EmployeePage01.savebutton.click().then(function () {
            console.log('User Created Sucessfully')
            expect('User Created Sucessfully').toEqual('User Created Sucessfully');
        })
        CommPage.sleep(3);
    })

    


})

