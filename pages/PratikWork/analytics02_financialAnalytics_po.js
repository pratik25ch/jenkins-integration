var FINANCIAL_ANALYTICSPO02 = function () {
    
    //Financial Anyalytics
    // this.financialAnlytics = element(by.xpath("//div[text()='Financial Analytics']"));
    this.financialAnlytics = element(by.id('mat-tab-label-0-1'));

    
    this.selectMatter = element(by.xpath("//mat-select[@placeholder='Select Matter']"));
    this.searchForMatter = element(by.xpath("//input[@placeholder='Search for matter']"));
    this.firstOptionMatter = element(by.id('matter-list-0'));
    this.graph = element(by.xpath("//div[@class='pd-tp highstock-chart']"));

    this.scrollEnd = function () {
        browser.actions()
            .sendKeys(protractor.Key.END)
            .perform();
    }
    
    this.matterNameOption = element(by.xpath("//mat-select[@placeholder='Select Matter']"));
    this.matterSearch = element(by.xpath("//input[@placeholder='Search for matter']"));
    this.matterName = element(by.xpath("//span[text()='DEMO PRO - 27.5']"));
   
    this.newButton = element(by.id("add-employee"));
    this.firstname = element(by.name('firstName'));
    this.lastname = element(by.name('lastName'));
    this.email = element(by.name('email'));
    this.yearlysalary = element(by.name('yearlySalary'));
    this.fulldays = element(by.name('fullDays'));
    this.halfdays = element(by.name('halfDays'));
    this.fulldaysHours = element(by.name('fullDaysHours'));
    this.halfdaysHours = element(by.name('halfDaysHours'));
    this.chargeOutRate = element(by.name('chargeOutRate'));
    this.role = element(by.name('role'));
    this.adminBtnRole = element(by.xpath("(//span[@class='mat-option-text'])[3]"));
    this.managerBtnRole = element(by.xpath("(//span[@class='mat-option-text'])[2]"));
    this.employeeBtnRole = element(by.xpath("(//span[@class='mat-option-text'])[1]"));
    this.contactNumber = element(by.name('contactNumber'));
    this.reportsTo = element(by.name('member'));
    this.reportslist = element(by.xpath("(//span[@class='mat-option-text'])[1]"));
    this.savebutton = element(by.xpath("//span[contains(text(),'SAVE')]"));
    this.cancelbutton = element(by.id('cancel-edit-employee'));
    
    

    this.startDate = element(by.xpath("(//button[@class='mat-icon-button'])[1]"));
    this.EndDate = element(by.xpath("(//button[@class='mat-icon-button'])[2]"));
    var d = new Date();

    this.todaysSelectedDate = element(by.xpath("//div[text()='"+d.getDate().toString()+"']"));

    this.dateBeforeFiveDays = function () {
       let dateText= todaysSelectedDate.getText();
       let dateInt = parseInt(dateText, 10);
       let beforeIntDate = dateInt-5;

element(by.xpath(`(//div[@class='mat-calendar-body-cell-content'])[${beforeIntDate}]`)).click();
    }

    this.analyseButton = element(by.id("analyze-financial-analytics"));
    
}
module.exports = new FINANCIAL_ANALYTICSPO02();