    var MEMBER_ANALYTICSPO01 = function () {
        this.matterAnalyticsTab = element(by.xpath("//div[text()='Matter Analytics']"));

        this.selectAssociate = element(by.id('mat-select-associate'));
        this.associateInput = element(by.xpath("//input[@placeholder='Associate']"));
        this.firstOption = element(by.id('Associate0'));
        //click on calender Icon
        this.startDate = element(by.xpath("(//button[@class='mat-icon-button'])[1]"));
        this.endDate = element(by.xpath("(//button[@class='mat-icon-button'])[2]"));
        //First Calender
        this.FirstCalender = function () {
            var Todaydate = element(by.xpath("//div[@class='mat-calendar-body-cell-content mat-calendar-body-today']"));
            Todaydate.isPresent().then(function (T) {

                if (T) {

                    Todaydate.click();

                } else {

                    element(by.xpath("//button[contains(@class,'mat-calendar-next-button mat-icon-button')]")).click();
                    element(by.xpath("//div[@class='mat-calendar-body-cell-content mat-calendar-body-today']")).click();
                }
            })
        }

        this.todaysDate = element(by.xpath("//div[@class='mat-calendar-body-cell-content mat-calendar-body-today']"));
        this.analyzeReport = element(by.xpath("//span[text()='analyze']"));

        this.analyzeData = function () {
            element(by.id('analytics-data3')).getText().then(function (text) {
                console.log('Total Number of Matters--', text);
            })
        }

        this.newMatterData = element(by.id('new-matter-value'));
        this.closeModel = element(by.id('close-modal'));
        this.downloadReport = element(by.id('pnt-btn'));
        this.closedMatterData = element(by.id('closed-matter-value'));
        this.activeMatterData = element(by.id('active-matter-value'));
        //Total MatterList
        this.matterList = function () {
            let list = element.all(by.xpath("(//p[contains(@class,'matter-name ')])"));
            list.count()
        }
        //Descending Active Matter Data
        this.descending = element(by.id('l3-name'));
        //First Matter
        this.firstMatter = element(by.xpath("(//p[contains(@class,'matter-name ')])[1]"));
        this.childWindow = function () {
            browser.getAllWindowHandles().then(function (handles) {
                browser.switchTo().window(handles[1]);
            })
        }
        this.parentWindow = function () {
            browser.getAllWindowHandles().then(function (handles) {
                // We currently are on the second tab...
                browser.driver.close();
                browser.switchTo().window(handles[0]);
            })
        }
        //Tracked Time
        this.AnalyticsUnclassifiedTime = async function () {
            return await element(by.id('unclassified-hours')).getText();
        }
        this.AnalyticsClassifiedTime = async function () {
            return await element(by.id('classified-hours')).getText();
        }
        //Analytics Matter Search
        this.searchMatter_Analytics = element(by.name('searchText'));

    }
    module.exports = new MEMBER_ANALYTICSPO01();