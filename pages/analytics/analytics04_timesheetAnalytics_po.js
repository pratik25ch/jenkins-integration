var CommPage = require("../../lib/CommLib/comm_lib")
var TIMESHEET_ANALYTICS04 = function () {

    // this.timesheetAnalytics = element(by.xpath("//div[contains(text(),'Timesheet Analytics')]"));
    this.timesheetAnalytics = function () {
        element(by.xpath("//div[contains(text(),'Timesheet Analytics')]")).click().then(function () {
            console.log("Timesheet Analytics Opened Successfully")
        })
    }
    this.searchMember = element(by.id('search-value'));
    this.getReport = element(by.xpath("//span[contains(text(),'Get')]"));

    //Timesheet Cpmparision
    this.TimesheetAnalytics_CF = async function () {
        return await element(by.id('cf-duration-1')).getText();
    }
    this.TimesheetAnalytics_UCF = async function () {
        return await element(by.id('ucf-hr-1')).getText();
    }

    let name_AD = element(by.id('timesheet-username'));
    let cf_AD = element(by.id('cf-hr-header'));
    let ucf_AD = element(by.id('ucf-ht'));
    let total_AD = element(by.id('total-duration'));
    let percentage_AD = element(by.id('pct'));
    let desktopversion_AD = element(by.id('desktop-version'));
    let pluginTime_AD = element(by.id('plugin-time'));
    let webTime_AD = element(by.id('web-time'));

    this.timesheetAnalytics_AD = function(){
        name_AD.click();
        CommPage.sleep(1);
        name_AD.click();
        CommPage.sleep(1);

        cf_AD.click();
        CommPage.sleep(1);
        cf_AD.click();
        CommPage.sleep(1);

        ucf_AD.click();
        CommPage.sleep(1);
        ucf_AD.click();
        CommPage.sleep(1);

        total_AD.click();
        CommPage.sleep(1);
        total_AD.click();
        CommPage.sleep(1);

        // percentage_AD.click();
        // CommPage.sleep(1);
        // percentage_AD.click();
        // CommPage.sleep(1);

        desktopversion_AD.click();
        CommPage.sleep(1);
        desktopversion_AD.click();
        CommPage.sleep(1);

        pluginTime_AD.click();
        CommPage.sleep(1);
        pluginTime_AD.click();
        CommPage.sleep(1);

        webTime_AD.click();
        CommPage.sleep(1);
        webTime_AD.click();
        CommPage.sleep(1);

    }

}
module.exports = new TIMESHEET_ANALYTICS04();