var MEMBER_ANALYTICS03 = function () {

    this.memberAnalytics = element(by.xpath("//div[contains(text(),'Member Analytics')]"));
    this.ClickOnMemberAnalytics = function () {
        memberAnalytics.click().then(function () {
            console.log("Member Analytics Opened Successfully")
        })
    }
    this.previousMonth = element(by.id("backward-date"));
    this.nextMonth = element(by.id('forward-date'));
    this.allEntriesText = function () {
        element(by.css("div.total-entries-detail.pd-tp-25")).getText().then(function (text) {
            console.log("All time Entries are Displayed Successfully");
            console.log(text);
        })
    }
    this.selectUser = element(by.xpath("//div[@class='mat-select-arrow']"));
    this.userInput = element(by.xpath("//input[@placeholder='Search for User...']"));
    this.firstOption = element(by.id('Search for User...0'));





}
module.exports = new MEMBER_ANALYTICS03();