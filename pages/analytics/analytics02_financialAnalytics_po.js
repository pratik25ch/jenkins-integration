var FINANCIAL_ANALYTICSPO02 = function () {
    
    //Financial Anyalytics
    this.financialAnlytics = element(by.xpath("//div[text()='Financial Analytics']"));
    this.selectMatter = element(by.xpath("//mat-select[@placeholder='Select Matter']"));
    this.searchForMatter = element(by.xpath("//input[@placeholder='Search for matter']"));
    this.firstOptionMatter = element(by.id('matter-list-0'));
    this.graph = element(by.xpath("//div[@class='pd-tp highstock-chart']"));

    this.scrollEnd = function () {
        browser.actions()
            .sendKeys(protractor.Key.END)
            .perform();
    }
    
}
module.exports = new FINANCIAL_ANALYTICSPO02();