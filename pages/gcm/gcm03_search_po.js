var GCM03PO = function () {

    //GCM Search
    this.gcmSearch = element(by.id('search-gcm'));
    this.totalMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters--', text);
        })
    }
    //Yesterday   
    this.selectDate = element(by.xpath("//mat-select[@id='select-date']//div[@class='mat-select-arrow-wrapper']"));
    this.yesterday = element(by.xpath("(//span[@class='mat-option-text'])[1]"));
    this.searchBtn = element(by.xpath("//span[@class='mat-button-wrapper'][contains(text(),'search')]"));
    this.yesterdayMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters Select by Yesterday--', text);
        })
    }
    this.clearBtn = element(by.xpath("//span[contains(text(),'CLEAR')]"));
    //Last 7 Days
    this.last7Days = element(by.xpath("(//span[@class='mat-option-text'])[2]"));
    this.last7DaysMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters Select by Last 7 Days--', text);
        })
    }
    //Last 30 Days
    this.last30Days = element(by.xpath("(//span[@class='mat-option-text'])[3]"));
    this.last30DaysMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters Select by Last 30 Days--', text);
        })
    }
    //This Month
    this.thisMonth = element(by.xpath("(//span[@class='mat-option-text'])[4]"));
    this.thisMonthMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters Select by This Month--', text);
        })
    }
    //Last Month
    this.lastMonth = element(by.xpath("(//span[@class='mat-option-text'])[5]"));
    this.lastMonthMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters Select by Last Month--', text);
        })
    }
    //Last Year
    this.lastYear = element(by.xpath("(//span[@class='mat-option-text'])[6]"));
    this.lastYearMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters Select by Last Year--', text);
        })
    }
    //Active
    this.active = element(by.xpath("//span[@class='mat-checkbox-label'][contains(text(),'Active')]"));
    this.activeMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Active Matters--', text);
        })
    }
    //Archived
    this.archived = element(by.xpath("//span[@class='mat-checkbox-label'][contains(text(),'Archived')]"));
    this.archivedMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Archived Matters--', text);
        })
    }
    //group
    this.group = element(by.id('mat-select-group'));
    this.groupSelect = element(by.xpath("(//span[@class='mat-option-text'])[2]"));
    this.groupMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters Select by Group--', text);
        })
    }
    //client
    this.client = element(by.id('mat-select-client'));
    this.clientSelect = element(by.xpath("(//span[@class='mat-option-text'])[3]"));
    this.clientMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters Select by Client--', text);
        })
    }
    //dropbox
    this.dropbox = element(by.xpath("//span[@class='mat-checkbox-label'][contains(text(),'Dropbox')]"));
    this.dropboxMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters Only Select by Dropbox--', text);
        })
    }
    this.withoutDropboxMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters Only Select by Dropbox not available--', text);
        })
    }
    //Salesforce
    this.salesforce = element(by.xpath("//span[@class='mat-checkbox-label'][contains(text(),'salesforce')]"));
    this.salesforceMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters Only Select by Salesforce--', text);
        })
    }
    this.withoutSalesforceMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters Only Select by Salesforce not available--', text);
        })
    }
    this.doubleclickDropbox = function () {
        browser.actions().doubleClick(element(by.xpath("//span[@class='mat-checkbox-label'][contains(text(),'Dropbox')]"))).perform();
    }
    this.doubleclickSalesforce = function () {
        browser.actions().doubleClick(element(by.xpath("//span[@class='mat-checkbox-label'][contains(text(),'salesforce')]"))).perform();
    }
    this.withoutDropboxSalesforceMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters not available in Dropbox and Salesforce--', text);
        })
    }
    //SelectEmployee
    this.selectEmployee = element(by.xpath("//input[@role='combobox']"));
    this.selectEmployeeMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters Search by Employee Name--', text);
        })
    }
    //Matter Search
    this.matter = element(by.id('matter-name'));
    this.totalIconMatters = function () {
        element(by.xpath("//div[@class='center result-count']")).getText().then(function (text) {
            console.log('Total Number of Matters Select by Matter Name--', text);
        })
    }
    this.exportsMatter = element(by.id('export-btn'));








    
    //Global Search 
    this.globalSearchIcon = element(by.xpath("//i[@class='fa fa-search']"));
    this.matterSearchInput = element(by.id('matter-search'));
    this.firstMatterSearch = element(by.xpath("//i[@class='fa fa-angle-right'][1]"));
    this.clientClick = element(by.xpath("(//span[@class='client-name'])[1]"));
    this.globalClientSearch = element(by.id('keyword-1'));
    this.globalGroupSearch = element(by.id('keyword-0'));

    this.globalSearch_firstClient = element(by.id('client-list-0'));
    //Associate Icon Matter
    this.iconMatterAssociate = element(by.xpath("(//i[@class='fa fa-plus'])[2]"));
    //Contact Icon Details 
    this.contactDetails = element(by.xpath("//span[contains(text(),'0A')]"));
    this.closeContact = element(by.xpath("//span[contains(text(),'close')]"));
    this.escBtn = function () {
        browser.actions().sendKeys(protractor.Key.ESCAPE).perform();
    }
    this.createClientGlobalSearch = element(by.id('add-client-0'));





}
module.exports = new GCM03PO();