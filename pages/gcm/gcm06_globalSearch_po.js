var GCM06PO = function(){

    //Global Search 
    this.globalSearchIcon = element(by.xpath("//i[@class='fa fa-search']"));
    this.matterSearchInput = element(by.id('matter-search'));
    this.firstMatterSearch = element(by.xpath("//i[@class='fa fa-angle-right'][1]"));
    this.clientClick = element(by.xpath("(//span[@class='client-name'])[1]"));
    this.globalClientSearch = element(by.id('keyword-1'));
    this.globalGroupSearch = element(by.id('keyword-0'));

    this.globalSearch_firstClient = element(by.id('client-list-0'));
    //Associate Icon Matter
    this.iconMatterAssociate = element(by.xpath("(//i[@class='fa fa-plus'])[2]"));
    //Contact Icon Details 
    this.contactDetails = element(by.xpath("//span[contains(text(),'0A')]"));
    this.closeContact = element(by.xpath("//span[contains(text(),'close')]"));
    this.escBtn = function () {
        browser.actions().sendKeys(protractor.Key.ESCAPE).perform();
    }
    this.createClientGlobalSearch = element(by.id('add-client-0'));

    let advanceSearch = element(by.xpath("//i[@class='fa fa-caret-down']"));
    let activeMatters = element(by.xpath("(//mat-checkbox[@type='checkbox'])[1]"));
    let archivedMatters = element(by.xpath("(//mat-checkbox[@type='checkbox'])[2]"));
    let searchResult =  element(by.css('span.search-result-length.ng-star-inserted'));
    let searchBtn = element(by.xpath("//span[contains(text(),'Search')]"));
    let dropboxMatter = element(by.xpath("(//div[@class='mat-checkbox-inner-container'])[7]"));
    let salesforceMatter = element(by.xpath("(//div[@class='mat-checkbox-inner-container'])[8]"));
    let selectDate = element(by.id('select-date'));

    this.exportBtn = element(by.xpath("(//button[@name='export-btn'])[2]"));
    this.clearSearch = element(by.xpath("//i[@mattooltip='clear search']"));
    this.yesterday = element(by.xpath("//span[contains(text(),'Yesterday')]"));
    

    this.advanceSearch = function(){
        advanceSearch.click();
    }
    this.searchResult = async function(){
        return await element(by.css('span.search-result-length.ng-star-inserted')).getText();
    }
    this.ActiveMatters = function(){
        activeMatters.click();
    }
    this.ArchivedMatters = function(){
        archivedMatters.click();
    }
    this.DropboxMatter = function(){
        dropboxMatter.click();
    }
    this.salesforceMatter = function(){
        salesforceMatter.click();
    }
    this.selectDate = function(){
        selectDate.click();
    }



    this.searchBtn = function(){
        searchBtn.click();
    }


}
module.exports = new GCM06PO()