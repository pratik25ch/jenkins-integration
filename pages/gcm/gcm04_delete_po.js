let R = Math.random().toString(36).substring(7);
var CommPage = require("../../lib/CommLib/comm_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var GCM04PO = function () {

    //Delete Client

    this.deleteBtn = element(by.xpath("//span[contains(text(),'DELETE')]"));
    this.reasonDeletion_Group = element(by.xpath("(//label[text()='Select Employee']/following::input)[2]"));
    this.reasonDeletion_Client = element(by.xpath("//input[contains(@class,'mat-input-element mat-form-field-autofill-control')]"));


    this.bulkUpload_Shotcut = element(by.xpath("//button[@mattooltip='Bulk Upload']"));
    this.bulkUpload = element(by.id('bulk-upload'));
    this.uploadFile = element(by.id('excel-file'));
    this.ExportAll = element(by.id('export-all'));

    let GDelete = element(by.css('button.mat-button.mat-warn'));


    //Delete Matter

    this.deleteTooltip = element(by.xpath("//button[@mattooltip='DELETE']"));
    this.deleteMatterBtn = element(by.xpath("//span[contains(text(),'DELETE')]"));
    this.ArchiveDeleteMatter = element(by.xpath("//div[contains(text(),'Archive Matter')]"));

    this.deleteMatterData = element(by.xpath("//div[@class='square cursor-pointer']"));
    this.validateBtn = element(by.id('validate-btn'));
    this.confirmMatterName = element(by.xpath("//input[@placeholder='Matter Name']"));
    this.deletionReason = element(by.xpath("//textarea[@placeholder='Reason (optional)']"));
    //this.reasonDeletion = element(by.xpath("//input[contains(@class,'mat-input-element mat-form-field-autofill-control')]"));


    //     Maatter view                         //button[contains(@class,'mat-button cdk-focused')]










    //Delete Blank Group
    let blankGroup = async function () {
        return await element(by.xpath("(//div[@name='no-client']/div[@class='gp-div hide-overflow'])[1]")).getText();
    }
    let mouseMoveBlankGroup = function () {
        browser.actions()
            .mouseMove(element(by.xpath("(//div[@name='no-client'])[1]")))
            .perform();
    }
    let nullGroup = element(by.xpath("(//div[@name='no-client'])[1]"))
    this.deleteBlankGroup = function () {
        nullGroup.isPresent().then(async function (T) {
            if (T) {
                mouseMoveBlankGroup();
                blankGroup();
                CommPage.sleep(2);
                GCMPage03.globalSearchIcon.click();
                CommPage.sleep(2);
                GCMPage03.globalGroupSearch.click();
                CommPage.sleep(2);
                let V1 = await blankGroup();
                //console.log(`V1 ${V1} type ${typeof(V1)}`);
                CommPage.sleep(2);
                GCMPage03.matterSearchInput.sendKeys(V1);
                CommPage.sleep(3);
                GCMPage01.globalSearchMouseMoveFirst();
                GCMPage01.editGlobalGroup.click();
                CommPage.sleep(1);
                GDelete.click();
                CommPage.sleep(5);
                GDelete.click();
                CommPage.sleep(5);
                CommPage.escBtn();
                CommPage.refresh();
            } else {
                browser.close();
            }
        })
    }










}
module.exports = new GCM04PO();