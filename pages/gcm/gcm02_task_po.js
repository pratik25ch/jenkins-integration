var GCM02PO = function () {

    //Add Task
    this.taskBtn = element(by.id('tasks'));
    this.taskName = element(by.name('name'));
    this.taskDescription = element(by.name('description'));
    this.addTask = element(by.id('add-task'));
    this.clickTaskName1 = element(by.xpath("(//span[@name='matterTaskName'])[1]"));
    this.clickTaskName2 = element(by.xpath("(//span[@name='matterTaskName'])[2]"));
    this.editTask = element(by.xpath("(//a[@name='edit-task-btn'])[1]"));
    this.firstTaskInput = element(by.id('matterTaskName-0'));
    this.saveTask = element(by.xpath("(//a[@id='save-task-btn']//span)[1]"));
    this.markAsActive = element(by.xpath("(//button[@class='btn-na'])[1]"));
    this.firstCheckbox = element(by.xpath("(//mat-checkbox[@name='checkbox-matterTask'])[1]"));
    this.notAvail = element(by.xpath("(//button[@class='btn-na'])[1]"));

    this.mouseMoveiIcon = function () {
        browser.actions()
            .mouseMove(element(by.xpath("//i[@class='fa fa-info']")))
            .perform();
    }
    //Edit Desc
    this.editDescBtn = element(by.xpath("//span[@class='txt-sm'][contains(text(),'Edit')]"));
    this.editDescInput = element(by.xpath("//textarea[@placeholder='Edit Description']"));
    this.saveDescBtn = element(by.id('save-des-btn'));
    //Add comment
    this.comments = element(by.xpath("//div[text()='Comments']"));
    this.addComment = element(by.id('add-comment'));
    this.submitComment = element(by.id('submit-comment'));
    this.editComment = element(by.id('edit-comment-0'));
    this.editCommentInput = element(by.xpath("//input[@appautofocus]"));
    this.saveComment = element(by.id('save-task-btn-0'));
    this.deleteComment = element(by.id('delete-comment-0'));
    this.confirmDeleteComment = element(by.xpath("//span[contains(text(),'CONFIRM')]"));
    //Expenses
    this.ExpensesBtn = element(by.id('expenses'));
    this.createExpenses = element(by.xpath("//button[@class='new-expense-icon mat-mini-fab mat-accent']"));
    this.member = element(by.xpath("//input[@formcontrolname='member']"));
    this.firstMember = element(by.xpath("//mat-option[@role='option']//span[1]"));
    this.expensesType = element(by.xpath("//input[@placeholder='Expense Type']"));
    this.communication = element(by.xpath("//span[text()=' Communication ']"));
    this.fuel = element(by.xpath("//span[text()=' Fuel ']"));
    this.expenseAmount = element(by.xpath("//input[@placeholder='Expense Amount']"));
    this.chooseDate = element(by.xpath("//input[@formcontrolname='createdAt']"));
    this.TodaysDate = element(by.xpath("//div[@class='mat-calendar-body-cell-content mat-calendar-body-today']"));
    this.saveExpenses = element(by.id('save-btn'));
    this.downloadExpenses = element(by.xpath("//button[@id='prnt-btn']//span[1]"));
    //edit expenses 
    this.editOption = element(by.id('menu-btn-0'));
    this.editBtn = element(by.id('edit-btn-0'));
    //Details
    this.details = element(by.id('details'));
    this.metaData = element(by.id('meta-data'));
    this.closeMetaData = element(by.xpath("//span[contains(text(),'close')]"));
    this.printDetails = element(by.xpath("(//button[@id='prnt-btn']//span)[2]"));
    this.dropbox_Flies = element(by.id('DROPBOX'));
    this.financialDetails = element(by.xpath("//div[text()=' FINANCIALS ']"));
}
module.exports = new GCM02PO();