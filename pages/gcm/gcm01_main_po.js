var GCM01PO = function () {

    //Click on NEW GROUP
    this.newGroup = element(by.xpath("//span[@class='hide-xs uppc']"));
    //Group Name
    this.groupName = element(by.xpath("//input[@name='groupName']"));
    this.groupTagContacts = element(by.xpath("//input[@placeholder='Tag Contacts  ']"));
    this.firstOption = element(by.xpath("(//span[@class='mat-option-text'])[1]"));
    this.placeType = element(by.id('place-type'));
    this.address = element(by.id('address'));
    //Save Group
    this.saveBtn = element(by.xpath("//span[contains(text(),'SAVE')]"));
    this.CancelBtn = element(by.id('cancel-btn'));
    //Edit Group
    this.globalSearchMouseMoveFirst = function () {
        browser.actions().
        mouseMove(element(by.xpath("(//div[@class='cdk-virtual-scroll-content-wrapper']/div)[1]"))).perform();
    }
    this.mouseMoveFirst = function () {
        browser.actions().
        mouseMove(element(by.xpath("//div[@id='group-name-0']"))).perform();
    }
    this.mouseMoveSecond = function () {
        browser.actions().
        mouseMove(element(by.xpath("//div[@id='group-name-1']"))).perform();
    }
    this.editGroup = element(by.xpath("(//i[@class='fa fa-pencil'])[1]"));
    this.editGlobalGroup = element(by.xpath("(//i[@id='edit-group-0'])[2]"));
    this.editBlankGroup = element(by.xpath("(//i[@name='edit-group'])[1]"));
    //Create New Client
    this.createClient_Msg = element(by.xpath("//span[contains(text(),'CREATE')]")); 
    this.newClientBtn = element(by.xpath("//button[@id='new-client-0']"));
    this.createSecondClient = element(by.xpath("//button[@id='new-client-1']"));
    this.clientName = element(by.name('clientName'));
    this.panCard = element(by.name('panNumber'));
    this.clientTagContacts = element(by.xpath("//input[@placeholder='Tag Contacts ']"));
    //Edit Client
    this.firstClient = element(by.xpath("(//span[@name='client-name'])[1]"));
    this.editClient = element(by.id('client-edit'));
    this.secondGroupClient = element(by.xpath("(//span[@id='client-name-0'])[2]"));
    //Search Matter Value in Side Client 
    this.searchMatterValue = element(by.id('search-value'));
    //New Matter 
    this.newMatterBtn = element(by.id('addl3'));
    this.matterName = element(by.name('L3Name'));
    //Select Member
    this.selectMember = element(by.xpath("//input[@placeholder='Select Member']"));
    //Template 
    this.templateOption = element(by.xpath("//strong[contains(text(),'Template')]"));
    this.categoryName = element(by.xpath("//mat-select[@name='catergoryName']"));
    this.templateName = element(by.xpath("//mat-select[@name='templateName']"));
    //fees
    this.fees = element(by.xpath("//strong[contains(text(),'Fees')]"));
    this.quotedfees = element(by.xpath("//input[@type='number']"));
    //Contacts
    this.contacts = element(by.xpath("//strong[contains(text(),'Contacts')]"));
    this.tagContacts = element(by.xpath("//input[@placeholder='Tag Contacts  ']"));
    //Edit Matter
    this.firstMatter = element(by.xpath("(//p[@class='matter-name'])[1]"));
    this.editMatter = element(by.xpath("(//span[@class='mat-button-wrapper'])[1]"));
    this.removeMember = element(by.id("remove-member-0"));
    //archive Matter 
    this.archiveBtn = element(by.xpath("//span[contains(text(),'Archive')]"));
    this.confirmBtn = element(by.xpath("//span[contains(text(),'confirm')]"));
    this.closedBtn = element(by.xpath("//button[@id='closed']"));
    this.unarchiveBtn = element(by.xpath("//span[contains(text(),'Unarchive')]"));
    
    
    //Icon Create GCM
    this.gcm_plusIcon = element(by.xpath("//i[@class='fa fa-plus']"));
    this.iconGroupDropDown = element(by.id('add-global-l1'));
    this.iconClientDropDown = element(by.id('add-global-l2'));
    this.iconMatterDropDown = element(by.id('add-global-l3'));
    this.associateIconMatterDropDown = element(by.xpath("(//button[@role='menuitem'])[1]"));

    this.matSelectGroup = element(by.id('mat-select-L1Name'));
    this.secondOption = element(by.xpath("(//span[@class='mat-option-text'])[2]"));
    this.thirdOption = element(by.xpath("(//span[@class='mat-option-text'])[3]"));
    this.matSelectClient = element(by.id('mat-select-L2Name'));

    this.searchDropdown = element(by.xpath("//input[contains(@class,'mat-select-search-input mat-input-element')]"));

    this.viewMatter = element(by.xpath("//span[text()='View']"));



}
module.exports = new GCM01PO();