var GCM05PO = function(){

    this.memberAssignIcon = element(by.id('other-employee-details'));
    this.searchMember = element(by.xpath("//input[@placeholder='Search']"));
    this.selectMember = element(by.xpath("(//span[@class='mat-option-text'])[1]"));
    this.secondAssociate = element(by.id('activeAssociate-1'));
    this.activeAssociate = element.all(by.xpath("//button[@name='activeAssociate']"));

    this.matterAnalytics = element(by.id('analytics'));
    this.analyticsData = element(by.id('analytics-div'));

    this.notificationIcon = element(by.id('notification-button'));
    this.rightNotification = element(by.xpath("//i[@class='fa fa-angle-right']"));


}
module.exports = new GCM05PO();