var TIMESHEETPO03 = function () {

    //SearchbyGCM 
    let classifyBtn = element(by.id('goto-classify'));
    this.searchByGCM = element(by.id('search-by-gcm'));

    this.classifyBtn = function () {
        classifyBtn.click();
    }
    this.classifyBtnEnable = async function () {
        return await element(by.id('goto-classify')).isPresent();
    }

    this.verifyClassifiedTime = async function () {
        this.confirmText = element(by.xpath("//div[@class='confirmation-message text-left']")).getText();
        confirmText = confirmText.split("classify ")[1];
        this.hoursString = confirmText.split('h')[0];
        confirmText=confirmText.split('h')[1];
        this.minutesString = confirmText.split('m')[0];
        this.secondsString = confirmText.split('m')[1];
        secondsString = secondsString.split('s')[0];
        this.HoursInt = parseInt(hoursString, 10);
        this.MinutesInt = parseInt(minutesString, 10);
        this.SecondsInt = parseInt(secondsString, 10);

        this.totalSeconds = HoursInt*60+MinutesInt*60+SecondsInt;

        return totalSeconds;
    }


    this.firstCheckbox = element(by.xpath("//tr[1]/td[1]"));
    this.secondCheckbox = element(by.xpath("//tr[2]/td[1]"));
    this.thirdCheckbox = element(by.xpath("//tr[3]/td[1]"));
    this.fourthCheckbox = element(by.xpath("//tr[4]/td[1]"));
    this.fifthCheckbox = element(by.xpath("//tr[5]/td[1]"));

    this.classifyDone = element(by.xpath("//mat-icon[text()='done']"));
    this.confirmRecords = element(by.id('validate-btn'));

    //Filter Timesheet
    this.durationFilter = element(by.css('div.duration-menu-title'));
    this.filterByGreaterThan = function () {
        browser.actions()
            .mouseMove(element(by.xpath("//span[@mattooltip='Greater Than']")))
            .perform();
    }
    this.filterLessThan = function () {
        browser.actions()
            .mouseMove(element(by.xpath("//span[@mattooltip='Less Than']")))
            .perform();
    }
    this.filterInBetween = function () {
        browser.actions()
            .mouseMove(element(by.xpath("//span[@mattooltip='In Between']")))
            .perform();
    }


    //SearchbyGCM 
    this.searchByGCM = element(by.id('search-by-gcm'));

    this.firstCheckbox = element(by.xpath("//tr[1]/td[1]"));
    this.secondCheckbox = element(by.xpath("//tr[2]/td[1]"));
    this.thirdCheckbox = element(by.xpath("//tr[3]/td[1]"));
    this.fourthCheckbox = element(by.xpath("//tr[4]/td[1]"));
    this.fifthCheckbox = element(by.xpath("//tr[5]/td[1]"));
    this.filterInput = element(by.xpath("//div[@class='display-flex filter-main-box']/input"));


    this.classifyDone = element(by.xpath("//mat-icon[text()='done']"));
    this.confirmRecords = element(by.id('validate-btn'));

    //View All 
    this.viewAll = element(by.xpath("//mat-select-trigger[text()=' View All  ']"));
    this.firstDropdown = element(by.xpath("(//span[@class='mat-option-text'])[1]"));


}
module.exports = new TIMESHEETPO03();