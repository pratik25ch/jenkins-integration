var Timesheet02PO = function () {

    let firstEntry = element(by.id('list-1'));
    let manualEntryDescription = element(by.name('description'));
    
    this.gcmName = function () {
        firstEntry.getText();
    }
    this.stopwatchTimer = element(by.xpath("//div[@class='mat-slide-toggle-thumb']"));
    this.startTimer = element(by.xpath("//mat-icon[text()='play_arrow']"));
    this.stopTimer = element(by.xpath("//mat-icon[text()='stop']"));
    this.manualEntryType = element(by.xpath("//input[@placeholder='Type']"));
    this.firstDropdownEntry = element(by.xpath("(//span[@class='mat-option-text'])[1]"));

    this.mouseMoveTimesheet = function(){
        browser.actions().
        mouseMove(element(by.xpath("//body//cdk-virtual-scroll-viewport[1]"))).perform();
    }

    this.removeTimer = element(by.xpath("//mat-icon[text()='remove_circle_outline']"));
    this.refreshTimer = element(by.xpath("//mat-icon[contains(text(),'refresh')]"));

    this.manualEntryDescription =function(){
        manualEntryDescription.sendKeys("The submission of payment information for a transaction by keying the customer's payment information into a hardware terminal or Virtual Terminal. For example, any transaction that is not submit")
    }
    this.mouseMoveLastTimesheet = function () {
        browser.actions()
            .mouseMove(element(by.xpath("(//div[@class='matter-details'])[last()]")))
            .perform();
    }




}
module.exports = new Timesheet02PO();