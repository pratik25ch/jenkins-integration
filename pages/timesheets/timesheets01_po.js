var CommPage = require("../../lib/CommLib/comm_lib");
var TIMESHEETPO01 = function () {

        let manualEntryButton = element(by.id('manual-entry'));
        this.unclassifiedButton = element(by.id('unclassify'));
        
        
        this.timesheetView = async function () {
                await element(by.xpath("//div[@class='detailed-view']")).getText().then(function (text) {
                        console.log(text);
                })
        }
        this.userSelect = element(by.xpath("(//mat-select[@placeholder='Select Associate'])[2]"));
        this.userInput = element(by.xpath("//input[@placeholder='search for user...']"));
        this.firstOption = element(by.xpath("(//span[@class='mat-option-text'])[1]"));

        this.timesheetCalender = element(by.xpath("(//input[@placeholder='Select date'])[2]"));
        this.todaysDate = element(by.xpath("//div[contains(@class,'mat-calendar-body-cell-content mat-calendar-body-selected')]"));
        this.backwordDate = element(by.id('backward2-date'));
        this.forwordDate = element(by.id('forward2-date'));
        //Manual Entry
        this.manualEntryBtn = function () {
                manualEntryButton.click();
        }
        this.associateName = element(by.name('user-name'));

        this.selectMatter = element(by.name('select-l3'));
        this.searchMatter = element(by.xpath("//input[contains(@class,'mat-select-search-input mat-input-element')]"));
        this.firstEntry = element(by.xpath("(//span[@class='mat-option-text'])[2]"));

        this.manualEntryName = element(by.name('entry-name'));
        this.validateBtn = element(by.id('validate-btn'));
        this.cancelBtn = element(by.id('cancel-btn'));

        //Filter
        this.filter = element(by.xpath("//div[@class='filter cursor-pointer']"));
        this.byType = element(by.id('type-list-btn'));
        this.manualEntry = element(by.xpath("//span[text()=' Manual Entry ']"));
        this.pluginIdle = element(by.xpath("//div[text()=' Plugin Idle ']"));
        this.plugIn = element(by.id("plugin-list-menu"));
        this.classified = element(by.id('plugin-classified'));
        this.unclassified = element(by.id('plugin-unclassified'));
        this.byApplication = element(by.id('application-list-btn'));

        let firstApplication = element(by.xpath("(//div[@id='application-type-0'])"));
        let secondApplication = element(by.xpath("(//div[@id='application-type-1'])"));
        let thirdApplication = element(by.xpath("(//div[@id='application-type-2'])"));
        let fourthApplication = element(by.xpath("(//div[@id='application-type-3'])"));
        this.selectAllCheckboxes = element(by.xpath("//th[@class='checkbox-header mat-header-cell cdk-column-select mat-column-select ng-star-inserted']/mat-checkbox/label"));


        let allApp = element(by.xpath("//div[@id='application-type-0']"));
        this.allAppFilter = function () {
                allApp.isPresent().then(function (T) {
                        if (T) {
                                firstApplication.click();
                                secondApplication.click();
                                thirdApplication.click();
                                fourthApplication.click();
                        } else {
                                CommPage.escBtn();
                        }
                })
        }
        this.applicationList = function () {
                firstApplication.click();
                secondApplication.click();
                thirdApplication.click();
                fourthApplication.click();
        }

        let pluginIdleEntries = element(by.xpath("//span[@class='ng-star-inserted'][contains(text(),'Plugin Idle')]"));
        this.pluginIdleFilter = function () {
                pluginIdleEntries.isPresent().then(function (idle) {
                        if (idle) {
                                pluginIdleEntries.click();
                                console.log("Plugin Idle Entries Present")
                                CommPage.sleep(2);
                                CommPage.tabBtn();
                        } else {
                                CommPage.tabBtn();
                                console.log("No Plugin Idle Entries")
                        }
                })
        }

        let pluginList = element(by.xpath("//button[@id='plugin-list-menu']"));
        let pluginClassified = element(by.id('plugin-classified'));
        let pluginUnclassified = element(by.id('plugin-unclassified'));
        this.pluginFilter = function () {
                pluginList.isPresent().then(function (C) {
                        if (C) {
                                pluginList.click();
                                CommPage.sleep(1)
                                pluginUnclassified.click();
                                CommPage.sleep(2);
                                pluginClassified.click();
                                CommPage.sleep(2);
                                CommPage.tabBtn();
                        } else{
                                CommPage.tabBtn();
                                console.log("No Plugin Entries")
                        }
                })
        }

        let removeicon = element(by.xpath("(//i[contains(@class,'fa fa-times-circle')])[1]"));
        this.removeIcon = function(){
                removeicon.isPresent().then(function(I){
                        if(I){
                                removeicon.click();
                                console.log('Removed')
                        }else{
                                console.log('No remove icon')
                        }
                })
        }

        this.removeFilterIcon = element(by.xpath("(//i[contains(@class,'fa fa-times-circle')])[1]"));
        this.byTime = element(by.id('time-list-btn'));

        //SearchbyGCM 
        this.classify = element(by.id('goto-classify'));
        this.searchByGCM = element(by.id('search-by-gcm'));

        this.firstCheckbox = element(by.xpath("//tr[1]/td[1]"));
        this.secondCheckbox = element(by.xpath("//tr[2]/td[1]"));
        this.thirdCheckbox = element(by.xpath("//tr[3]/td[1]"));
        this.fourthCheckbox = element(by.xpath("//tr[4]/td[1]"));
        this.fifthCheckbox = element(by.xpath("//tr[5]/td[1]"));

        this.classifyDone = element(by.xpath("//mat-icon[text()='done']"));
        this.confirmRecords = element(by.id('validate-btn'));

        //View All 
        this.viewAll = element(by.xpath("//mat-select-trigger[text()=' View All  ']"));
        this.firstDropdown = element(by.xpath("(//span[@class='mat-option-text'])[1]"));

        //Previous Date Manual Entry
        this.PreviousDate = element(by.id('backward2-date'));

        //Timesheet Comparision
        this.timesheetUnclassifiedTime = async function () {
                return await element(by.xpath("//td[text()='Unclassified Entry']/following-sibling::td[1]")).getText();
        }
        this.timesheetClassifiedTime = async function () {
                return await element(by.xpath("//td[text()='Classified Entry']/following-sibling::td[1]")).getText();
        }

        //Repeat Entry
        this.startTime = element(by.name('start-time'));
        this.endTime = element(by.name('end-time'));
        this.secondEntry = element(by.xpath("(//span[@class='mat-option-text'])[3]"));
        this.thirdEntry = element(by.xpath("(//span[@class='mat-option-text'])[4]"));
        this.fourthEntry = element(by.xpath("(//span[@class='mat-option-text'])[5]"));
        this.fifthEntry = element(by.xpath("(//span[@class='mat-option-text'])[6]"));
}
module.exports = new TIMESHEETPO01();