var CommPage = require("../../lib/CommLib/comm_lib");
var TIMESHEETPO04 = function () {

    let tooltip = element(by.xpath("//button[@mattooltip='DELETE']"));
    this.timesheetTooltip = function () {
        tooltip.isPresent().then(function (T) {
            if (T) {
                tooltip.click()
            } else {
                console.log("No Timesheet")
            }
        })
    }

    this.deleteTimesheetBtn= element(by.xpath("//span[contains(text(),'Delete Unclassified Entries')]"));
    this.cancelDeleteTimesheet= element(by.id('cancel-delete-timesheet-btn'));
    this.firstCheckbox = element(by.xpath("(//label[@class='mat-checkbox-layout'])[1]"));
    this.secondCheckbox = element(by.xpath("(//label[@class='mat-checkbox-layout'])[2]"));
    this.firstCheckboxSelected = element(by.xpath("//mat-checkbox[@class='mat-checkbox mat-accent ng-valid ng-star-inserted ng-dirty ng-touched checked-for-deletion mat-checkbox-checked']"));




}
module.exports = new TIMESHEETPO04();