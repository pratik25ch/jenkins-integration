var EXPENSE02PO = function () {

    let details = element(by.xpath("(//div[@class='mat-tab-label-content'])[2]"));
    let newExpenseBtn = element(by.id('add-expense'));

    this.details_page = element(by.xpath("//div[@class='table-container']"));
    this.matterName = element(by.xpath("(//input[@role='combobox'])[2]"));
    this.firstOption = element(by.xpath("(//span[@class='mat-option-text'])[1]"));
    this.secondOption = element(by.xpath("(//span[@class='mat-option-text'])[2]"));
    this.thirdOption = element(by.xpath("(//span[@class='mat-option-text'])[3]"));
    this.memberName = element(by.xpath("//input[@placeholder='Member']"));
    this.expenseType = element(by.xpath("//input[@placeholder='Expense Type']"));
    this.expenseAmount = element(by.xpath("//input[@placeholder='Expense Amount']"));
    this.calender = element(by.xpath("//button[@class='mat-icon-button']"));
    this.todaysDate = element(by.xpath("//div[@class='mat-calendar-body-cell-content mat-calendar-body-today']"));
    this.saveBtn = element(by.xpath("//span[@id='save-btn-text']"));

    this.editDots = element(by.id('menu-btn-0'));
    this.editBtn = element(by.id('edit-btn-0'));
    this.nextPage = element(by.xpath("//button[@aria-label='Next page']"));
    this.previousPage = element(by.xpath("//button[@aria-label='Previous page']"));
    this.lastPage = element(by.xpath("//button[@aria-label='Last page']"));
    this.firstPage = element(by.xpath("//button[@aria-label='First page']"));

    this.dateHeader = element(by.id('date-col'));
    this.matterHeader = element(by.id('Matter-col'));
    this.nameHeader = element(by.id('name-col'));
    this.typeHeader = element(by.id('type-col'));
    this.amountHeader = element(by.id('amount-col'));






    
    this.details_Btn = function () {
        details.click();
        console.log('Table Opened Successfully')
    }
    this.newExpenseBtn = function () {
        newExpenseBtn.click();
    }

}
module.exports = new EXPENSE02PO();