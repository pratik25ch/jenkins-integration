var BILLINGPO = function () {

    this.hideZeroAmount = element(by.xpath("//div[@class='mat-checkbox-inner-container']"));
    this.filterBtn = element(by.xpath("//div[@class='filter-data']//div[1]"));
    this.closedMatter = element(by.id('closed-filter-btn'));
    this.activeMatter = element(by.id('active-filter-btn'));
    this.billingsSearchMatter = element(by.id('search-value'));

    //Create Invoice
    this.newInvoice = element(by.id('add-invoice'));
    this.selectMatter_CreateInvoice = element(by.xpath("(//div[@class='mat-select-arrow-wrapper'])[2]"));
    this.searchMatter_CreateInvoice = element(by.xpath("//input[contains(@class,'mat-select-search-input mat-input-element')]"));
    this.firstOption_CreateInvoice = element(by.id('L3-0'));
    this.billing_endDate = element(by.xpath("//mat-icon[@class='mat-icon material-icons keyboard-down mat-icon-no-color']"));
    this.billing_endMin = element(by.xpath("//owl-date-time-timer-box[2]//button[2]//span[1]"));
    this.billing_setTime = element(by.xpath("//span[contains(text(),'Set')]"));
    
    //Generating Invoice
    this.invoiceId = element(by.name('invoiceId'));
    this.perticularTitle = element(by.id('particular-title'));
    this.particularCost= element(by.id('particular-cost'));
    this.addCost= element(by.xpath("//span[@class='mat-button-wrapper'][contains(text(),'Add')]"));
    this.firstMatter_Outstanding = element(by.id('outstanding-matter-0'));
    this.generatingInvoice = element(by.xpath("(//span[text()=' generate invoice'])[1]"));
    this.outOfPocketExpenses = element(by.xpath("//input[@name='outOfPocketExpenses']"));
    this.professionalFee = element(by.name('ProfessionalFee'));
    this.createInvoice = element(by.xpath("//span[text()=' create ']"));



    this.outstandingAmount = async function () {
        return await element(by.xpath("//span[@class='g-invoice']/following-sibling::span[1]")).getText();
    }





    //UpdateInvoice
    this.firstMatter_UpdateInvoice = element(by.xpath("//tr[1]//td[2]"));
    this.editValue = element(by.id('term-edit-1'));
    this.editProfessionalFees = element(by.id('term-val-1'));
    this.editPocketExpences = element(by.id('term-val-2'));
    this.updateInvoice = element(by.id('submit-btn'));

    this.amount = element(by.name('paymentAmount'));
    this.proof = element(by.name('paymentProof'));
    this.addAmount = element(by.xpath("//a[contains(@class,'btn-wht cb')]"));
    this.markAsCompleted = element(by.id('mark-as-completed'));
    this.downloadInvoice = element(by.xpath("(//i[@mattooltip='Download Invoice'])[1]"));

    //Completed Invoice
    this.completed = element(by.id('mat-tab-label-0-2'));
    this.closeInvoice = element(by.xpath("//span[contains(text(),'close')]"));
    this.customizeSetting = element(by.xpath("//mat-icon[text()='settings ']"));

    this.creationDate = element(by.xpath("//span[contains(text(),'Creation Date')]"));
    this.receivedAmount = element(by.xpath("//span[contains(text(),'Received Amount')]"));
    this.margin = element(by.xpath("(//span[contains(text(),'Margin')])[2]"));
    this.download = element(by.xpath("//span[contains(text(),'Download')]"));





















}
module.exports = new BILLINGPO();