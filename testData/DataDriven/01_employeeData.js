module.exports = {
    employee: {
        NewEmployee: {
            firstName: "Demo",
            lastName: "Automation",
            yearlySal: 100000,
            fullDays: 300,
            halfDays: 50,
            fullHours: 11,
            halfHours: 8,
            chargeOutRate: 12.3,
            contactNumber: 9090909090,
            reportsTo: "Debasish Patra",
        },
        EditEmployee: {
            firstName: "Z Edit",
            lastName: "Employee",
            yearlySal: 200000,
            fullDays: 280,
            halfDays: 45,
            fullHours: 10,
            halfHours: 7,
            chargeOutRate: 16.3,
            historicalChargeOutRate: 100,
            contactNumber: 7070707070,
            reportsTo: "soham",
        },
        NewL3:{
            matterName:"-Icon Matter-",
            selectFirstMember:"Debasish",
            selectSecondMember: "QA One"
        }


    }
}