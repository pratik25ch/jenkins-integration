var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
require('dotenv').config();
exports.config = {

    // seleniumAddress: 'http://localhost:4444/wd/hub',
    directConnect: true,
    restartBrowserBetweenTests: false,
    framework: 'jasmine',
    useAllAngular2AppRoots: true,

    'jasmineNodeOpts': {
        showColors: true,
        defaultTimeoutInterval: 5000000,
        isVerbose: true
    },
    specs: [
    
        // 'components/flowace_website.js',
        // 'components/flowaceStatus/flowace_status_spec.js',
        // 'specs/login/login01_invalid_spec.js',

        // 'specs/gcm/gcm01_main_spec.js',
        // 'specs/gcm/gcm02_task_spec.js',
        // 'specs/gcm/gcm03_search_spec.js',
        // 'specs/gcm/gcm05_assignAnalytics_spec.js',
        // 'specs/gcm/gcm04_delete_spec.js',

        // 'specs/gcm/gcm06_globalSearch_spec.js',

        //// 'pages/PratikWork/analytics02_financialAnalytics_spec.js'
        //// 'pages/PratikWork/AddMemberAndCheckAnalytics.js'


        //// Permissions module
        // 'specs/administration/permissions/manager/permissions_manager_specs.js',
        // 'specs/administration/permissions/admin/permissions_Admin_specs.js',
        // 'specs/administration/permissions/Associate/permissions_Associate_specs.js'

        ////Historical Chargeout rate
        'specs/administration/employees/employees04_chargeoutrate_spec.js'


        //// 'specs/pratik/timesheetAuto.js'

        ////     'specs/administration/permissions/*/*.js'
        ////     'pages/PratikWork/AddEmployee.js'

        // 'specs/gcm/gcm07_deleteImpact_spec.js',
        // 'specs/gcm/gcm01_main_spec.js',
        // 'specs/gcm/gcm02_task_spec.js',
        // 'specs/gcm/gcm03_search_spec.js',
        // 'specs/gcm/gcm05_assignAnalytics_spec.js',
        // 'specs/gcm/gcm04_delete_spec.js',


        // 'specs/timesheets/timesheets01_spec.js',
        // 'specs/timesheets/timesheets02_manualEntry_spec.js',
        // 'specs/timesheets/timesheets03_classify_spec.js',
        // 'specs/timesheets/timesheets04_delete_spec.js',
        // 'specs/timesheets/timesheets05_override_spec.js',

        // 'specs/billing/billing_main_spec.js',
        // 'specs/billing/billing_previousDate_spec.js',
        // 'specs/billing/billing_timesheet_spec.js',

        // 'specs/analytics/analytics01_matterAnalytics_spec.js',
        // 'specs/analytics/analytics03_memberAnalytics_spec.js',
        // 'specs/analytics/analytics04_timesheetAnalytics_spec.js',
        // 'specs/analytics/analytics02_financialAnalytics_spec.js',

        // 'specs/expense/expense01_summary_spec.js',
        // 'specs/expense/expense02_details_spec.js',

        // 'specs/locationServices/location_spec.js',

        // 'specs/places/places01_viewer_spec.js',
        // 'specs/places/places02_address_spec.js',
        // 'specs/administration/employees/employees01_spec.js',
        // 'specs/administration/employees/employees02_assign_spec.js',
        // 'specs/administration/employees/employees03_role_spec.js',
        // 'specs/administration/templates/templates01_spec.js',
        // 'specs/administration/templates/templates02_applyToAll_spec.js',
        // 'specs/administration/templates/templates03_defaultTemp_spec.js',
        // 'specs/administration/contacts/contacts01_spec.js',
        // 'specs/administration/contacts/contacts02_userDetails_spec.js',
        // 'specs/administration/contacts/contacts03_tag_spec.js',
        // 'specs/administration/settings/settings01_emp_spec.js',
        // 'specs/administration/settings/settings02_website_spec.js',
        // 'specs/administration/settings/settings03_app_spec.js',
        // 'specs/administration/settings/settings04_integration_spec.js',
        // 'specs/administration/settings/settings05_configuration_spec.js',

    ],

    suites: {
        gcm: ['./specs/gcm/*_spec.js'],
        analytics: ['./specs/analytics/*_spec.js'],
        expense: ['./specs/expense/*_spec.js'],
        places: ['./specs/places/*_spec.js'],
        employees: ['./specs/administration/employees/*_spec.js'],
        templates: ['./specs/administration/templates/*_spec.js'],
        contacts: ['./specs/administration/contacts/*_spec.js'],
        settings: ['./specs/administration/settings/*_spec.js']
        // administration: [
        //     './specs/administration/employees/*_spec.js',
        //     './specs/administration/templates/*_spec.js',
        //     './specs/administration/contacts/*_spec.js',
        //     './specs/administration/settings/*_spec.js'
        // ]
    },
    /*
        multiCapabilities: [{
                browserName: 'firefox'
            },
            {
                browserName: 'chrome'
            }
        ],
    */
    capabilities: {
        'browserName': 'chrome',
        'chromeOptions': {
            // 'args': ['--headless', '--disable-gpu', '--no-sandbox', '--window-size=1920x1200'],
            'args': ['disable-infobars'],
 binary: 'C:/Users/Pratik/AppData/Local/Google/Chrome/Application/chrome.exe',
            args: [],
            extensions: []
        },
    },

    // suites: {
    //     smoke: ['./smoke/*.spec.js'],
    //     regression: ['./regression/*.spec.js'],
    //     functional: ['./functional/*.spec.js'],
    //     all: ['./*/*.spec.js'],
    //     selected: ['./functional/addcustomer.spec.js', './regression/openaccount.spec.js'],

    //     RegressionTesting: [
    //         'specs/settings/settings_spec.js',
    //         'specs/contacts/contacts_spec.js'
    //     ],

    //     prodSanityTesting: [
    //         'specs/gcm/gcm_spec.js',
    //         'specs/timesheets/timesheets_spec.js',
    //         'specs/analytics/analytics_spec.js',
    //         'specs/employees/employees_spec.js',
    //         'specs/templates/templates_spec.js',
    //         'specs/contacts/contacts_spec.js',
    //         'specs/settings/settings_spec.js'
    //     ]
    // },

    onPrepare: function () {
        browser.ignoreSynchronization = true;
        browser.waitForAngularEnabled(false);
        browser
            .manage()
            .window()
            .maximize();
        browser
            .manage()
            .timeouts()
            .implicitlyWait(25000);
        browser.get('https://demo.flowace.in');

        jasmine.getEnv().addReporter(
            new Jasmine2HtmlReporter({
                savePath: 'testReport/screenshots'
            }));
    },

    onComplete: function () {
        // var sys = require('util')
        // var exec = require('child_process').exec;

        // function puts(error, stdout, stderr) {
        //     sys.puts(stdout)
        // }
        // exec("node temp_mail.js", puts);
    },

}