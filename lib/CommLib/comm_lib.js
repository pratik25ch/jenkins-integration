var LoginData = require("../../utils/jsonUtils/login.json");

var ComLib = function () {

    this.flowaceWebsiteURL = function () {
        browser.get("https://flowace.in/");
    };
    this.back = function () {
        browser.navigate().back()
    }
    this.close = function () {
        browser.close();
    }
    this.refresh = function () {
        browser.navigate().refresh();
    }
    this.escBtn = function () {
        browser.actions()
            .sendKeys(protractor.Key.ESCAPE)
            .perform();
    }
    this.tabBtn = function () {
        browser.actions()
            .sendKeys(protractor.Key.TAB)
            .perform();
    }
    this.downArrow = function () {
        browser.actions()
            .sendKeys(protractor.Key.ARROW_DOWN)
            .perform();
    }
    this.enterBtn = function () {
        browser.actions()
            .sendKeys(protractor.Key.ENTER)
            .perform();
    }
    this.endBtn = function () {
        browser.actions()
            .sendKeys(protractor.Key.END)
            .perform();
    }
    this.scrollDown = function () {
        browser.actions()
            .sendKeys(protractor.Key.PAGE_DOWN)
            .perform();
    }
    this.sleep = function (sec) {
        browser.sleep(sec * 1000);
    }

    this.MaximiseWindow = function () {
        browser
            .manage()
            .window()
            .maximize();
    }

    this.childWindow = function () {
        browser.getAllWindowHandles().then(function (handles) {
            browser.switchTo().window(handles[1]);
        })
    }
    this.parentWindow = function () {
        browser.getAllWindowHandles().then(function (handles) {
            // We currently are on the second tab...
            browser.driver.close();
            browser.switchTo().window(handles[0]);
        })
    }
    this.scrollDown1 = function () {
        browser.executeScript('window.scrollTo(0,10000);').then(function () {
            console.log('++++++SCROLLED Down+++++');
        });
    }

    this.sliderDragDrop = function () {
        browser.actions().dragAndDrop(
            element(by.id('slider-range')),
            {x:100, y:0}
        ).perform();
      }

}
module.exports = new ComLib();