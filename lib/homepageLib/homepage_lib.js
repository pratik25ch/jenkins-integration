var HomePagePO = function () {

    let associateMatter = element(by.id('My-Matters'));
    let dashBoard = element(by.id('dashboard'));
    let Matter = element(by.id('matters'));
    let Timesheet = element(by.id('Timesheets'));
    let Billing = element(by.id('Billing'));
    let Reporting = element(by.id('Reporting'));
    let AnalyticsLabel = element(by.id('Analytics'));
    let Expance = element(by.id('Expense'));
    let Places = element(by.id('Places'));
    let Administration = element(by.id('administration-section-expand'));
    let Employee = element(by.id('employee-view'));
    let LocationService = element(by.xpath("//div[text()='Location Service']"));
    let Template = element(by.id('template-view'));
    let Contacts = element(by.xpath("(//a[@id='contact-view'])[1]"));
    let Settings = element(by.id('settings-view'));
   let Permissions= element(by.xpath("//div[text()='Permissions']"));

   this.verifyAnalyticsLabelNotPresent = function () {
    expect(element(by.xpath("//div[text()='Permissions']")).isDisplayed()).toBe(true);
}

this.verifyPermissionsOptionNotPresent = function () {
    expect(Permissions.isDisplayed()).toBe(false);
}

    this.associateMatter = function () {
        associateMatter.click();
    }
    this.Matter = function () {
        Matter.click();
    }
    this.DashBoard = function () {
        dashBoard.click();
    }
    this.Timesheet = function () {
        Timesheet.click();
    }
    this.Billing = function () {
        Billing.click();
    }
    this.Reporting = function () {
        Reporting.click();
    }
    this.Analytics = function () {
        AnalyticsLabel.click();
    }
    this.Expances = function () {
        Expance.click();
    }
    this.Places = function () {
        Places.click();
    }
    this.Administration = function () {
        Administration.click();
    }
    this.Employee = function () {
        Employee.click();
    }
    this.LocationService = function () {
        LocationService.click();
    }
    this.Template = function () {
        Template.click();
    }
    this.Contacts = function () {
        Contacts.click();
    }
    this.Settings = function () {
        Settings.click();
    }
    this.Permissions = function () {
        Permissions.click();
    }




}
module.exports = new HomePagePO();