var verifyPO = function () {

    //Status Flowace
    this.flowace_statuspage = element(by.xpath("//div[@class='container']"));
    //Login
    this.invalidLigin_msg = element(by.xpath("//span[contains(text(),'Login Failed')]"));
    //GCM==========>
    this.gcmPage_ContentDisplay = element(by.xpath("//div[@class='layout-content']"));
    this.firstGroup_verify = element(by.id('group-name-0'));
    this.newGroup_msg = element(by.xpath("//span[contains(text(),'created Successfully. Click create button')]"));
    this.tagContactVerify = element(by.css('mat-option.mat-option.ng-star-inserted'));
    //element(by.xpath("(//span[contains(text(),'Debasish Patra')])[1]"));
    this.duplicateGroup_msg = element(by.xpath("//span[@class='example-pizza-party']"));
    this.saveBtn_disabled = element(by.xpath("//button[@class='mat-button f-rt btn-wht cb']"));
    this.updateGroup_msg = element(by.xpath("//span[contains(text(),'updated Successfully')]"));

    this.openPanel_verify = element(by.css('mat-dialog-content.mg-tp.mg-bt.mat-dialog-content'));
    this.newClient_msg = element(by.xpath("//span[contains(text(),'created Successfully')]"));
    this.duplicateClient_msg = element(by.xpath("//span[contains(text(),'creation failed due to')]"));
    this.updateClient_msg = element(by.xpath("//span[contains(text(),' updated Successfully')]"));

    this.newMatter_msg = element(by.xpath("//span[contains(text(),' created Successfully')]"));
    this.inside_Matter = element(by.xpath("//div[@class='box-shadow']"));
    this.updateMatter_msg = element(by.xpath("//span[contains(text(),' updated Successfully')]"));
    this.noActiveMatter_msg = element(by.xpath("//span[text()='No active matters ']"));

    this.timeVsTask_verify = element(by.css('div.task-detail-box'));
    this.firstTask_verify = element(by.xpath("(//div[@name='task-matterTask'])[1]/span[@class='t-txt-marg']"));
    this.secondTask_verify = element(by.xpath("(//div[@name='task-matterTask'])[2]/span[@class='t-txt-marg']"));
    this.taskDesc_verify = element(by.xpath("//small[contains(text(),'Description added')]"));
    this.taskComment_verify = element(by.xpath("//div[@class='talktext']"));
    this.deleteComment_verify = element(by.xpath("//div[contains(text(),'No comments')]"));
    this.emptyMatterExpenses_verify = element(by.css('div.no-data'));
    this.newExpenses_msg = element(by.xpath("//span[contains(text(),'Successfully Created')]"));
    this.updateExpenses_msg = element(by.xpath("//span[contains(text(),'Successfully Updated')]"));
    this.matterDetails_verify = element(by.id('details-div'));

    this.deleteMatter_msg = element(by.xpath("//span[contains(text(),' successfully deleted')]"));
    this.deleteClient_msg = element(by.xpath("//span[contains(text(),' Deleted Successfully')]"));
    this.deleteGroup_msg = element(by.xpath("//div[contains(text(),' Deleted Successfully')]"));
    
    this.displayAllMatter_verify = element(by.xpath("//div[@class='matter-result']"));

    this.globalSearch_verify = element(by.css('div.list.keyword-block'));
    this.advanceSearchResult_verify = element(by.css('ul.list'));
    this.advanceSearchTab_verify = element(by.css('form.ng-touched.ng-pristine.ng-valid'));


    //TIMESHEET===========>
    this.timesheetlogs_verify = element(by.xpath("(//div[@class='detailed-view-row'])[1]"));
    this.timesheetPage_verify = element(by.xpath("//div[@class='timesheet-view']"));
    this.manualEntryPage_verify = element(by.css("form.ng-untouched.ng-pristine.ng-invalid"));
    this.createManualEntry_msg = element(by.xpath("//div[contains(text(),' Successfully Added')]"));
    this.changeAssociate_verify = element(by.xpath("(//mat-select-trigger[contains(text(),'QA')])[1]"));
    this.duplicateManualEntry_msg = element(by.xpath("//span[@class='example-pizza-party']"));
    this.filter_ManualEntry_verify = element(by.xpath("//span[text()=' Manual Entry ']"));
    this.filterTimer_verify = element(by.xpath("//i[@class='fa fa-circle']"));



    //ANALYTICS============>

    //matterAnalytics
    this.matterAnalytics_verify = element(by.id('analytics-data1'));
    this.timesheetAnalytics_verify = element(by.id('timesheet-username-1'));

    //memberAnalytics
    this.memberAnalytics_verify = element(by.xpath("//div[@class='left-section']"));



    //PLACES===============>
    this.addressPage_verify = element(by.css('div.location-details'));
    this.viewerPage_verify = element(by.css('div.location-details.align-center'));

    //EMPLOYEE=============>
    this.Emp_verify = element(by.xpath("//h3[contains(text(),'Qa One Alpha')]"));
    this.pluginIntegration_verify = element(by.css("span.ng-tns-c15-306.ng-star-inserted"));

    //CONTACTS=============>
    this.userAddress_verify = element(by.css('div.contact-address'));

}
module.exports = new verifyPO();