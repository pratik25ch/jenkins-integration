var DROPBOXPO = function () {

this.dropbox_Emailname = element(by.name('login_email'));
this.dropbox_Password = element(by.name('login_password'));
this.dropbox_SignIn = element(by.xpath("//button[@type='submit']//div[1]"));

this.dropbox_SearchMatter = element(by.xpath("//input[@placeholder='Search']"));
this.dropbox_firstMatter = element(by.xpath("(//div[contains(@class,'mc-media-cell-text mc-media-cell-text-title')]//div)[1]"));

this.signInWithGoogle = element(by.xpath("//div[@class='sign-in-text']"));

this.gmailName = element(by.xpath("//div[@data-identifier='debasish@flowace.in']"));
this.userName = element(by.id('identifierId'));
this.password = element(by.name('password'));
this.NextBtn = element(by.xpath("//span[@class='RveJvd snByac']"));

}

module.exports = new DROPBOXPO();