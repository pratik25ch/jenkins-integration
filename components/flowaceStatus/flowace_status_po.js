var STATUSPO = function () {

    this.QAstatusURL = function () {
        browser.get("https://qa.status.flowace.in/")
    }
    this.StatusText = async function () {
        return await element(by.xpath("//h1[@class='text_center width_100']")).getText();
    }
    
}
module.exports = new STATUSPO();