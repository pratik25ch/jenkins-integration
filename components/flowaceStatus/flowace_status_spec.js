var CommPage = require("../../lib/CommLib/comm_lib")
var VerifyPage = require("../../lib/actionLib/verify")
var flowaceStatusPage = require("../flowaceStatus/flowace_status_po")

describe('Flowace-Running Status', function () {
    it('Should go to the Status Page', async function () {
        flowaceStatusPage.QAstatusURL();
        CommPage.sleep(3)
        expect(VerifyPage.flowace_statuspage.isPresent()).toBe(true);
    })
    it('Should Check the Status--514', async function () {
        let s1 = await flowaceStatusPage.StatusText();
        console.log(s1);
        expect(s1).toEqual("Flowace is up and running");
    })
})