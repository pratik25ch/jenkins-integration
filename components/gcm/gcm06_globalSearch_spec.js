var CommPage = require("../../lib/CommLib/comm_lib")
var HelperPage = require("../../utils/helperUtils/helper")
var VerifyPage = require("../../lib/actionLib/verify")
var LoginPage = require("../../pages/login/login_po")
var HomePage = require("../../lib/homepageLib/homepage_lib")
var GCMPage01 = require("../../pages/gcm/gcm01_main_po");
var GCMPage02 = require("../../pages/gcm/gcm02_task_po");
var GCMPage03 = require("../../pages/gcm/gcm03_search_po");
var GCMPage04 = require("../../pages/gcm/gcm04_delete_po");
var GCMPage06 = require("../../pages/gcm/gcm06_globalSearch_po");
var TimesheetPage = require("../../pages/timesheets/timesheets01_po")
var TestCompPage = require("../../testComponents/gcm/gcmComp")

let R = Math.random().toString(36).substring(7);

beforeAll(function () {
    LoginPage.GetURL();
    LoginPage.login()
    CommPage.sleep(3);
})

describe('GCM MODULE:-- Global Search', function () {
    it('Should Create Group', async function () {
        HomePage.Matter();
        GCMPage01.gcm_plusIcon.click();
        GCMPage01.iconGroupDropDown.click();
        HelperPage.waitForDisplay(VerifyPage.openPanel_verify);
        TestCompPage.newGroup(`Global Group-${R}`);
        expect(VerifyPage.newGroup_msg.getText()).toContain("created Successfully");
        CommPage.sleep(2);
    })
    it('Should Create Client', async function () {
        GCMPage01.gcm_plusIcon.click();
        CommPage.sleep(1);
        GCMPage01.iconClientDropDown.click();
        HelperPage.waitForDisplay(VerifyPage.openPanel_verify);
        GCMPage01.matSelectGroup.click();
        GCMPage01.searchDropdown.sendKeys("Globl Group-", R);
        GCMPage01.secondOption.click();
        GCMPage01.clientName.sendKeys("Global Client-", R);
        await GCMPage01.saveBtn.click().then(function () {
            console.log("CLIENT created sucessfully");
        })
        expect(VerifyPage.newClient_msg.getText()).toContain('created Successfully');
        CommPage.sleep(2);
    })
    it('Should Create Matter', async function () {
        GCMPage01.gcm_plusIcon.click();
        CommPage.sleep(1);
        GCMPage01.iconMatterDropDown.click();
        CommPage.sleep(5);
        GCMPage01.matSelectGroup.click();
        GCMPage01.searchDropdown.sendKeys("Globl Group-", R);
        GCMPage01.secondOption.click();
        CommPage.sleep(2);
        GCMPage01.matSelectClient.click();
        GCMPage01.searchDropdown.sendKeys("Globl Client-", R);
        GCMPage01.secondOption.click();
        CommPage.sleep(2);
        GCMPage01.matterName.sendKeys('Global Matter-', R);
        CommPage.sleep(2)
        GCMPage01.selectMember.sendKeys('Debasish Patra');
        CommPage.sleep(3);
        GCMPage01.firstOption.click();
        GCMPage01.saveBtn.click().then(function () {
            console.log("Matter Created sucessfully")
        })
        expect('Matter Created sucessfully').toEqual('Matter Created sucessfully');
        CommPage.sleep(1);
    })
})
fdescribe('GCM Advance Global Search', function () {
    it('Should Open the Advance Search Tab ', async function () {
        HomePage.Matter();
        GCMPage06.globalSearchIcon.click();
        HelperPage.waitForDisplay(VerifyPage.globalSearch_verify);
        CommPage.sleep(2);
        GCMPage06.advanceSearch();
        CommPage.sleep(3);
        expect('Advance Search Opened').toEqual('Advance Search Opened');
    })
    it('Should Search by Active Matters', async function () {
        //Active Matter
        GCMPage06.ActiveMatters();
        GCMPage06.searchBtn();
        HelperPage.waitForDisplay(VerifyPage.advanceSearchResult_verify);
        let v1 = await GCMPage06.searchResult();
        console.log("Total Active Matters are", v1);
        CommPage.sleep(3);
        expect('Active Matters').toEqual('Active Matters');
    })
    it('Should Export the Active Matters', async function () {
        GCMPage06.exportBtn.click().then(function () {
            console.log("Active Matters Imported Successfully");
        })
        CommPage.sleep(2);
        expect('Export Successfully').toEqual('Export Successfully');
    })
    it('Should Search by Archived Matters', async function () {
        GCMPage06.clearSearch.click();
        GCMPage06.advanceSearch();
        CommPage.sleep(3);
        GCMPage06.ArchivedMatters();
        GCMPage06.searchBtn();
        HelperPage.waitForDisplay(VerifyPage.advanceSearchResult_verify);
        let v2 = await GCMPage06.searchResult();
        console.log("Total Archived Matters are", v2);
        CommPage.sleep(3);
        expect('Archived Matters').toEqual('Archived Matters');
    })
    it('Should Export the Archived Matters', async function () {
        GCMPage06.exportBtn.click().then(function () {
            console.log("Archived Matters Imported Successfully");
        })
        CommPage.sleep(2);
        expect('Export Successfully').toEqual('Export Successfully');
    })
    it('Should Search Matters Shared with Dropbox', async function () {
        GCMPage06.clearSearch.click();
        GCMPage06.advanceSearch();
        CommPage.sleep(3);
        GCMPage06.DropboxMatter();
        GCMPage06.searchBtn();
        HelperPage.waitForDisplay(VerifyPage.advanceSearchResult_verify);
        let v3 = await GCMPage06.searchResult();
        console.log("Total Matters with Dropbox are", v3);
        CommPage.sleep(3);
        expect('Dropbox Matters').toEqual('Dropbox Matters');
    })
    it('Should Export the Dropbox Matters', async function () {
        GCMPage06.exportBtn.click().then(function () {
            console.log("Dropbox Matters Imported Successfully");
        })
        CommPage.sleep(2);
        expect('Export Successfully').toEqual('Export Successfully');
    })
    it('Should Search Matters without Dropbox', async function () {
        GCMPage06.clearSearch.click();
        GCMPage06.advanceSearch();
        CommPage.sleep(3);
        GCMPage06.DropboxMatter();
        GCMPage06.DropboxMatter();
        GCMPage06.searchBtn();
        HelperPage.waitForDisplay(VerifyPage.advanceSearchResult_verify);
        let v4 = await GCMPage06.searchResult();
        console.log("Total Matters Without Dropbox are", v4);
        CommPage.sleep(3);
        expect('Without Dropbox Matters').toEqual('Without Dropbox Matters');
    })
    it('Should Export Matters without Dropbox', async function () {
        GCMPage06.exportBtn.click().then(function () {
            console.log("Dropbox Matters Imported Successfully");
        })
        CommPage.sleep(2);
        expect('Export Successfully').toEqual('Export Successfully');
    })
    it('Should Search Matters Shared with Salesforce', async function () {
        GCMPage06.clearSearch.click();
        GCMPage06.advanceSearch();
        CommPage.sleep(3);
        GCMPage06.DropboxMatter();
        GCMPage06.searchBtn();
        HelperPage.waitForDisplay(VerifyPage.advanceSearchResult_verify);
        let v3 = await GCMPage06.searchResult();
        console.log("Total Matters with Dropbox are", v3);
        CommPage.sleep(3);
        expect('Dropbox Matters').toEqual('Dropbox Matters');
    })
    it('Should Export the Salesforce Matters', async function () {
        GCMPage06.exportBtn.click().then(function () {
            console.log("Dropbox Matters Imported Successfully");
        })
        CommPage.sleep(2);
        expect('Export Successfully').toEqual('Export Successfully');
    })
    it('Should Search Matters without Salesforce', async function () {
        GCMPage06.clearSearch.click();
        GCMPage06.advanceSearch();
        CommPage.sleep(3);
        GCMPage06.salesforceMatter();
        GCMPage06.salesforceMatter();
        GCMPage06.searchBtn();
        HelperPage.waitForDisplay(VerifyPage.advanceSearchResult_verify);
        let v5 = await GCMPage06.searchResult();
        console.log("Total Matters Without Salesforce are", v5);
        CommPage.sleep(3);
        expect('Without Salesforce Matters').toEqual('Without Salesforce Matters');
    })
    it('Should Export Matters without Salesforce', async function () {
        GCMPage06.exportBtn.click().then(function () {
            console.log("Salesforce Matters Imported Successfully");
        })
        CommPage.sleep(2);
        expect('Export Successfully').toEqual('Export Successfully');
    })
    it('Should Search Matters By Datewise', async function () {
        GCMPage06.clearSearch.click();
        GCMPage06.advanceSearch();
        CommPage.sleep(3);
        GCMPage06.selectDate();
        CommPage.sleep(1);
        GCMPage06.yesterday.click();

        















        CommPage.sleep(1);
        // GCMPage03.totalIconMatters();





    })

})