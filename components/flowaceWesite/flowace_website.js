var CommPage = require("../../lib/CommLib/comm_lib")

describe('Flowace-Website', function () {
    it('Should Open Flowace Website-HomePage', async function () {
        CommPage.flowaceWebsiteURL();
        CommPage.sleep(3);
        expect(element(by.id("home_page")).isDisplayed()).toBeTruthy();
        console.log("Flowace Home Page Displayed Successfuly")
    })
    it('Should Open flowace-About Section', async function () {
        element(by.xpath("(//a[@class='active-hover'])[2]")).click()
        CommPage.sleep(5);
        expect(element(by.id("about_page")).isDisplayed()).toBeTruthy();
        console.log("Flowace About Section Displayed Successfuly")
    })
    it('Should Open flowace-features Section', async function () {
        element(by.xpath("(//a[@class='active-hover'])[3]")).click()
        CommPage.sleep(5);
        expect(element(by.id("features_page")).isDisplayed()).toBeTruthy();
        console.log("Flowace features Section Displayed Successfuly")
    })
    it('Should Open flowace-Testimonials Section', async function () {
        element(by.xpath("(//a[@class='active-hover'])[4]")).click()
        CommPage.sleep(5);
        expect(element(by.id("testimonial_page")).isDisplayed()).toBeTruthy();
        console.log("Flowace testimonials Section Displayed Successfuly")
    })
    it('Should Open flowace-Contact US Section', async function () {
        element(by.xpath("(//a[@class='active-hover'])[5]")).click()
        CommPage.sleep(5);
        expect(element(by.id("contact_page")).isDisplayed()).toBeTruthy();
        console.log("Flowace Contact US Section Displayed Successfuly")
    })
})
describe('Flowace location', function () {
    it('Should Open flowace location', async function () {
        element(by.xpath("//span[@class='lnr lnr-map-marker']")).click();
        CommPage.sleep(2);
        browser.getAllWindowHandles().then(function (handles) {
            browser.switchTo().window(handles[1]);
        })
        CommPage.sleep(8);
        expect(element(by.xpath("//canvas[@class='widget-scene-canvas']")).isDisplayed()).toBeTruthy();
        console.log('Location Displayed Successfully');
        browser.getAllWindowHandles().then(function (handles) {
            // We currently are on the second tab...
            browser.driver.close();
            browser.switchTo().window(handles[0]);
        })
        expect(element(by.id("contact_page")).isDisplayed()).toBeTruthy();
        CommPage.sleep(5);
    })
    it('Should Open Book a Demo', async function () {
        element(by.xpath("//div[@class='right-button hidden-xs']//a[1]")).click();
        CommPage.sleep(3);
        CommPage.refresh();






    })

})