var CommPage = require("../../../lib/CommLib/comm_lib")
var LoginPage = require("../../../pages/login/login_po")
var HomePage = require("../../../lib/homepageLib/homepage_lib")
var TimesheetPage = require("../../../pages/timesheets/timesheets01_po")
var BillingPage = require("../../../pages/billing/billing_po")
var ContactsPage01 = require("../../../pages/administration/contacts/contacts01_po")

let R = Math.random().toString(36).substring(7);

/**
 * 1- Should Create New Contact.
 * 2- Should Search Contact By Name.
 * 3- Should Search Contact By Primary Numbar.
 * 4- Should Search Contact By Secondary Number.
 * 4- Should Edit Contact.
 */

beforeAll(async function () {
    LoginPage.GetURL();
    LoginPage.login()
    CommPage.sleep(3);
    HomePage.Administration();
    CommPage.sleep(0.5);
    HomePage.Contacts()
    CommPage.sleep(1);
})

describe('CONTACTS MODULE :--- Add and Edit Contact', function () {
    it('Should Create New Contact', async function () {
        ContactsPage01.newContactBtn();
        ContactsPage01.CreateContact(`001 Demo-${R}`, "Testing", "AMAZON", 1111166666, "abc@gmail.com", "Flowace", 2222266666);
        expect('Contact Created Sucessfully').toEqual('Contact Created Sucessfully');
    })
    it('Should Search Contact By Name', async function () {
        ContactsPage01.searchContact(`001 Demo-${R} Testing`);
        expect('Contact Created Sucessfully').toEqual('Contact Created Sucessfully');
        CommPage.sleep(0.5);
    })
    it('Should Search Contact By Primary Numbar ', async function () {
        CommPage.sleep(2)
        ContactsPage01.searchContact(1111166666);
        expect('Contact Created Sucessfully').toEqual('Contact Created Sucessfully');
        CommPage.sleep(0.5);
    })
    it('Should Search Contact By Secondary Number--313 ', async function () {
        CommPage.sleep(2)
        ContactsPage01.searchContact(2222266666);
        expect('Contact Created Sucessfully').toEqual('Contact Created Sucessfully');
        CommPage.sleep(0.5);
    })
    it('Should Edit Contact--364', async function () {
        await ContactsPage01.editOption.click();
        CommPage.sleep(1);
        ContactsPage01.editcontact.click();
        CommPage.sleep(5);
        await ContactsPage01.firstname.clear().sendKeys('001 Automation');
        await ContactsPage01.lastname.clear().sendKeys('Testing-' + R + '-');
        ContactsPage01.company.clear().sendKeys('Flowace');
        ContactsPage01.email.clear().sendKeys('qa@gmail.in');
        ContactsPage01.seconderyType.click();
        ContactsPage01.numbertype.click();
        ContactsPage01.selectcountry.click();
        ContactsPage01.countrySearch.sendKeys('india');
        ContactsPage01.country.click();
        ContactsPage01.inputnumber.sendKeys('9785461320');
        ContactsPage01.addnumber.click();
        CommPage.sleep(1);
        //Tag Contact-364
        ContactsPage01.tagsGroup.click();
        ContactsPage01.tagsGroupInput.click();
        ContactsPage01.tagsGroupInput.sendKeys('N');
        ContactsPage01.firstInput.click();
        ContactsPage01.tagsClient.click();
        ContactsPage01.tagsClientInput.sendKeys('N');
        ContactsPage01.firstInput.click();
        ContactsPage01.tagsMatter.click();
        ContactsPage01.tagsMatterInput.sendKeys('N');
        ContactsPage01.firstInput.click();
        ContactsPage01.saveBtn.click().then(function () {
            CommPage.sleep(3);
            console.log('Contact updated sucessfully')
        })
        expect('Contact updated sucessfully').toEqual('Contact updated sucessfully');
    })
})