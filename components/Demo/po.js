var CommPage = require("../../../lib/CommLib/comm_lib");
var Contacts01PO = function () {

    let R = Math.random().toString(36).substring(7);
    //New Contact
    let newContact = element(by.id('add-contact'));
    let firstname = element(by.name('firstName'));
    let lastname = element(by.name('lastName'));
    let company = element(by.name('company'));
    let primaryNumber = element(by.id('primaryPhone'));
    let email = element(by.name('email'));
    let firstInput = element(by.xpath("(//span[@class='mat-option-text'])[1]"));
    let placeType = element(by.id('place-type'));
    let address = element(by.id('address'));

    let firstAddress = element(by.xpath("//div[@xpath='1']//input[1]"));
    let seconderyType = element(by.xpath("//span[text()='Type']"));
    let numbertype = element(by.xpath("(//span[@class='mat-option-text'])[1]"));
    let selectcountry = element(by.id('mat-select-name'));
    let countrySearch = element(by.xpath("//input[@placeholder='Country']"));
    let country = element(by.xpath("//span[text()='India (91)']"));
    let inputnumber = element(by.xpath("//input[@name='number']"));
    let addnumber = element(by.xpath("//span[@class='mat-button-wrapper'][contains(text(),'Add')]"));
    let saveBtn = element(by.xpath("//span[contains(text(),'save')]"));
    //Edit Contact
    let searchContact = element(by.xpath("//input[@type='text']"));
    this.editOption = element(by.xpath("(//i[@class='fa fa-ellipsis-v'])[1]"));
    this.editcontact = element(by.xpath("(//button[@role='menuitem'])[1]"));
    //Edit Contact Tag Group
    this.tagsGroup = element(by.xpath("(//mat-panel-title[@class='mat-expansion-panel-header-title']//strong)[1]"));
    this.tagsGroupInput = element(by.name('group'));
    this.tagsClient = element(by.xpath("(//mat-panel-title[@class='mat-expansion-panel-header-title']//strong)[2]"));
    this.tagsClientInput = element(by.name('client'));
    this.tagsMatter = element(by.xpath("(//mat-panel-title[@class='mat-expansion-panel-header-title']//strong)[3]"));
    this.tagsMatterInput = element(by.name('matter'));

    this.newContactBtn = function () {
        newContact.click();
    }
    this.CreateContact = function (FN, LN, Company_Name, PN, mail, place, SN) {
        CommPage.sleep(3);
        firstname.sendKeys(FN);
        lastname.sendKeys(LN);
        company.sendKeys(Company_Name);
        primaryNumber.sendKeys(PN);
        email.sendKeys(mail);
        CommPage.sleep(1);
        placeType.click();
        CommPage.sleep(0.5)
        firstInput.click();
        CommPage.sleep(1);
        address.sendKeys(place);
        seconderyType.click();
        numbertype.click();
        selectcountry.click();
        countrySearch.sendKeys('india');
        country.click();
        inputnumber.sendKeys(SN);
        addnumber.click();
        CommPage.sleep(1);
        saveBtn.click().then(function () {
            console.log('Contact Created Sucessfully')
        })
        CommPage.sleep(3);
    }
    this.searchContact = function (search) {
        searchContact.clear().sendKeys(search);
        console.log('Contact Searching Sucessfully')
    }









}
module.exports = new Contacts01PO();