var Request = require("request");
var TimesheetStart = 1583209800000
var TimesheetEnd = TimesheetStart + 60000
let timesheetInsertion = function (hasConflict) {
    if (hasConflict===true) {
        hasConflict = -1
        console.log("has conflicts")
    }else{
        hasConflict = 1
        console.log(" doesn't have conflicts")
    }
    return function () {

        let count = 5;
        let timestampIncr = 0;
        for (let i = 0; i < count; i++) {
            console.log(i)
            it("api Testing in protractor", function (done) {
                let start = TimesheetStart + hasConflict * timestampIncr;
                let end = TimesheetEnd + timestampIncr;
                console.log('start ', start);
                console.log('end ', end);
                console.log(i);
                Request.post({
                        "headers": ({
                            "content-type": process.env.CONTENT,
                            "Authorization": process.env.TOKEN,
                            "Origin": process.env.ORIGIN,
                        }),
                        "url": `${process.env.URL}/v1/Timesheets/manualEntry`,
                        "body": JSON.stringify({
                            "type": "MANUAL_ENTRY",
                            "userId": +process.env.USER_ID,
                            "appTitle": "QA Api Entry",
                            "matterId": +process.env.MATTER_ID,
                            "appId": 0,
                            "source": "WebClient",
                            "userRate": 600000,
                            "updatedByUserId": +process.env.USER_ID,
                            "requestType": "save",
                            "timestampStart": start,
                            "timestampEnd": end,
                        })
                    },
                    (error, response, body) => {
                        if (error) {
                            return console.dir(error);
                        }
                        console.dir("Body : ******************");
                        console.dir(response.body);

                        console.log("Status : ****************");
                        console.log(response.statusCode)
                        expect(response.statusCode).toEqual(204);
                        // if (response.statusCode !== 204) {
                        //     break;
                        //     console.log(i)
                        // }
                        timestampIncr += 60000;
                        // console.log("\n\nHeader ****:")
                        // console.log(response.headers)
                        // this below line took half day of research
                        done();
                    });

            });

        }
    }
}
describe("API Testing With timesheet conflicts", timesheetInsertion(true));
// describe("API Testing Without timesheet conflicts", timesheetInsertion(false));