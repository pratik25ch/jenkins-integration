var Request = require("request");
var api = require('../../../../api_pageObject/gcm_api');
describe("Errors in Protractor", function () {
    it("api Testing in protractor", function (done) {
        //console.log(process.env.QA_ORIGIN)
        Request.post({
            "headers": ({
                "content-type":process.env.CONTENT,
                "Authorization": process.env.TOKEN,
                "Origin": process.env.ORIGIN,
            }),
            "url": `${process.env.URL}/v1/Groups`,
            "body": JSON.stringify({
                "newContacts": "[]",
                "groupName": "POSTMAN GAURA5",
                "removedContacts": "[]",
                "updatedByUserId": "164",
                "createdByUserId": "164",
            })
        },
        (error, response, body) => {
            if (error) {
                return console.dir(error);
            }
            console.dir("Body : ******");
            console.dir(response.body);

            console.log("Status ****:")
            console.log(response.statusCode)
            //expect(response.statusCode).toEqual(200);

            // console.log("\n\nHeader ****:")
            // console.log(response.headers)

            // this below line took half day of research
            done();
        });
    });
});