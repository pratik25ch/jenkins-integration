var Request = require("request");
var locationData = [{
        "accuracy": 23.68,
        "address": "Ram Mandir Rd, Talvali, Ghansoli Gaon, Ghansoli, Navi Mumbai, Maharashtra 400701, India",
        "locTime": "2019-12-20T01:30:04.000Z",
        "location": {
            "lng": 72.9948176,
            "lat": 19.1233608
        },
        "macAddress": "40:A1:08:55:C9:F2",
        "metaData": {
            "nameValuePairs": {
                "appVersion": "2.4.4.0",
                "brand": "OnePlus",
                "deviceName": "OnePlus A6003",
                "freeInternalStorage": "19663 MB",
                "gpsStatus": "true",
                "id": "O11019",
                "internetStatus": "true",
                "model": "OnePlus A6003",
                "osVersion": "10.1.0",
                "totalRam": "2791 MB"
            }
        },
        "placeEventId": null,
        "userId": 164
    },
    {
        "accuracy": 23.68,
        "address": "Ram Mandir Rd, Talvali, Ghansoli Gaon, Ghansoli, Navi Mumbai, Maharashtra 400701, India",
        "locTime": "2019-12-20T02:30:04.000Z",
        "location": {
            "lng": 72.9948176,
            "lat": 19.1233608
        },
        "macAddress": "40:A1:08:55:C9:F2",
        "metaData": {
            "nameValuePairs": {
                "appVersion": "2.4.4.0",
                "brand": "OnePlus",
                "deviceName": "OnePlus A6003",
                "freeInternalStorage": "19663 MB",
                "gpsStatus": "true",
                "id": "O11019",
                "internetStatus": "true",
                "model": "OnePlus A6003",
                "osVersion": "10.1.0",
                "totalRam": "2791 MB"
            }
        },
        "placeEventId": null,
        "userId": 164
    },
    {
        "accuracy": 23.68,
        "address": "Ram Mandir Rd, Talvali, Ghansoli Gaon, Ghansoli, Navi Mumbai, Maharashtra 400701, India",
        "locTime": "2019-12-20T03:30:04.000Z",
        "location": {
            "lng": 72.9948176,
            "lat": 19.1233608
        },
        "macAddress": "40:A1:08:55:C9:F2",
        "metaData": {
            "nameValuePairs": {
                "appVersion": "2.4.4.0",
                "brand": "OnePlus",
                "deviceName": "OnePlus A6003",
                "freeInternalStorage": "19663 MB",
                "gpsStatus": "true",
                "id": "O11019",
                "internetStatus": "true",
                "model": "OnePlus A6003",
                "osVersion": "10.1.0",
                "totalRam": "2791 MB"
            }
        },
        "placeEventId": null,
        "userId": 164
    },
    {
        "accuracy": 25.68,
        "address": "2, Diva Nagar Rd, Mangrove Forest, Airoli, Navi Mumbai, Maharashtra 400708, India",
        "locTime": "2019-12-20T04:30:04.000Z",
        "location": {
            "lng": 72.9827727,
            "lat": 19.1490011
        },
        "macAddress": "40:A1:08:55:C9:F2",
        "metaData": {
            "nameValuePairs": {
                "appVersion": "2.4.4.0",
                "brand": "OnePlus",
                "deviceName": "OnePlus A6003",
                "freeInternalStorage": "19663 MB",
                "gpsStatus": "true",
                "id": "O11019",
                "internetStatus": "true",
                "model": "OnePlus A6003",
                "osVersion": "10.1.0",
                "totalRam": "2791 MB"
            }
        },
        "placeEventId": null,
        "userId": 164
    },
    {
        "accuracy": 25.68,
        "address": "2, Diva Nagar Rd, Mangrove Forest, Airoli, Navi Mumbai, Maharashtra 400708, India",
        "locTime": "2019-12-20T05:30:04.000Z",
        "location": {
            "lng": 72.9827727,
            "lat": 19.1490011
        },
        "macAddress": "40:A1:08:55:C9:F2",
        "metaData": {
            "nameValuePairs": {
                "appVersion": "2.4.4.0",
                "brand": "OnePlus",
                "deviceName": "OnePlus A6003",
                "freeInternalStorage": "19663 MB",
                "gpsStatus": "true",
                "id": "O11019",
                "internetStatus": "true",
                "model": "OnePlus A6003",
                "osVersion": "10.1.0",
                "totalRam": "2791 MB"
            }
        },
        "placeEventId": null,
        "userId": 164
    },
    {
        "accuracy": 25.68,
        "address": "2, Diva Nagar Rd, Mangrove Forest, Airoli, Navi Mumbai, Maharashtra 400708, India",
        "locTime": "2019-12-20T06:30:04.000Z",
        "location": {
            "lng": 72.9827727,
            "lat": 19.1490011
        },
        "macAddress": "40:A1:08:55:C9:F2",
        "metaData": {
            "nameValuePairs": {
                "appVersion": "2.4.4.0",
                "brand": "OnePlus",
                "deviceName": "OnePlus A6003",
                "freeInternalStorage": "19663 MB",
                "gpsStatus": "true",
                "id": "O11019",
                "internetStatus": "true",
                "model": "OnePlus A6003",
                "osVersion": "10.1.0",
                "totalRam": "2791 MB"
            }
        },
        "placeEventId": null,
        "userId": 164
    }, {
        "accuracy": 26.68,
        "address": "1, Millenium Business Park, Sector 2, Mahape, Navi Mumbai, Maharashtra 400701, India",
        "locTime": "2019-12-20T07:30:04.000Z",
        "location": {
            "lng": 73.0154859,
            "lat": 19.1107414
        },
        "macAddress": "40:A1:08:55:C9:F2",
        "metaData": {
            "nameValuePairs": {
                "appVersion": "2.4.4.0",
                "brand": "OnePlus",
                "deviceName": "OnePlus A6003",
                "freeInternalStorage": "19663 MB",
                "gpsStatus": "true",
                "id": "O11019",
                "internetStatus": "true",
                "model": "OnePlus A6003",
                "osVersion": "10.1.0",
                "totalRam": "2791 MB"
            }
        },
        "placeEventId": null,
        "userId": 164
    },
    {
        "accuracy": 26.68,
        "address": "1, Millenium Business Park, Sector 2, Mahape, Navi Mumbai, Maharashtra 400701, India",
        "locTime": "2019-12-20T08:30:04.000Z",
        "location": {
            "lng": 73.0154859,
            "lat": 19.1107414
        },
        "macAddress": "40:A1:08:55:C9:F2",
        "metaData": {
            "nameValuePairs": {
                "appVersion": "2.4.4.0",
                "brand": "OnePlus",
                "deviceName": "OnePlus A6003",
                "freeInternalStorage": "19663 MB",
                "gpsStatus": "true",
                "id": "O11019",
                "internetStatus": "true",
                "model": "OnePlus A6003",
                "osVersion": "10.1.0",
                "totalRam": "2791 MB"
            }
        },
        "placeEventId": null,
        "userId": 164
    },
    {
        "accuracy": 26.68,
        "address": "1, Millenium Business Park, Sector 2, Mahape, Navi Mumbai, Maharashtra 400701, India",
        "locTime": "2019-12-20T09:30:04.000Z",
        "location": {
            "lng": 73.0154859,
            "lat": 19.1107414
        },
        "macAddress": "40:A1:08:55:C9:F2",
        "metaData": {
            "nameValuePairs": {
                "appVersion": "2.4.4.0",
                "brand": "OnePlus",
                "deviceName": "OnePlus A6003",
                "freeInternalStorage": "19663 MB",
                "gpsStatus": "true",
                "id": "O11019",
                "internetStatus": "true",
                "model": "OnePlus A6003",
                "osVersion": "10.1.0",
                "totalRam": "2791 MB"
            }
        },
        "placeEventId": null,
        "userId": 164
    }
];
console.log('location length ', locationData.length)
let locationList = [];
locationData.forEach((data) => {
    let i;
    locationList.push(data);
    let incr = 30000;
    for (i = 0; i < 10; i++) {
        let ts = new Date(data.locTime).getTime() + incr;
        data.locTime = new Date(ts).toISOString();
        locationList.push(data);
        incr += incr;
    }
})
/**
 * 1- Crete Place Data.(Bunch Data)
 * 
 * 
 * 
 */
describe("Errors in Protractor", function () {
    it("api Testing in protractor", function (done) {
        Request.post({
                "headers": ({
                    "content-type": process.env.CONTENT,
                    "Authorization": process.env.TOKEN,
                    "Origin": process.env.ORIGIN,
                }),
                "url": `${process.env.URL}/v1/UserLocations`,
                "body": JSON.stringify(
                    locationList
                )
            },
            (error, response, body) => {
                if (error) {
                    return console.dir(error);
                }
                console.log("Status ****:")
                console.log(response.statusCode)
                done();
            });
    });
});